CREATE DATABASE  IF NOT EXISTS `e-learning` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `e-learning`;
-- MariaDB dump 10.19  Distrib 10.5.9-MariaDB, for osx10.16 (x86_64)
--
-- Host: 127.0.0.1    Database: e-learning
-- ------------------------------------------------------
-- Server version	10.5.9-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `courses` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `description` varchar(800) NOT NULL,
  `isPublic` tinyint(1) NOT NULL DEFAULT 1,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `creator_id` int(11) NOT NULL,
  `image` varchar(200) DEFAULT '0578c577c6d58f1daaabf03305501f78',
  PRIMARY KEY (`id`),
  UNIQUE KEY `title_UNIQUE` (`title`),
  KEY `fk_courses_users1_idx` (`creator_id`),
  CONSTRAINT `fk_courses_users1` FOREIGN KEY (`creator_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=99 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
INSERT INTO `courses` VALUES (89,'JavaScript for Beginners','The ultimate JavaScript course for absolute beginners.',1,0,7,'0578c577c6d58f1daaabf03305501f78'),(90,'C# for Beginners','Master the basics of C# programming',1,0,7,'0578c577c6d58f1daaabf03305501f78'),(91,'C++ Advanced - Part 1','Master C++ programming',1,0,7,'f0134cc35b0b30d79eedc043a9fc628e'),(92,'C++ Advanced - Part 2','Master C++ programming',1,0,7,'f0134cc35b0b30d79eedc043a9fc628e'),(93,'React for Advanced Developers','Everything you need to build SPAs with React',0,0,7,'0578c577c6d58f1daaabf03305501f78'),(94,'Go for Advanced Developers','Level up your Go skills.',0,0,7,'c7e566d1b51a8fcdba519511b48465ea'),(95,'Photography for teachers','The course is ideal for both curious students and adults',1,0,8,'cb5ec92ceb1d6bfb1114dd52ef294024'),(96,'Python from A to Z Deleted on: 9/1/2021 8:39:10 PM','Step-by-step online course for beginners',1,1,7,'498a14ab7a9c54680b63903380115ae6'),(97,'Ruby on Rails for Kids Deleted on: 9/2/2021 12:12:37 AM','Ruby on Rails for kids in 3-5 grade',1,1,7,'0578c577c6d58f1daaabf03305501f78'),(98,'Photography - First Steps Deleted on: 9/2/2021 9:28:22 AM','The photography basics',1,1,7,'0578c577c6d58f1daaabf03305501f78');
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `roles`
--

DROP TABLE IF EXISTS `roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `roles` (
  `id` int(11) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `type_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `roles`
--

LOCK TABLES `roles` WRITE;
/*!40000 ALTER TABLE `roles` DISABLE KEYS */;
INSERT INTO `roles` VALUES (1,'Student'),(2,'Teacher');
/*!40000 ALTER TABLE `roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `sections`
--

DROP TABLE IF EXISTS `sections`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `sections` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `title` varchar(100) NOT NULL,
  `content` text NOT NULL,
  `order_num` int(11) NOT NULL,
  `restriction_date` datetime DEFAULT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  `course_id` int(11) NOT NULL,
  `isEmbedded` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  KEY `fk_sections_courses1_idx` (`course_id`),
  CONSTRAINT `fk_sections_courses1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=166 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `sections`
--

LOCK TABLES `sections` WRITE;
/*!40000 ALTER TABLE `sections` DISABLE KEYS */;
INSERT INTO `sections` VALUES (150,'First steps with JavaScript','<p>Make your first steps with JavaScript</p>',10,NULL,0,89,0),(151,'Variables','<iframe width=\"950\" height=\"550\" src=\"https://docs.google.com/presentation/d/e/2PACX-1vR9nPLcRE4_8KX_BBH3KSwHU1WvUrh-rMIefp4eH-_SYFqrc6yeP8eZbcNC4xGq-g/embed?start=false&loop=false&delayms=3000\" frameborder=\"0\" allowfullscreen=\"true\" mozallowfullscreen=\"true\" webkitallowfullscreen=\"true\"></iframe><p>Source: Telerik Academy</p>',20,NULL,0,89,0),(152,'Variables and Data Types','<iframe width=\"950\" height=\"550\"\" src=\"https://www.youtube.com/embed/edlFjlzxkSI\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe><p>Source: Dev Ed</p>',30,NULL,0,89,0),(153,'Functions and Parameters','<iframe width=\"950\" height=\"550\" src=\"https://www.youtube.com/embed/xjAu2Y2nJ34\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe><p>Source: Dev Ed</p>',40,NULL,0,89,0),(154,'Let vs. Var','<iframe width=\"950\" height=\"550\" src=\"https://www.youtube.com/embed/XgSjoHgy3Rk\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe><p>Source: Programming with Mosh</p>',50,NULL,0,89,0),(155,'C++ Programming Tutorial 1 - Intro to C++','<iframe width=\"950\" height=\"550\" src=\"https://www.youtube.com/embed/OTroAxvRNbw\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe><p>Source: Caleb Curry</p>',10,NULL,0,91,0),(156,'C# First Steps','<iframe width=\"950\" height=\"550\"src=\"https://www.youtube.com/embed/GhQdlIFylQ8\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe><p>Source: Free CodeCamp</p>',20,NULL,0,90,0),(157,'C# Tutorial For Beginners','<iframe width=\"950\" height=\"550\" src=\"https://www.youtube.com/embed/gfkTfcpWqAY\" title=\"YouTube video player\" frameborder=\"0\" allow=\"accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe><p>Source: Programming with Mosh</p>',30,NULL,0,90,0),(158,'Guidelines for photography','<iframe src=\"https://drive.google.com/file/d/1HH1pdmISG9QOYQL-yllUIqjnpgbZEdCD/preview\" width=\"940\" height=\"780\" allow=\"autoplay\"></iframe><p>Source: Harvard Graduate School of Education</p>',10,NULL,0,95,0),(159,'Before your start with C#','&lt;p&gt;This is what you&apos;ll learn during the course:&lt;/p&gt;&lt;ul&gt;&lt;li&gt;&lt;p&gt;C# Basics for Beginners: Learn C# Fundamentals by Coding&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;C# Intermediate: Classes, Interfaces, and OOP&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;C# Advanced Topics: Prepare for Technical Interviews&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;Introduction to C# Programming and Unity&lt;/p&gt;&lt;/li&gt;&lt;li&gt;&lt;p&gt;C# Fundamentals&lt;/p&gt;&lt;/li&gt;&lt;/ul&gt;',10,NULL,0,90,1),(160,'First C# Task','&lt;p&gt;Your first C# assignment will be to solve a set of tasks. &lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://www.w3schools.com/cs/cs_exercises.php&quot;&gt;Click here for the tasks.&lt;/a&gt;&lt;/p&gt;',40,'2021-09-07 12:30:00',0,90,0),(161,'Additional Resources','&lt;p&gt;&lt;strong&gt;Resources:&lt;/strong&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://medium.com/@cybercodetwins/intro-to-c-your-first-program-8fcc936ce1bb&quot;&gt;Your first C++ program&lt;/a&gt;&lt;/p&gt;&lt;p&gt;&lt;a href=&quot;https://medium.com/free-code-camp/some-awesome-modern-c-features-that-every-developer-should-know-5e3bf6f79a3c&quot;&gt;Modern C++ features you must learn about&lt;/a&gt;&lt;/p&gt;',20,NULL,0,91,0),(162,'Python 1:1','&lt;p&gt;&amp;lt;iframe width=&quot;560&quot; height=&quot;315&quot; src=&quot;https://www.youtube.com/embed/_uQrJ0TkZlc&quot; title=&quot;YouTube video player&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture&quot; allowfullscreen&amp;gt;&amp;lt;/iframe&amp;gt;&lt;/p&gt;&lt;p&gt;&lt;/p&gt;&lt;p&gt;Source: Programming with Mosh&lt;/p&gt;',10,NULL,0,96,0),(163,'Go Resources','&lt;p&gt;To be included here&lt;/p&gt;',10,NULL,0,94,0),(164,'Additional Resources','&lt;p&gt;&lt;a href=&quot;https://learn.telerikacademy.com/&quot;&gt;Link to advanced topics&lt;/a&gt;&lt;/p&gt;',50,NULL,0,90,0),(165,'Test Test','&lt;p&gt;&amp;lt;iframe width=&quot;950&quot; height=&quot;550&quot; src=&quot;https://www.youtube.com/embed/KJgsSFOSQv0&quot; title=&quot;YouTube video player&quot; frameborder=&quot;0&quot; allow=&quot;accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture&quot; allowfullscreen&amp;gt;&amp;lt;/iframe&amp;gt;&lt;/p&gt;',60,NULL,1,90,0);
/*!40000 ALTER TABLE `sections` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tokens`
--

DROP TABLE IF EXISTS `tokens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tokens` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(300) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `token_UNIQUE` (`token`)
) ENGINE=InnoDB AUTO_INCREMENT=125 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tokens`
--

LOCK TABLES `tokens` WRITE;
/*!40000 ALTER TABLE `tokens` DISABLE KEYS */;
INSERT INTO `tokens` VALUES (114,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MTQsImVtYWlsIjoicG9seWFAZ21haWwuY29tIiwicm9sZSI6MSwiaWF0IjoxNjMwNTEwMTM0LCJleHAiOjE2MzkxNTAxMzR9.tdBbhCBTrhQZxP5dY20FELNNbhyRMLFeKp9k8PLdkWA'),(116,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ0ZWFjaGVyQGdtYWlsLmNvbSIsInJvbGUiOjIsImlhdCI6MTYzMDUxMjk4OSwiZXhwIjoxNjM5MTUyOTg5fQ.Bkwy_AtgMQWvWbxONqkcl0pD1dT-3Sey8e0VoHM9KcM'),(118,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ0ZWFjaGVyQGdtYWlsLmNvbSIsInJvbGUiOjIsImlhdCI6MTYzMDUxODM3NSwiZXhwIjoxNjM5MTU4Mzc1fQ.fWjteW4E6UoFJa1OfFrIqmLCTHX1rMaXQpbOY6ygtBk'),(120,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ0ZWFjaGVyQGdtYWlsLmNvbSIsInJvbGUiOjIsImlhdCI6MTYzMDUzMzYwMywiZXhwIjoxNjM5MTczNjAzfQ.WW1K9DCeJq3oErnkPgd2Set4SoSSICGgyDvO1VrYQzQ'),(123,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6NywiZW1haWwiOiJ0ZWFjaGVyQGdtYWlsLmNvbSIsInJvbGUiOjIsImlhdCI6MTYzMDUzNDUxMCwiZXhwIjoxNjM5MTc0NTEwfQ.dKWRC2A7FNFBEk7Z9_DkylElzUYyfRKwiCbH6Z7HN7M'),(124,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwiZW1haWwiOiJzdHVkZW50QGdtYWlsLmNvbSIsInJvbGUiOjEsImlhdCI6MTYzMDU2MzczOCwiZXhwIjoxNjM5MjAzNzM4fQ.h-httOjRzbJCINkeV_H8EvKq23XKFY_cIYVrnzpjOHM'),(115,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwiZW1haWwiOiJzdHVkZW50QGdtYWlsLmNvbSIsInJvbGUiOjEsImlhdCI6MTYzMDUxMTk5MSwiZXhwIjoxNjM5MTUxOTkxfQ.Pz0HVQiHOmaftsiz5MFRsDgR2-y1gixqdTU69Q3aG8M'),(117,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwiZW1haWwiOiJzdHVkZW50QGdtYWlsLmNvbSIsInJvbGUiOjEsImlhdCI6MTYzMDUxNzk4NCwiZXhwIjoxNjM5MTU3OTg0fQ.FOAi7987MGCpibBdngwhlPjyP6pqUdog9kBXdfny1pI'),(119,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwiZW1haWwiOiJzdHVkZW50QGdtYWlsLmNvbSIsInJvbGUiOjEsImlhdCI6MTYzMDUzMTY5NSwiZXhwIjoxNjM5MTcxNjk1fQ.9y4A9WSmb6I6v4N_L8A3O5Mv32OU5-YxaKZu9htZJFI'),(122,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwiZW1haWwiOiJzdHVkZW50QGdtYWlsLmNvbSIsInJvbGUiOjEsImlhdCI6MTYzMDUzMzkzMywiZXhwIjoxNjM5MTczOTMzfQ.PVhdh2W1_F2uT7KzPFE2zatLP6vsuMvksPNFrDieT9s'),(121,'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6OSwiZW1haWwiOiJzdHVkZW50QGdtYWlsLmNvbSIsInJvbGUiOjEsImlhdCI6MTYzMDUzMzY5NCwiZXhwIjoxNjM5MTczNjk0fQ.a7Ez0rXrE3aMzb0ITVIPghFYJUJyGz84JGvhmpRfY4A');
/*!40000 ALTER TABLE `tokens` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(320) NOT NULL,
  `firstname` varchar(45) NOT NULL,
  `lastname` varchar(45) NOT NULL,
  `password` varchar(128) NOT NULL,
  `role_id` int(11) NOT NULL,
  `isDeleted` tinyint(1) NOT NULL DEFAULT 0,
  PRIMARY KEY (`id`),
  UNIQUE KEY `id_UNIQUE` (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`),
  KEY `fk_users_roles_idx` (`role_id`),
  CONSTRAINT `fk_users_roles` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
INSERT INTO `users` VALUES (7,'teacher@gmail.com','Tony','Daskalova','$2b$10$f/po2uYm6v/0qs.5HUzUFOmiJVNKh58/j7x9oJHSDc.9jOz7/JS02',2,0),(8,'teo@gmail.com','Teo','Atanasov','$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',2,0),(9,'student@gmail.com','Alex','Studentska','$2b$10$ba4MPEc2Xp0F1tWJIUvWFObZZnxNZlKFMGOY4Y7lNGsiu9YSNVaJO',1,0),(10,'pesho@gmail.com','Pesho','Peshev','$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',1,0),(11,'anton@gmail.com','Anton','Antonov','$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',1,0),(12,'drago@gmail.com','Drago','Draganov','$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',1,0),(13,'pavla@gmail.com','Pavla','Dimitrova','$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',1,0);
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_access_section`
--

DROP TABLE IF EXISTS `users_access_section`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_access_section` (
  `user_id` int(11) NOT NULL,
  `section_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`section_id`),
  KEY `fk_users_has_section_section1_idx` (`section_id`),
  KEY `fk_users_has_section_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_has_section_section1` FOREIGN KEY (`section_id`) REFERENCES `sections` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_section_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_access_section`
--

LOCK TABLES `users_access_section` WRITE;
/*!40000 ALTER TABLE `users_access_section` DISABLE KEYS */;
INSERT INTO `users_access_section` VALUES (9,164);
/*!40000 ALTER TABLE `users_access_section` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users_enrolled_course`
--

DROP TABLE IF EXISTS `users_enrolled_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users_enrolled_course` (
  `user_id` int(11) NOT NULL,
  `course_id` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`course_id`),
  KEY `fk_users_has_course_course1_idx` (`course_id`),
  KEY `fk_users_has_course_users1_idx` (`user_id`),
  CONSTRAINT `fk_users_has_course_course1` FOREIGN KEY (`course_id`) REFERENCES `courses` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_users_has_course_users1` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users_enrolled_course`
--

LOCK TABLES `users_enrolled_course` WRITE;
/*!40000 ALTER TABLE `users_enrolled_course` DISABLE KEYS */;
INSERT INTO `users_enrolled_course` VALUES (9,89),(9,90),(9,91),(9,95),(10,93),(11,93);
/*!40000 ALTER TABLE `users_enrolled_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'e-learning'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2021-09-02  9:54:01
