import pool from './pool.js'

export const blacklistToken = async (token) => {
  await pool.query('insert into tokens (token) values (?)', [token])
}

export const tokenExists = async (token) => {
  const result = await pool.query('select * from tokens where token = ?', [token])

  return result.length > 0
}

export default {
  blacklistToken,
  tokenExists
}
