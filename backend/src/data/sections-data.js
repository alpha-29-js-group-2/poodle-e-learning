import pool from '../data/pool.js'
import userRole from '../common/user-role.js'
import { IS_NOT_DELETED, IS_DELETED, SECTIONS_ORDER_INCR } from '../common/constants.js'

/* if get all sections becomes needed
export const getAllSections = async () => {
    const sql = `select id, title, content, order_num, restriction_date, isDeleted, \
course_id from sections where isDeleted = ?`;
    const result = await pool.query(sql, [IS_NOT_DELETED]);
    return result;
} */

export const getAllSectionsFromACourseQuery = async (cid) => {
  const sql = 'select * from sections where course_id = ? and isDeleted = ? order by order_num'
  const result = await pool.query(sql, [cid, IS_NOT_DELETED])
  return result
}

export const getUsersWithAccessToSectionQuery = async (sectionId) => {
  const sql = `select user_id id from users_access_section a \
    inner join sections s on s.id = a.section_id
where section_id = ? 
    and isDeleted = ?`
  const result = await pool.query(sql, [sectionId, IS_NOT_DELETED])
  return result
}

export const getUsersWithoutAccessToSectionQuery = async (sectionId) => {
  const sql = `select distinct u.id, CONCAT(u.firstname, " ", u.lastname) as name, u.email from users u
    inner join users_enrolled_course c on u.id = c.user_id 
    inner join sections s on c.course_id = s.course_id 
    where u.role_id = ? and s.id = ? and
    u.id not in (
    select user_id id2 from users_access_section a 
        inner join sections s on s.id = a.section_id
    where a.section_id = ? 
        and isDeleted = ?)`
  const result = await pool.query(sql, [userRole.STUDENT, sectionId, sectionId, IS_NOT_DELETED])
  return result
}

export const getSectionById = async (sectionId) => {
  const sql = 'select * from sections where  id = ? and isDeleted = ?'
  const result = await pool.query(sql, [sectionId, IS_NOT_DELETED])
  if (result[0]) {
    return result[0]
  }

  return null
}

export const getSectionByIdIfDeleted = async (sectionId) => {
  const sql = 'select * from sections where  id = ? and isDeleted = ?'
  const result = await pool.query(sql, [sectionId, IS_DELETED])
  if (result[0]) {
    return result[0]
  }

  return null
}

export const getSectionByOrderNum = async (orderNum, courseId, isDeleted) => {
  isDeleted = isDeleted || IS_NOT_DELETED
  const params = [orderNum, courseId, isDeleted]
  const sql = `
            SELECT *
            FROM sections
            WHERE order_num = ? AND course_id = ? AND isDeleted = ?
            `
  const result = await pool.query(sql, params)
  return result[0]
}

export const getLastSectionOrder = async (courseId) => {
  const params = [courseId, IS_NOT_DELETED]
  const sql = `
            SELECT MAX(order_num) 
            FROM sections
            WHERE course_id = ? AND isDeleted = ?
            `
  const result = await pool.query(sql, params)
  const { 'MAX(order_num)': res } = result[0]
  return res
}

export const createSectionQuery = async (reqParams) => {
  let lastOrderNum = await getLastSectionOrder(parseInt(reqParams.courseId, 10))
  lastOrderNum = Math.floor(lastOrderNum / 10) * 10 + SECTIONS_ORDER_INCR || SECTIONS_ORDER_INCR
  const restrictionDate = reqParams.restrictionDate || null

  const embedded = parseInt(reqParams.isEmbedded, 10) || 0

  const params = [parseInt(reqParams.courseId, 10), reqParams.title, reqParams.encodedContent, lastOrderNum, restrictionDate, embedded]
  const sql = `
            INSERT INTO sections(course_id, title, content, order_num, restriction_date, isEmbedded)
            VALUES(?, ?, ?, ?, ?, ?)
            `

  await pool.query(sql, params)

  const result = await getSectionByOrderNum(lastOrderNum, parseInt(reqParams.courseId, 10))
  return result
}

export const updateSectionQuery = async (reqParams) => {
  const params = []
  const sqlParams = []

  if (!reqParams.title && !reqParams.encodedContent && !reqParams.restrictionDate && !reqParams.orderNum) {
    return {}
  }

  let sql = `
            UPDATE sections
            SET
            `

  if (reqParams.title) {
    sqlParams.push('title = ?')
    params.push(reqParams.title)
  }

  if (reqParams.encodedContent) {
    sqlParams.push('content = ?')
    params.push(reqParams.encodedContent)
  }

  if (reqParams.orderNum) {
    sqlParams.push('order_num = ?')
    params.push(parseInt(reqParams.orderNum, 10))
  }

  if (reqParams.restrictionDate || reqParams.restrictionDate === null) {
    sqlParams.push('restriction_date = ?')
    params.push(reqParams.restrictionDate)
  }

  if (reqParams.isEmbedded === 1 || reqParams.isEmbedded === 0) {
    sqlParams.push('isEmbedded = ?')
    params.push(reqParams.isEmbedded)
  }

  sql += sqlParams.join(',')
  sql += ` 
        WHERE id = ?`

  params.push(reqParams.sectionId)

  await pool.query(sql, params)

  const updatedSection = await getSectionById(reqParams.sectionId)

  return updatedSection
}

export const deleteSection = async (sectionId) => {
  const sql = `
    update sections
    set isDeleted = ?
    where id = ?;
  `

  await pool.query(sql, [IS_DELETED, sectionId])

  const result = await getSectionByIdIfDeleted(sectionId)
  return result
}

export const isSectionRestrictedForStudentsQuery = async (sectionId) => {
  const sql = `
    select * from users_access_section
    where section_id = ?`

  const result = await pool.query(sql, [sectionId])

  if (result[0]) {
    return true
  }
  return false
}

export const isStudentGrantedAccessToSectionQuery = async (sectionId, userId) => {
  const sql = `
    select * from users_access_section
    where section_id = ? and user_id = ?`

  const result = await pool.query(sql, [sectionId, userId])

  if (result[0]) {
    return true
  }
  return false
}

export const sectionExistsById = async (id) => {
  return !!(await getSectionById(id))
}
