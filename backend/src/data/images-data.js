import pool from './pool.js'
import coursesData from '../data/courses-data.js'

const updateCourseImageQuery = async (id, filename) => {
  const params = [filename, id]
  const sql = `
            UPDATE courses
            SET
            image = ?
            WHERE id = ?
            `

  await pool.query(sql, params)
  const updatedCourse = await coursesData.getCourseByQuery('id', id)
  return updatedCourse
}

const getCourseImageQuery = async (id) => {
  const sql = `
        SELECT image
        FROM courses
        WHERE id = ?;
    `
  const result = await pool.query(sql, [id])
  return result[0]
}

export default {
  updateCourseImageQuery,
  getCourseImageQuery
}
