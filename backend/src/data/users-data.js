import pool from '../data/pool.js'
import bcrypt from 'bcrypt'
import userRole from '../common/user-role.js'
import { IS_NOT_DELETED, IS_DELETED, GET_STUDENTS_QUERY_LIMIT } from '../common/constants.js'

export const getAllUsers = async () => {
  return await pool.query(`
      select id, email, firstname, lastname, role_id
      from users u where u.isDeleted = ?;
    `, [IS_NOT_DELETED])
}

export const getAllStudents = async () => {
  return await pool.query(`select id, firstname, lastname, email, 
    role_id, isDeleted from users u where u.role_id = ? and u.isDeleted = ?`,
  [userRole.STUDENT, IS_NOT_DELETED])
}

export const getAllDeletedStudents = async () => {
  return await pool.query(`select id, firstname, lastname, email, 
    role_id, isDeleted from users u where u.role_id = ? and u.isDeleted = ?`,
  [userRole.STUDENT, IS_DELETED])
}

export const getAllTeachers = async () => {
  return await pool.query(`select id, firstname, lastname, email, 
    role_id from users u where u.role_id = ? and u.isDeleted = ?`,
  [userRole.TEACHER, IS_NOT_DELETED])
}

export const getAuthorByCourseIdQuery = async (cid) => {
  const result = await pool.query(
    `Select CONCAT(u.firstname, " ", u.lastname) as name, u.id as id from users u 
        inner join courses c where c.creator_id = u.id and c.id=?`, [cid])
  if (result[0]) {
    return result[0]
  };

  return null
}

export const getAllEnrolledStudents = async (cid, searchTerm) => {
  const search = searchTerm && searchTerm !== ''
  return await pool.query(`SELECT u.id, u.email, \
CONCAT(u.firstname, " ", u.lastname) as name from \
users u inner join users_enrolled_course c
    on u.id = c.user_id  where u.role_id=? and c.course_id = ? and isDeleted =? 
    ${search
      ? `having (name like '%${decodeURIComponent(searchTerm)}%' or
    u.email like '%${decodeURIComponent(searchTerm)}%')`
      : ''}`, [userRole.STUDENT, cid, IS_NOT_DELETED])
}

export const getNotEnrolledStudentsQuery = async (cid, searchTerm) => {
  const search = searchTerm && searchTerm !== ''
  return await pool.query(`select u.id, u.email, 
    CONCAT(u.firstname, " ", u.lastname) as name 
    from users u
    where u.role_id = ? and u.isDeleted = ? and
    u.id not in (SELECT u2.id from users
         u2 inner join users_enrolled_course c
        on u2.id = c.user_id  where c.course_id = ?) 
    ${search
      ? `having (name like '%${decodeURIComponent(searchTerm)}%' or
    u.email like '%${decodeURIComponent(searchTerm)}%')`
      : ''}
        Limit ${GET_STUDENTS_QUERY_LIMIT}`,
  [userRole.STUDENT, IS_NOT_DELETED, cid])
}

export const getAllNotEnrolledStudents = async (cid) => {
  return await pool.query(`select u.id, u.email, \
CONCAT(u.firstname, " ", u.lastname) as name from users u
    where u.role_id = ? and u.isDeleted = ? and\
u.id not in (SELECT u2.id from users
         u2 inner join users_enrolled_course c
        on u2.id = c.user_id  where c.course_id = ?)`,
  [userRole.STUDENT, IS_NOT_DELETED, cid])
}

export const getUserById = async (id) => {
  const result = await pool.query(`select id, email, firstname,
     lastname, role_id, password from users u where u.id = ? and
     u.isDeleted = ?`, [id, IS_NOT_DELETED])
  if (result[0]) {
    return result[0]
  }

  return null
}

export const getUserByEmail = async (email) => {
  const result = await pool.query(`select id, firstname, lastname, email, role_id, password
from users u where u.email = ? and u.isDeleted = ?`, [email, IS_NOT_DELETED])
  if (result[0]) {
    return result[0]
  }

  return null
}

export const userExistsByEmail = async (email) => {
  return !!(await getUserByEmail(email))
}

export const userExistsById = async (id) => {
  return !!(await getUserById(id))
}

export const isStudentEnrolled = async (studentId, cid) => {
  const result = await pool.query(`select 
    id, firstname, lastname, email, role_id
    from users u
    inner join 
    users_enrolled_course c on u.id = c.user_id where u.id = ? 
    and u.isDeleted = ? and c.course_id = ?`, [studentId, IS_NOT_DELETED, cid])
  if (result[0]) {
    return result[0]
  }

  return null
}

export const getStudentByIdIfDeleted = async (id) => {
  const result = await pool.query(`select id, email, firstname,
     lastname, role_id, isDeleted from users u where u.id = ? and
     u.isDeleted = ?`, [id, IS_DELETED])
  if (result[0]) {
    return result[0]
  }
  return null
}

export const isStudentDeleted = async (id) => {
  const result = !!(await getStudentByIdIfDeleted(id))

  return result
}

export const isUserATeacher = async (id) => {
  const result = await pool.query(`select * from users u where u.id = ? and
     u.role_id = ?`, [id, userRole.TEACHER])
  if (result[0]) {
    return true
  }
  return false
}

export const isStudentGrantedAccessQuery = async (studentId, sectionId) => {
  const result = await pool.query('select * from users_access_section where user_id = ? and section_id = ?', [studentId, sectionId])
  if (result[0]) {
    return true
  }
  return false
}

export const enrollStudent = async (studentId, cid) => {
  const sql = `
    insert into users_enrolled_course (user_id, course_id)
    values (?, ?);
  `
  await pool.query(sql, [studentId, cid])
  return {
    message:
      `User with id ${studentId} successfully enrolled in \
course with id ${cid}`
  }
}

export const unEnrollStudent = async (studentId, cid) => {
  const sql = `
    delete from users_enrolled_course where
    user_id = ? and course_id = ?;
  `
  await pool.query(sql, [studentId, cid])
  return {
    message:
      `User with id ${studentId} successfully removed from \
course with id ${cid}`
  }
}

export const grantAccessToSectionQuery = async (studentId, sectionId) => {
  const sql = `
    insert into users_access_section (user_id, section_id)
    values (?, ?);
  `
  await pool.query(sql, [studentId, sectionId])
  return {
    message:
      `User with id ${studentId} successfully given access to \
section with id ${sectionId}`
  }
}

export const revokeAccessToSectionQuery = async (studentId, sectionId) => {
  const sql = `
    delete from users_access_section where user_id=? and section_id=?;
  `
  await pool.query(sql, [studentId, sectionId])
  return {
    message:
      `User with id ${studentId} successfully revoked access to \
section with id ${sectionId}`
  }
}

export const createStudent = async (user) => {
  user.password = await bcrypt.hash(user.password, 10)
  const sql = `
    insert into users (email, password, firstname, lastname, role_id)
    values (?, ?, ?, ?, ?);
  `
  const insert = await pool.query(sql,
    [user.email, user.password, user.firstname, user.lastname, userRole.STUDENT])

  const sql2 = `
    select email, firstname, lastname, role_id
    from users u
    where u.id = ?;
  `
  const createdStudent = (await pool.query(sql2, [insert.insertId]))[0]

  return createdStudent
}

export const deleteStudent = async (studentId) => {
  const sql = `
    update users u
    set isDeleted = ?
    where u.id = ?;
  `
  await pool.query(sql, [IS_DELETED, studentId])

  const result = await getStudentByIdIfDeleted(studentId)
  return result
}

export const unDeleteStudent = async (studentId) => {
  const sql = `
    update users u
    set isDeleted = ?
    where u.id = ?;
  `
  await pool.query(sql, [IS_NOT_DELETED, studentId])

  // eslint-disable-next-line camelcase
  const { email, firstname, lastname, isDeleted, role_id } = await getUserById(studentId)
  return { email, firstname, lastname, isDeleted, role_id }
}

export const getAllEnrolledCoursesQuery = async (studentId) => {
  const sql = `
    select course_id as enrolledCourses from users_enrolled_course
    where user_id = ?
    `
  const result = await pool.query(sql, [studentId])
  return result
}

export const updateUserQuery = async (newUser, userId) => {
  const params = []
  const sqlParams = []
  let sql = `
    update users
    set
  `

  if (newUser.email) {
    sqlParams.push('email = ?')
    params.push(newUser.email)
  }

  if (newUser.firstName) {
    sqlParams.push('firstname = ?')
    params.push(newUser.firstName)
  }

  if (newUser.lastName) {
    sqlParams.push('lastname = ?')
    params.push(newUser.lastName)
  }

  sql += sqlParams.join(',')
  sql += ` 
    where id = ?
    `
  params.push(userId)

  await pool.query(sql, params)

  const updatedUser = await getUserById(userId)

  return updatedUser
}

export const updatePassword = async (userId, passwordNew) => {
  passwordNew = await bcrypt.hash(passwordNew, 10)

  const sql = `
  update users
  set password = ?
  where id = ?;
  `

  await pool.query(sql, [passwordNew, userId])

  // return 'Password updated successfully'
  const updatedUser = await getUserById(userId)

  return updatedUser
}
