import { DEFAULT_PAGE_LIMIT, DEFAULT_PAGE_OFFSET, IS_DELETED, IS_NOT_DELETED, IS_NOT_PUBLIC } from '../common/constants.js'
import pool from './pool.js'

const findCourseExistsQuery = async (column, value) => {
  return !!(await getCourseByQuery(column, value))
}

const getAllCoursesQuery = async (isPublic, pagination, userId) => {
  let { limit, offset } = pagination
  limit = parseInt(limit) || DEFAULT_PAGE_LIMIT
  offset = parseInt(offset) || DEFAULT_PAGE_OFFSET
  const params = [IS_NOT_DELETED]

  let sql = `
            SELECT * 
            FROM courses
            WHERE isDeleted = ?
            `

  if (isPublic) {
    sql += ' AND isPublic = ?'
    params.push(parseInt(isPublic, 10))
  }

  if (userId) {
    sql += ' AND creator_id = ?'
    params.push(userId)
  }

  sql += ` ORDER BY title
        LIMIT ?
        OFFSET ?
        `
  params.push(limit, offset)

  const result = await pool.query(sql, params)
  return result
}

const getAllSearchedCoursesQuery = async (column, value, isPublic, pagination, userId) => {
  let { limit, offset } = pagination
  limit = parseInt(limit) || DEFAULT_PAGE_LIMIT
  offset = parseInt(offset) || DEFAULT_PAGE_OFFSET
  const params = [IS_NOT_DELETED]

  let sql = `SELECT * 
            FROM courses
            WHERE ${column} LIKE '%${decodeURIComponent(value)}%' AND isDeleted = ?`

  if (isPublic) {
    sql += ' AND isPublic = ?'
    params.push(parseInt(isPublic, 10))
  }

  if (userId) {
    sql += ' AND creator_id = ?'
    params.push(userId)
  }

  sql += ` ORDER BY title
        LIMIT ?
        OFFSET ?`
  params.push(limit, offset)

  const result = await pool.query(sql, params)
  return result
}

const getCourseByQuery = async (column, value, isDeleted) => {
  isDeleted = isDeleted || IS_NOT_DELETED
  const params = [value, isDeleted]
  const sql = `
            SELECT *
            FROM courses
            WHERE ${column} = ? AND isDeleted = ?
            `

  const result = await pool.query(sql, params)
  return result[0]
}

const getAllCoursesByStudentQuery = async (userId, pagination, search) => {
  let { limit, offset } = pagination
  limit = parseInt(limit) || DEFAULT_PAGE_LIMIT
  offset = parseInt(offset) || DEFAULT_PAGE_OFFSET
  const params = [userId, IS_NOT_DELETED, limit, offset]
  let sql
  if (!search) {
    sql = `
            SELECT * FROM courses AS c
            JOIN users_enrolled_course AS uc 
            ON uc.course_id = c.id
            WHERE uc.user_id = ? AND c.isDeleted = ?
            ORDER BY c.title
            LIMIT ?
            OFFSET ?
    `
  } else {
    sql = `
            SELECT * FROM courses AS c
            JOIN users_enrolled_course AS uc 
            ON uc.course_id = c.id
            WHERE uc.user_id = ? AND c.isDeleted = ? AND c.title LIKE '%${search}%'
            ORDER BY c.title
            LIMIT ?
            OFFSET ?
    `
    params.push(search)
  }
  const result = await pool.query(sql, params)
  return result
}

const getCourseByStudentQuery = async (id, userId) => {
  const params = [userId, id, IS_NOT_DELETED]

  const sql = `
            SELECT * FROM courses AS c
            JOIN users_enrolled_course AS uc 
            ON uc.course_id = c.id
            WHERE uc.user_id = ? AND c.id = ? AND c.isDeleted = ?
    `

  const result = await pool.query(sql, params)
  return result[0]
}

const getAllCoursesCountQuery = async (isPublic, column, value, userId) => {
  const params = [IS_NOT_DELETED]

  let sql = `
            SELECT COUNT(*) FROM courses
            WHERE isDeleted = ?
            `

  if (column && value) {
    sql += ` AND ${column} LIKE '%${value}%'`
  }

  if (isPublic) {
    sql += ' AND isPublic = ?'
    params.push(parseInt(isPublic, 10))
  }

  if (userId) {
    sql += ' AND creator_id = ?'
    params.push(userId)
  }

  const result = await pool.query(sql, params)
  const { 'COUNT(*)': countNum } = result[0]
  return countNum
}

const getAllDeletedCoursesCountQuery = async (column, value) => {
  const params = [IS_DELETED]

  let sql = `
            SELECT COUNT(*) FROM courses
            WHERE isDeleted = ?
            `

  if (column && value) {
    sql += ` AND ${column} LIKE '%${value}%'`
  }

  const result = await pool.query(sql, params)
  const { 'COUNT(*)': countNum } = result[0]
  return countNum
}

const getAllCoursesByStudentCountQuery = async (userId, search) => {
  const params = [userId, IS_NOT_DELETED]
  let sql
  if (!search) {
    sql = `
            SELECT COUNT(*) FROM courses AS c
            JOIN users_enrolled_course AS uc 
            ON uc.course_id = c.id
            WHERE uc.user_id = ? AND c.isDeleted = ?
            ORDER BY c.title
            `
  } else {
    sql = `
            SELECT COUNT(*) FROM courses AS c
            JOIN users_enrolled_course AS uc 
            ON uc.course_id = c.id
            WHERE uc.user_id = ? AND c.isDeleted = ? AND c.title LIKE '%${search}%'
            ORDER BY c.title
            `
    params.push(search)
  }
  const result = await pool.query(sql, params)
  const { 'COUNT(*)': countNum } = result[0]
  return countNum
}

const getAllDeletedCoursesQuery = async (search, pagination) => {
  let { limit, offset } = pagination
  limit = parseInt(limit) || DEFAULT_PAGE_LIMIT
  offset = parseInt(offset) || DEFAULT_PAGE_OFFSET
  const params = [IS_DELETED]

  let sql = `
            SELECT * 
            FROM courses
            WHERE isDeleted = ?
            `

  if (search) {
    // sql += ` AND title LIKE '%${decodeURIComponent(search)}%'`;
    sql += ` AND title LIKE '%${search}%'`
  }

  sql += ` ORDER BY title
            LIMIT ?
            OFFSET ?`
  params.push(limit, offset)

  const result = await pool.query(sql, params)
  return result
}

const createCourseQuery = async (title, description, isPublic, userId) => {
  isPublic = parseInt(isPublic, 10) || IS_NOT_PUBLIC
  const params = [title, description, isPublic, userId]

  const sql = `
            INSERT INTO courses(title, description, isPublic, creator_id)
            VALUES(?, ?, ?, ?)
            `

  await pool.query(sql, params)
  const result = await getCourseByQuery('title', title)

  return result
}

const updateSectionsOrderQuery = async (order) => {
  order.forEach(async (id, index) => {
    await pool.query('update sections set order_num = ? where id = ?;', [index + 1, id])
  })
  return 'Order updated'
}

const updateCourseQuery = async (title, description, isPublic, id, isDeleted) => {
  const params = [title]

  let sql = `
            UPDATE courses
            SET
            title = ?
            `

  if (description) {
    sql += `, description = ?
            `
    params.push(description)
  }
  if (isPublic) {
    sql += `, isPublic = ?
        `
    params.push(parseInt(isPublic, 10))
  }
  if (isDeleted) {
    sql += `, isDeleted = ?
            `
    params.push(IS_NOT_DELETED)
  }

  sql += ' WHERE id = ?'
  params.push(id)
  await pool.query(sql, params)

  const updatedCourse = await getCourseByQuery('id', id)
  return updatedCourse
}

const deleteCourseQuery = async (id) => {
  const courseToDelete = await getCourseByQuery('id', id)
  const dateDeleted = new Date()
  const updatedTitle = courseToDelete.title + ' Deleted on: ' + dateDeleted.toLocaleDateString() + ' ' + dateDeleted.toLocaleTimeString()

  const params = [updatedTitle, IS_DELETED, id]

  const sql = `
            UPDATE courses
            SET
            title = ?,
            isDeleted = ?
            WHERE id = ?
            `
  await pool.query(sql, params)

  const deletedCourse = await getCourseByQuery('id', id, IS_DELETED)

  return deletedCourse
}

const updateDeletedCourseQuery = async (title, description, isPublic, id, isDeleted) => {
  const params = [title, description, parseInt(isPublic, 10), !isDeleted, id]

  const sql = `
            UPDATE courses
            SET
            title = ?,
            description = ?,
            isPublic = ?,
            isDeleted = ?
            WHERE id = ?
            `
  await pool.query(sql, params)

  const updatedCourse = await getCourseByQuery('id', id)
  return updatedCourse
}

const searchEnrolledInCourse = async (courseId, userId) => {
  const params = [userId, courseId]

  const sql = `
            SELECT *
            FROM users_enrolled_course
            WHERE user_id = ? AND course_id = ?
            `
  const result = await pool.query(sql, params)
  return result[0]
}

const updateCourseIsLiked = async (courseId, userId, isLiked) => {
  const params = [!isLiked, courseId, userId]
  const sql = `
            UPDATE users_enrolled_course 
            SET 
            isLiked = ?
            WHERE
            course_id = ? AND user_id = ?
            `

  await pool.query(sql, params)
  const updatedIsLike = await searchEnrolledInCourse(courseId, userId)
  return updatedIsLike
}

const getCourseCreatorId = async (courseId) => {
  const params = [courseId, IS_NOT_DELETED]
  const sql = `
        SELECT DISTINCT c.creator_id
        FROM courses AS c 
        WHERE c.id = ? AND c.isDeleted = ?;
    `
  const result = await pool.query(sql, params)
  return result[0]
}

export default {
  getAllCoursesQuery,
  getAllSearchedCoursesQuery,
  getCourseByQuery,
  getAllCoursesByStudentQuery,
  getCourseByStudentQuery,
  getAllCoursesCountQuery,
  getAllCoursesByStudentCountQuery,
  getAllDeletedCoursesQuery,
  getAllDeletedCoursesCountQuery,
  createCourseQuery,
  findCourseExistsQuery,
  updateCourseQuery,
  deleteCourseQuery,
  searchEnrolledInCourse,
  updateCourseIsLiked,
  updateDeletedCourseQuery,
  getCourseCreatorId,
  updateSectionsOrderQuery
}
