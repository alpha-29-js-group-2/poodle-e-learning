export const IS_NOT_DELETED = 0

export const IS_DELETED = 1
export const MIN_TITLE_LENGTH = 5
export const MAX_TITLE_LENGTH = 100
export const MIN_DESCRIPTION_LENGTH = 10
export const MAX_DESCRIPTION_LENGTH = 800

export const MIN_CONTENT_LENGTH = 10

export const MAX_CONTENT_LENGTH = 100_000

export const MIN_FIRSTNAME_LENGTH = 3
export const MAX_FIRSTNAME_LENGTH = 45

export const MIN_LASTNAME_LENGTH = 3
export const MAX_LASTNAME_LENGTH = 45

export const MIN_EMAIL_LENGTH = 3
export const MAX_EMAIL_LENGTH = 320
// eslint-disable-next-line no-control-regex
export const EMAIL_REGEX = /(?:[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9]))\.){3}(?:(2(5[0-5]|[0-4][0-9])|1[0-9][0-9]|[1-9]?[0-9])|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/gm

export const MIN_PASSWORD_LENGTH = 6
export const MAX_PASSWORD_LENGTH = 128

export const GET_STUDENTS_QUERY_LIMIT = 8

export const IS_NOT_PUBLIC = 0

export const IS_PUBLIC = 1

export const DEFAULT_PAGE_LIMIT = 6

export const DEFAULT_PAGE_OFFSET = 0

export const IS_LIKED = 1
export const IS_NOT_LIKED = 0

export const SECTIONS_ORDER_INCR = 10

export const HTML_SANITIZER_ALLOWED_TAGS = ['img', 'iframe', 'font']

export const HTML_SANITIZER_ALLOWED_ATTRIBUTES = {
  a: ['href', 'name', 'target'],
  iframe: ['width', 'height', 'src', 'autoplay', 'title', 'frameBorder'],
  img: ['width', 'height', 'src', 'alt'],
  font: ['size', 'face']
}
