import passport from 'passport'
import { tokenExists } from '../services/auth-service.js'
import tokenData from '../data/auth-data.js'

const authMiddleware = async (req, res, next) => {
  const token = req.headers.authorization?.replace('Bearer ', '')
  if (!token) {
    return res.status(400).send({
      message: 'No token supplied.'
    })
  }
  if (!(await tokenExists(tokenData)(token))) {
    passport.authenticate('jwt', { session: false })(req, res, next)
  } else {
    return res.status(403).send({
      message: 'Invalid token.'
    })
  }
}

/**
 * roleId is the minimal clearance required to view the current resource
 */
const roleMiddleware = roleId => {
  return (req, res, next) => {
    if (req.user && +req.user.role >= +roleId) {
      next()
    } else {
      res.status(403).send({
        message: 'You are not authorized to access this resource.'
      })
    }
  }
}

export {
  authMiddleware,
  roleMiddleware
}
