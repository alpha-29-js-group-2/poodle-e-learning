export default (validator) => {
  return async (req, res, next) => {
    const body = req.body
    const validations = Object.keys(validator)
    const errors = validations.map(key => validator[key](body[key]))
    const fails = errors.filter(e => e.error === true)
    if (fails.length > 0) {
      res.status(400).send(fails)
      return
    }
    await next()
  }
}
