import {
  MIN_FIRSTNAME_LENGTH,
  MAX_FIRSTNAME_LENGTH,
  MIN_LASTNAME_LENGTH,
  MAX_LASTNAME_LENGTH
} from '../common/constants.js'

export default {
  firstName: value => {
    if (typeof value !== 'string' ||
              value.length < MIN_FIRSTNAME_LENGTH ||
              value.length > MAX_FIRSTNAME_LENGTH) {
      return {
        error: true,
        msg: `The first name must be between ${MIN_FIRSTNAME_LENGTH} \
  and ${MAX_FIRSTNAME_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  },
  lastName: value => {
    if (typeof value !== 'string' ||
              value.length < MIN_LASTNAME_LENGTH ||
              value.length > MAX_LASTNAME_LENGTH) {
      return {
        error: true,
        msg: `The last name must be between ${MIN_LASTNAME_LENGTH} \
  and ${MAX_LASTNAME_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  }
}
