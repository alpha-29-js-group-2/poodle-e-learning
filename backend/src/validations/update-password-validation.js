import {
  MIN_PASSWORD_LENGTH,
  MAX_PASSWORD_LENGTH
} from '../common/constants.js'
export default {
  comparePassword: value => {
    if (typeof value !== 'string' ||
                  value.length < MIN_PASSWORD_LENGTH ||
                  value.length > MAX_PASSWORD_LENGTH) {
      return {
        error: true,
        msg: `The password should be a string between \
      ${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  },
  newPassword: value => {
    if (typeof value !== 'string' ||
              value.length < MIN_PASSWORD_LENGTH ||
              value.length > MAX_PASSWORD_LENGTH) {
      return {
        error: true,
        msg: `The password should be a string between \
  ${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  }
}
