import { MAX_TITLE_LENGTH, MIN_TITLE_LENGTH, MAX_CONTENT_LENGTH, MIN_CONTENT_LENGTH } from '../common/constants.js'

// "courseId": 1,
// title
// "content": "<h1>Welcome</h1>",
// "restrictionDate": "2022-08-22 20:21:21.03",
// "orderNum": 107
export default {
  courseId: (value) => {
    if (!value) {
      return {
        error: true,
        msg: 'The section must have a courseId to belong to.'
      }
    }
    if (isNaN(parseInt(value, 10))) {
      return {
        error: true,
        msg: 'The section must have a courseId, which is a number.'
      }
    }
    return {
      error: false
    }
  },
  title: (value) => {
    if (
      value && typeof value === 'string' &&
      (value.length > MAX_TITLE_LENGTH ||
        value.length < MIN_TITLE_LENGTH)
    ) {
      return {
        error: true,
        msg: `The section must have a title between ${MIN_TITLE_LENGTH} and ${MAX_TITLE_LENGTH} symbols.`
      }
    }
    return {
      error: false
    }
  },
  content: (value) => {
    if (
      value && typeof value === 'string' &&
      (value.length > MAX_CONTENT_LENGTH ||
        value.length < MIN_CONTENT_LENGTH)
    ) {
      return {
        error: true,
        msg: `The section must have content between ${MIN_CONTENT_LENGTH} and ${MAX_CONTENT_LENGTH} symbols.`
      }
    }
    return {
      error: false
    }
  },
  restrictionDate: (value) => {
    // eslint-disable-next-line prefer-regex-literals
    const regExp = new RegExp('^(-?(?:[1-9][0-9]*)?[0-9]{4})-(1[0-2]|0[1-9])-(3[01]|0[1-9]|[12][0-9]) (2[0-3]|[01][0-9]):([0-5][0-9]):([0-5][0-9])?$')

    if (!value) {
      return {
        error: false
      }
    }
    if (!regExp.test(value)) {
      return {
        error: true,
        msg: 'The restrictionDate must be a date string in the format YYYY/MM/DD HH:MM:SS.'
      }
    }
    return {
      error: false
    }
  },
  orderNum: (value) => {
    if (value && isNaN(parseInt(value, 10))) {
      return {
        error: true,
        msg: 'Invalid order number - must be a number.'
      }
    }
    return {
      error: false
    }
  }
}
