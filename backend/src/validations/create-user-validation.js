import {
  MIN_FIRSTNAME_LENGTH,
  MAX_FIRSTNAME_LENGTH,
  MIN_LASTNAME_LENGTH,
  MAX_LASTNAME_LENGTH,
  MIN_EMAIL_LENGTH,
  MAX_EMAIL_LENGTH,
  EMAIL_REGEX,
  MIN_PASSWORD_LENGTH,
  MAX_PASSWORD_LENGTH
} from '../common/constants.js'

export default {
  firstname: value => {
    if (typeof value !== 'string' ||
            value.length < MIN_FIRSTNAME_LENGTH ||
            value.length > MAX_FIRSTNAME_LENGTH) {
      return {
        error: true,
        msg: `The first name must be between ${MIN_FIRSTNAME_LENGTH} \
and ${MAX_FIRSTNAME_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  },
  lastname: value => {
    if (typeof value !== 'string' ||
            value.length < MIN_LASTNAME_LENGTH ||
            value.length > MAX_LASTNAME_LENGTH) {
      return {
        error: true,
        msg: `The last name must be between ${MIN_LASTNAME_LENGTH} \
and ${MAX_LASTNAME_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  },
  email: (value) => {
    if (
      typeof value !== 'string' ||
            value.length > MAX_EMAIL_LENGTH ||
            value.length < MIN_EMAIL_LENGTH
    ) {
      return {
        error: true,
        msg: `The email must be a string  between \
${MIN_EMAIL_LENGTH} and ${MAX_EMAIL_LENGTH} symbols.`
      }
    }
    const test = value.match(EMAIL_REGEX)
    if (!test) {
      return {
        error: true,
        msg: 'The email must be valid'
      }
    }
    return {
      error: false
    }
  },
  password: value => {
    if (typeof value !== 'string' ||
         value.length < MIN_PASSWORD_LENGTH ||
          value.length > MAX_PASSWORD_LENGTH) {
      return {
        error: true,
        msg: `The password should be a string between \
${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  }
}
