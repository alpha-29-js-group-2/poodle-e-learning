import {
  MIN_EMAIL_LENGTH,
  MAX_EMAIL_LENGTH,
  EMAIL_REGEX,
  MIN_PASSWORD_LENGTH,
  MAX_PASSWORD_LENGTH
} from '../common/constants.js'
export default {
  email: (value) => {
    if (
      typeof value !== 'string' ||
            value.length > MAX_EMAIL_LENGTH ||
            value.length < MIN_EMAIL_LENGTH
    ) {
      return {
        error: true,
        msg: `The email must be a string  between \
${MIN_EMAIL_LENGTH} and ${MAX_EMAIL_LENGTH} symbols.`
      }
    }
    const test = value.match(EMAIL_REGEX)
    if (!test) {
      return {
        error: true,
        msg: 'The email must be valid'
      }
    }
    return {
      error: false
    }
  },
  password: value => {
    if (typeof value !== 'string' ||
            value.length < MIN_PASSWORD_LENGTH ||
            value.length > MAX_PASSWORD_LENGTH) {
      return {
        error: true,
        msg: `The password should be a string between \
${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} symbols.`
      }
    }

    return {
      error: false
    }
  }
}
