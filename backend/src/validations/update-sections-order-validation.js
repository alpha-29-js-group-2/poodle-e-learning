export default {
  order: (value) => {
    if (!value || !Array.isArray(value)) {
      return {
        error: true,
        msg:
                    'The array of section ids must be an non-empty array'
      }
    }

    for (let i = 0; i < value.length; i++) {
      const id = value[i]
      if (isNaN(id)) {
        return {
          error: true,
          msg:
                        `The section id at index ${i} is not a number.`
        }
      }
    }

    return {
      error: false
    }
  }
}
