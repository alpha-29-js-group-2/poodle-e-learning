import { MAX_TITLE_LENGTH, MIN_TITLE_LENGTH, MAX_DESCRIPTION_LENGTH, MIN_DESCRIPTION_LENGTH } from '../common/constants.js'
export default {
  title: (value) => {
    if (
      typeof value !== 'string' ||
            value.length > MAX_TITLE_LENGTH ||
            value.length < MIN_TITLE_LENGTH
    ) {
      return {
        error: true,
        field: 'title',
        msg: `The course must have a title between ${MIN_TITLE_LENGTH} and ${MAX_TITLE_LENGTH} symbols.`
      }
    }
    return {
      error: false
    }
  },
  description: (value) => {
    if (
      typeof value !== 'string' ||
            value.length > MAX_DESCRIPTION_LENGTH ||
            value.length < MIN_DESCRIPTION_LENGTH
    ) {
      return {
        error: true,
        field: 'description',
        msg: `The course must have a description between ${MIN_DESCRIPTION_LENGTH} and ${MAX_DESCRIPTION_LENGTH} symbols.`
      }
    }
    return {
      error: false
    }
  },
  isPublic: (value) => {
    if (!value) {
      return {
        error: false
      }
    }
    if (
      parseInt(value, 10) !== 0 &&
            parseInt(value, 10) !== 1
    ) {
      return {
        error: true,
        msg: 'The isPublic status must be 1 or 0.'
      }
    }
    return {
      error: false
    }
  }
}
