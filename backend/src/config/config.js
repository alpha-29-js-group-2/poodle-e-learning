import dotenv from 'dotenv'

const result = dotenv.config()
const PORT = result.parsed.PORT
const DB_HOST = result.parsed.DB_HOST
const DB_PORT = result.parsed.DB_PORT
const DB_USER = result.parsed.DB_USER
const DB_PASS = result.parsed.DB_PASS
const DATABASE = result.parsed.DATABASE

const SECRET_KEY = result.parsed.SECRET_KEY
const TOKEN_LIFETIME = result.parsed.TOKEN_LIFETIME

export {
  PORT,
  DB_HOST,
  DB_PORT,
  DB_USER,
  DB_PASS,
  DATABASE,
  SECRET_KEY,
  TOKEN_LIFETIME
}
