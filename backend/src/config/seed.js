import userRole from '../common/user-role.js'
import pool from '../data/pool.js'

const roles = [
  {
    id: 1,
    name: 'Student'
  },
  {
    id: 2,
    name: 'Teacher'
  }
]

const users = [
  {
    email: 'teacher@gmail.com',
    firstname: 'Tony',
    lastname: 'Daskalov',
    password: '$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',
    role_id: userRole.TEACHER
  },
  {
    email: 'teo@gmail.com',
    firstname: 'Teo',
    lastname: 'Atanasov',
    password: '$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',
    role_id: userRole.TEACHER
  },
  {
    email: 'student@gmail.com',
    firstname: 'Alex',
    lastname: 'Studentska',
    password: '$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',
    role_id: userRole.STUDENT
  },
  {
    email: 'pesho@gmail.com',
    firstname: 'Pesho',
    lastname: 'Peshev',
    password: '$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',
    role_id: userRole.STUDENT
  },
  {
    email: 'anton@gmail.com',
    firstname: 'Anton',
    lastname: 'Antonov',
    password: '$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',
    role_id: userRole.STUDENT
  },
  {
    email: 'drago@gmail.com',
    firstname: 'Drago',
    lastname: 'Draganov',
    password: '$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',
    role_id: userRole.STUDENT
  },
  {
    email: 'pavla@gmail.com',
    firstname: 'Pavla',
    lastname: 'Dimitrova',
    password: '$2b$10$xLmzmYTvzeB0uaa85O3I3ued69wJnUEuDV/auER9beYuzOaw4G9tO',
    role_id: userRole.STUDENT
  }
]

const courses = [
  {
    title: 'JavaScript for Beginners',
    description: 'The ultimate JavaScript course for absolute beginners.',
    isPublic: 1,
    creator_id: 1,
    image: '05024df299e5f651f0b20049d9a6e225'
  },
  {
    title: 'C# for Beginners',
    description: 'Master the basics of C# programming',
    isPublic: 1,
    creator_id: 1,
    image: '05024df299e5f651f0b20049d9a6e225'
  },
  {
    title: 'C++ Advanced - Part 1',
    description: 'Master C++ programming',
    isPublic: 0,
    creator_id: 1,
    image: '05024df299e5f651f0b20049d9a6e225'
  },
  {
    title: 'C++ Advanced - Part 2',
    description: 'Master C++ programming',
    isPublic: 0,
    creator_id: 1,
    image: '05024df299e5f651f0b20049d9a6e225'
  },
  {
    title: 'React for Advanced Developers',
    description: 'Everything you need to build SPAs with React',
    isPublic: 0,
    creator_id: 1,
    image: '05024df299e5f651f0b20049d9a6e225'
  },
  {
    title: 'Go for Advanced Developers',
    description: 'Level up your Go skills.',
    isPublic: 0,
    creator_id: 1,
    image: '05024df299e5f651f0b20049d9a6e225'
  },
  {
    title: 'Photography for teachers',
    description: 'The course is ideal for both curious students and adults.',
    isPublic: 1,
    creator_id: 2,
    image: '05024df299e5f651f0b20049d9a6e225'
  }
]

const sections = [
  {
    title: 'First steps with JavaScript',
    content: '<p>Make your first steps with JavaScript</p>',
    order_num: 10,
    course_id: 1,
    isEmbedded: 1
  },
  {
    title: 'Variables',
    content: '<iframe width="992" height="550" src="https://docs.google.com/presentation/d/e/2PACX-1vR9nPLcRE4_8KX_BBH3KSwHU1WvUrh-rMIefp4eH-_SYFqrc6yeP8eZbcNC4xGq-g/embed?start=false&loop=false&delayms=3000" frameborder="0" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe><p>Source: Telerik Academy</p>',
    order_num: 20,
    course_id: 1
  },
  {
    title: 'Variables and Data Types',
    content: '<iframe width="992" height="550" src="https://www.youtube.com/embed/edlFjlzxkSI" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Source: Dev Ed</p>',
    order_num: 30,
    course_id: 1
  },
  {
    title: 'Functions and Parameters',
    content: '<iframe width="992" height="550" src="https://www.youtube.com/embed/xjAu2Y2nJ34" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Source: Dev Ed</p>',
    order_num: 40,
    course_id: 1
  },
  {
    title: 'Let vs. Var',
    content: '<iframe width="992" height="550" src="https://www.youtube.com/embed/XgSjoHgy3Rk" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Source: Programming with Mosh</p>',
    order_num: 50,
    course_id: 1
  },
  {
    title: 'C++ Programming Tutorial 1 - Intro to C++',
    content: '<iframe width="992" height="550" src="https://www.youtube.com/embed/OTroAxvRNbw" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Source: Caleb Curry</p>',
    order_num: 10,
    course_id: 3
  },
  {
    title: 'C# - Starting with the basics',
    content: '<iframe width="992" height="550" src="https://www.youtube.com/embed/GhQdlIFylQ8" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Source: Free CodeCamp</p>',
    order_num: 10,
    course_id: 2
  },
  {
    title: 'C# Tutorial For Beginners',
    content: '<iframe width="992" height="550" src="https://www.youtube.com/embed/gfkTfcpWqAY" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe><p>Source: Programming with Mosh</p>',
    order_num: 10,
    course_id: 2
  },
  {
    title: 'Guidelines for photography',
    content: '<iframe src="https://drive.google.com/file/d/1HH1pdmISG9QOYQL-yllUIqjnpgbZEdCD/preview" width="992" height="780" allow="autoplay"></iframe><p>Source: Harvard Graduate School of Education</p>',
    order_num: 10,
    course_id: 7
  }
]

const usersEnrolled = [
  {
    user_id: 3,
    course_id: 1
  },
  {
    user_id: 3,
    course_id: 2
  },
  {
    user_id: 3,
    course_id: 3
  }
];

(async () => {
  for (const role of roles) {
    await pool.query(`
      INSERT INTO roles (id, name)
      values (?, ?)
    `, [role.id, role.name])
  }
  console.log('Roles were created!')

  for (const user of users) {
    await pool.query(`
      INSERT INTO users (email, firstname, lastname, password, role_id)
      values (?, ?, ?, ?, ?)
    `, [user.email, user.firstname, user.lastname, user.password, user.role_id])
  }
  console.log('Default users were created!')

  for (const course of courses) {
    await pool.query(`
      INSERT INTO courses (title, description, isPublic, creator_id, image)
      values (?, ?, ?, ?, ?)
    `, [course.title, course.description, course.isPublic, course.creator_id, course.image])
  }
  console.log('Default courses were created!')

  for (const section of sections) {
    await pool.query(`
      INSERT INTO sections (title, content, order_num, course_id)
      values (?, ?, ?, ?)
    `, [section.title, section.content, section.order_num, section.course_id])
  }
  console.log('Default sections were created!')

  for (const enrolled of usersEnrolled) {
    await pool.query(`
      INSERT INTO users_enrolled_course (user_id, course_id)
      values (?, ?)
    `, [enrolled.user_id, enrolled.course_id])
  }
  console.log('Default student was enrolled!')

  pool.end()
})()
