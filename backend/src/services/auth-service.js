import serviceErrors from '../common/service-errors.js'

export const blacklistToken = (tokenData) => async (token) => {
  try {
    if (await tokenData.tokenExists(token)) {
      return {
        error: serviceErrors.DUPLICATE_RECORD
      }
    }
    await tokenData.blacklistToken(token)
    return {
      error: null
    }
  } catch (e) {
    return {
      error: serviceErrors.SERVER_ERROR
    }
  }
}

export const tokenExists = (tokenData) => async (token) => {
  return await tokenData.tokenExists(token)
}
