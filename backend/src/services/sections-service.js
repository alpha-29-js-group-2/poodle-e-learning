import serviceErrors from '../common/service-errors.js'

/* if get all sections become needed
export const getAllSections = (sectionsData) => async () => {
  try {
    const data = await sectionsData.getAllSections();
    return {
      error: null,
      data,
    };
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null,
    };
  }
}; */

export const getAllSectionsFromACourse = (sectionsData, coursesData) => async (cid) => {
  try {
    const courseExists = await coursesData.findCourseExistsQuery('id', cid)

    if (!courseExists) {
      return { error: serviceErrors.RECORD_NOT_FOUND, data: null }
    }

    const data = await sectionsData.getAllSectionsFromACourseQuery(cid)
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.SERVER_ERROR,
      data: null
    }
  }
}

export const getUsersWithAccessToSection = (sectionsData) => async (sectionId) => {
  try {
    const exists = await sectionsData.sectionExistsById(sectionId)
    if (!exists) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        data: null
      }
    }

    const data = await sectionsData.getUsersWithAccessToSectionQuery(sectionId)

    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.SERVER_ERROR,
      data: null
    }
  }
}

export const getUsersWithoutAccessToSection = (sectionsData) => async (sectionId) => {
  try {
    const exists = await sectionsData.sectionExistsById(sectionId)
    if (!exists) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        data: null
      }
    }

    const data = await sectionsData.getUsersWithoutAccessToSectionQuery(sectionId)

    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.SERVER_ERROR,
      data: null
    }
  }
}

export const getSectionById = (sectionsData) => async (sectionId) => {
  try {
    const data = await sectionsData.getSectionById(sectionId)
    if (data === null) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        data: null
      }
    }
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.SERVER_ERROR,
      data: null
    }
  }
}

export const createSection =
  (sectionsData, coursesData) => async (reqParams, userId) => {
    try {
      const courseExists = await coursesData.findCourseExistsQuery(
        'id',
        reqParams.courseId
      )

      if (!courseExists) {
        return { error: serviceErrors.RECORD_NOT_FOUND, data: null }
      }

      const { creator_id: courseCreator } =
        await coursesData.getCourseCreatorId(reqParams.courseId)

      if (courseCreator !== userId) {
        return { error: serviceErrors.OPERATION_NOT_PERMITTED, data: null }
      }
      const section = await sectionsData.createSectionQuery(reqParams)

      return { error: null, data: section }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }

export const updateSection =
  (sectionsData, coursesData) => async (reqParams, userId) => {
    try {
      const courseExists = await coursesData.findCourseExistsQuery(
        'id',
        reqParams.courseId
      )

      if (!courseExists) {
        return { error: serviceErrors.RECORD_NOT_FOUND, course: null }
      }

      const sectionExists = await sectionsData.getSectionById(
        reqParams.sectionId
      )

      if (!sectionExists) {
        return { error: serviceErrors.RECORD_NOT_FOUND, section: null }
      }

      const { creator_id: courseCreator } =
        await coursesData.getCourseCreatorId(reqParams.courseId)

      if (courseCreator !== userId) {
        return { error: serviceErrors.OPERATION_NOT_PERMITTED, section: null }
      }

      if (reqParams.orderNum) {
        const duplicateOrder = await sectionsData.getSectionByOrderNum(
          reqParams.orderNum,
          reqParams.courseId
        )

        if (duplicateOrder) {
          return { error: serviceErrors.DUPLICATE_RECORD, section: null }
        }
      }

      const updatedSection = await sectionsData.updateSectionQuery(reqParams)

      return { error: null, section: updatedSection }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }

export const deleteSection =
  (sectionsData, coursesData) => async (sectionId, userId) => {
    try {
      const section = await sectionsData.getSectionById(sectionId)

      if (!section) {
        return {
          error: serviceErrors.RECORD_NOT_FOUND
        }
      }

      const { creator_id: courseCreator } =
        await coursesData.getCourseCreatorId(section.course_id)

      if (courseCreator !== userId) {
        return { error: serviceErrors.OPERATION_NOT_PERMITTED, data: null }
      }

      const result = await sectionsData.deleteSection(sectionId)

      return {
        error: null,
        data: result
      }
    } catch (e) {
      return {
        error: serviceErrors.SERVER_ERROR,
        data: null
      }
    }
  }

export const isSectionRestrictedForStudents = (sectionsData) => async (sectionId) => {
  return await sectionsData.isSectionRestrictedForStudentsQuery(sectionId)
}

export const isStudentGrantedAccessToSection = (sectionsData) => async (sectionId, userId) => {
  return await sectionsData.isStudentGrantedAccessToSectionQuery(sectionId, userId)
}
