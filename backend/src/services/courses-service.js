import serviceErrors from '../common/service-errors.js'
import userRole from '../common/user-role.js'

const getAllCourses = (coursesData) => {
  return async (search, isPublic, pagination, role) => {
    try {
      if (
        (!isPublic && role === userRole.TEACHER) ||
        (parseInt(isPublic, 10) === 0 && role === userRole.TEACHER) ||
        parseInt(isPublic, 10) === 1
      ) {
        const results = search
          ? await coursesData.getAllSearchedCoursesQuery(
            'title',
            search,
            isPublic,
            pagination
          )
          : await coursesData.getAllCoursesQuery(isPublic, pagination)

        // For pagination -> we need total # of results
        const totalNumCourses = search
          ? await coursesData.getAllCoursesCountQuery(isPublic, 'title', search)
          : await coursesData.getAllCoursesCountQuery(isPublic)

        const allCourses = { results }
        allCourses.total = totalNumCourses

        if (allCourses.results.length === 0) {
          return { error: serviceErrors.RECORD_NOT_FOUND, courses: null }
        }

        return { error: null, courses: allCourses }
      }
      return { error: serviceErrors.OPERATION_NOT_PERMITTED, courses: null }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const getCourseById = (coursesData, usersData) => {
  return async (id, role, userId) => {
    try {
      if (role === userRole.STUDENT) {
        const foundCourse = await coursesData.getCourseByQuery('id', id)

        if (!foundCourse) {
          return { error: serviceErrors.RECORD_NOT_FOUND, course: null }
        }

        const enrolledInCourse = await coursesData.getCourseByStudentQuery(id, userId)

        if (foundCourse.isPublic === 0) {
          if (enrolledInCourse) {
            return { error: null, course: foundCourse }
          }
          return { error: serviceErrors.OPERATION_NOT_PERMITTED, course: null }
        }

        if (foundCourse.isPublic === 1) {
          if (enrolledInCourse) {
            return { error: null, course: foundCourse }
          }

          await usersData.enrollStudent(userId, id)
          return { error: null, course: foundCourse }
        }
      } else if (role === userRole.TEACHER) {
        const foundCourse = await coursesData.getCourseByQuery('id', id)
        if (!foundCourse) {
          return { error: serviceErrors.RECORD_NOT_FOUND, course: null }
        }
        return { error: null, course: foundCourse }
      }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    };
  }
}

const getAllCoursesByUser = (coursesData) => {
  return async (userId, pagination, role, search) => {
    try {
      let results = []
      let totalNumCourses

      if (role === userRole.STUDENT) {
        results = await coursesData.getAllCoursesByStudentQuery(
          userId,
          pagination,
          search
        )
      } else if (role === userRole.TEACHER) {
        results = search
          ? await coursesData.getAllSearchedCoursesQuery(
            'title',
            search,
            null,
            pagination,
            userId
          )
          : await coursesData.getAllCoursesQuery(
            null,
            pagination,
            userId
          )
      }
      if (results.length === 0) {
        return { error: serviceErrors.RECORD_NOT_FOUND, courses: null }
      }

      if (role === userRole.STUDENT) {
        totalNumCourses = await coursesData.getAllCoursesByStudentCountQuery(
          userId,
          search
        )
      } else if (role === userRole.TEACHER) {
        totalNumCourses = search
          ? await coursesData.getAllCoursesCountQuery(
            null,
            'title',
            search,
            userId
          )
          : await coursesData.getAllCoursesCountQuery(
            null,
            null,
            null,
            userId
          )
      }
      const allCoursesByUser = { results }
      allCoursesByUser.total = totalNumCourses

      return { error: null, courses: allCoursesByUser }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const createCourse = (coursesData) => {
  return async (title, description, isPublic, userId) => {
    try {
      const duplicateTitle = await coursesData.findCourseExistsQuery('title', title)

      if (duplicateTitle) {
        return { error: serviceErrors.DUPLICATE_RECORD, course: null }
      }

      const newCourse = await coursesData.createCourseQuery(title, description, isPublic, userId)

      return { error: null, course: newCourse }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    };
  }
}

const updateSectionsOrder = (coursesData, sectionsData) => {
  return async (cid, order, userId) => {
    try {
      const foundCourse = await coursesData.getCourseByQuery('id', cid)

      if (!foundCourse) {
        return ({ error: serviceErrors.RECORD_NOT_FOUND, course: null })
      }

      if (foundCourse.creator_id !== userId) {
        return { error: serviceErrors.OPERATION_NOT_PERMITTED, course: null }
      }

      const courseSections = await sectionsData.getAllSectionsFromACourseQuery(cid)

      if (courseSections.length !== order.length) {
        throw new Error()
      }

      const updatedSectionOrder = await coursesData.updateSectionsOrderQuery(order)

      return { error: null, course: updatedSectionOrder }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const updateCourse = (coursesData) => {
  return async (title, description, isPublic, id, userId, isDeleted) => {
    try {
      const foundCourse = await coursesData.getCourseByQuery('id', id, isDeleted)

      if (!foundCourse) {
        return ({ error: serviceErrors.RECORD_NOT_FOUND, course: null })
      }

      if (foundCourse.creator_id !== userId) {
        return { error: serviceErrors.OPERATION_NOT_PERMITTED, course: null }
      }

      const courseSameTitle = await coursesData.getCourseByQuery('title', title)

      if (courseSameTitle && courseSameTitle.id !== foundCourse.id) {
        return { error: serviceErrors.DUPLICATE_RECORD, course: null }
      }

      const updatedCourse = await coursesData.updateCourseQuery(title, description, isPublic, id, isDeleted)

      return { error: null, course: updatedCourse }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    };
  }
}

const deleteCourse = (coursesData) => {
  return async (id, userId) => {
    try {
      const foundCourse = await coursesData.getCourseByQuery('id', id)

      if (!foundCourse) {
        return { error: serviceErrors.RECORD_NOT_FOUND, course: null }
      }
      if (foundCourse.creator_id !== userId) {
        return { error: serviceErrors.OPERATION_NOT_PERMITTED, course: null }
      }

      const deletedCourse = await coursesData.deleteCourseQuery(id)

      return { error: null, course: deletedCourse }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const updateCourseIsLiked = (coursesData) => {
  return async (courseId, userId) => {
    try {
      const course = await coursesData.getCourseByQuery('id', courseId)

      if (!course) {
        return { error: serviceErrors.RECORD_NOT_FOUND }
      }

      const userEnrolled = await coursesData.searchEnrolledInCourse(
        courseId,
        userId
      )

      if (userEnrolled) {
        const updatedIsLiked = await coursesData.updateCourseIsLiked(
          courseId,
          userId,
          userEnrolled.isLiked
        )
        return { error: null, isLiked: updatedIsLiked }
      }

      return { error: serviceErrors.OPERATION_NOT_PERMITTED, isLiked: null }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const getAllDeletedCourses = (coursesData) => {
  return async (search, pagination) => {
    try {
      const results = await coursesData.getAllDeletedCoursesQuery(
        search,
        pagination
      )

      const totalNumCourses = search
        ? await coursesData.getAllDeletedCoursesCountQuery('title', search)
        : await coursesData.getAllDeletedCoursesCountQuery()

      const allCourses = { results }
      allCourses.total = totalNumCourses

      if (allCourses.length === 0) {
        return { error: serviceErrors.RECORD_NOT_FOUND, courses: null }
      }

      return { error: null, courses: allCourses }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const getDeletedCourseById = (coursesData) => {
  return async (id, isDeleted) => {
    try {
      const foundCourse = await coursesData.getCourseByQuery(
        'id',
        id,
        isDeleted
      )

      if (!foundCourse) {
        return { error: serviceErrors.RECORD_NOT_FOUND, course: null }
      }

      return { error: null, course: foundCourse }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const updateDeletedCourseById = (coursesData) => {
  return async (title, description, isPublic, id, isDeleted) => {
    try {
      const foundCourse = await coursesData.getCourseByQuery(
        'id',
        id,
        isDeleted
      )

      if (!foundCourse) {
        return { error: serviceErrors.RECORD_NOT_FOUND, course: null }
      }
      const courseSameTitle = await coursesData.getCourseByQuery(
        'title',
        title
      )

      if (courseSameTitle && courseSameTitle.id !== foundCourse.id) {
        return { error: serviceErrors.DUPLICATE_RECORD, course: null }
      }

      const updatedCourse = await coursesData.updateDeletedCourseQuery(
        title,
        description,
        isPublic,
        id,
        isDeleted
      )
      return { error: null, course: updatedCourse }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    }
  }
}

const courseIsPublic = (coursesData) => {
  return async (cid) => {
    try {
      const foundCourse = await coursesData.getCourseByQuery('id', cid)

      if (!foundCourse) {
        return { error: serviceErrors.RECORD_NOT_FOUND, data: null }
      }

      const isPublic = foundCourse.isPublic

      return {
        error: null,
        data: !!(isPublic)
      }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR, data: null }
    }
  }
}

export default {
  getAllCourses,
  getCourseById,
  getAllCoursesByUser,
  createCourse,
  updateCourse,
  deleteCourse,
  updateCourseIsLiked,
  getAllDeletedCourses,
  getDeletedCourseById,
  updateDeletedCourseById,
  courseIsPublic,
  updateSectionsOrder
}
