import serviceErrors from '../common/service-errors.js'
import fs from 'fs'

const updateCourseImage = (imagesData) => {
  return async (id, filename) => {
    try {
      const updatedCourse = await imagesData.updateCourseImageQuery(id, filename)
      return { error: null, course: updatedCourse }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    };
  }
}

const getCourseImage = (imagesData) => {
  return async (id) => {
    try {
      const course = await imagesData.getCourseImageQuery(id)
      const image = fs.readFileSync(`./uploads/${course.image}`)
      return { error: null, image: image }
    } catch (e) {
      return { error: serviceErrors.SERVER_ERROR }
    };
  }
}

export default {
  updateCourseImage,
  getCourseImage
}
