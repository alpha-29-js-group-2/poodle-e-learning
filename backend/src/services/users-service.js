// import userRole from "../common/user-role.js";
import serviceErrors from '../common/service-errors.js'
import bcrypt from 'bcrypt'

export const getAllUsers = (usersData) => async () => {
  try {
    const data = await usersData.getAllUsers()
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getAllStudents = (usersData) => async () => {
  try {
    const data = await usersData.getAllStudents()
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getAllTeachers = (usersData) => async () => {
  try {
    const data = await usersData.getAllTeachers()
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getAuthorByCourseId = (usersData) => async (cid) => {
  try {
    const data = await usersData.getAuthorByCourseIdQuery(cid)
    if (data === null) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        data: null
      }
    }

    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getAllEnrolledStudents = (usersData) => async (cid, searchTerm) => {
  try {
    const data = await usersData.getAllEnrolledStudents(cid, searchTerm)
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getNotEnrolledStudents = (usersData) => async (cid, searchTerm) => {
  try {
    const data = await usersData.getNotEnrolledStudentsQuery(cid, searchTerm)
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getAllDeletedStudents = (usersData) => async () => {
  try {
    const data = await usersData.getAllDeletedStudents()
    return {
      error: null,
      data
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const enrollStudent = (usersData) => async (studentId, cid) => {
  try {
    const result = await usersData.enrollStudent(studentId, cid)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const unEnrollStudent = (usersData) => async (studentId, cid) => {
  try {
    const result = await usersData.unEnrollStudent(studentId, cid)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const grantAccessToSection = (usersData) => async (studentId, sectionId) => {
  try {
    const result = await usersData.grantAccessToSectionQuery(studentId, sectionId)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const revokeAccessToSection = (usersData) => async (studentId, sectionId) => {
  try {
    const result = await usersData.revokeAccessToSectionQuery(studentId, sectionId)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const validateUser = (usersData) => async ({ email, password }) => {
  const user = await getUserByEmail(usersData)(email)
  if (user.error) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      user: null
    }
  }
  if (await bcrypt.compare(password, user.data.password)) {
    return {
      error: null,
      user: user.data
    }
  }

  return {
    error: serviceErrors.OPERATION_NOT_PERMITTED,
    user: null
  }
}

export const createStudent = (usersData) => async (student) => {
  try {
    const result = await usersData.createStudent(student)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const deleteStudent = (usersData) => async (studentId) => {
  try {
    const result = await usersData.deleteStudent(studentId)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const unDeleteStudent = (usersData) => async (studentId) => {
  try {
    const result = await usersData.unDeleteStudent(studentId)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getUserByEmail = (usersData) => async (email) => {
  try {
    const user = await usersData.getUserByEmail(email)

    if (user === null) {
      return {
        error: serviceErrors.RECORD_NOT_FOUND,
        data: null
      }
    } else {
      return {
        error: null,
        data: user
      }
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const getAllEnrolledCourses = (usersData) => async (userId) => {
  try {
    const result = await usersData.getAllEnrolledCoursesQuery(userId)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const updateUserById = (usersData) => async (newUser, userId) => {
  try {
    const result = await usersData.updateUserQuery(newUser, userId)
    return {
      error: null,
      data: result
    }
  } catch (e) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      data: null
    }
  }
}

export const updatePassword = (usersData) => async (comparePassword, newPassword, userId) => {
  const user = await usersData.getUserById(userId)
  if (user.error) {
    return {
      error: serviceErrors.RECORD_NOT_FOUND,
      user: null
    }
  }

  if (!(await bcrypt.compare(comparePassword, user.password))) {
    return {
      error: serviceErrors.OPERATION_NOT_PERMITTED,
      user: null
    }
  }
  const result = await usersData.updatePassword(userId, newPassword)
  return {
    error: null,
    user: result
  }
}

export const isStudentGrantedAccess = (usersData) => async (studentId, sectionId) => {
  return await usersData.isStudentGrantedAccessQuery(studentId, sectionId)
}

export const userExistsByEmail = (usersData) => async (email) => {
  const result = await usersData.userExistsByEmail(email)
  return result
}

export const userExistsById = (usersData) => async (id) => {
  const result = await usersData.userExistsById(id)
  return result
}

export const isStudentEnrolled = (usersData) => async (studentId, cid) => {
  const result = await usersData.isStudentEnrolled(studentId, cid)
  return result
}

export const isStudentDeleted = (usersData) => async (studentId) => {
  const result = await usersData.isStudentDeleted(studentId)
  return result
}

export const isUserATeacher = (usersData) => async (studentId) => {
  const result = await usersData.isUserATeacher(studentId)
  return result
}
