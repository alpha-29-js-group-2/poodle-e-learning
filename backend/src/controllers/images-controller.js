
import express from 'express'
import { authMiddleware, roleMiddleware } from '../middleware/authentication.js'
import userRole from '../common/user-role.js'
import multer from 'multer'
import imagesService from '../services/images-service.js'
import imagesData from '../data/images-data.js'
import serviceErrors from '../common/service-errors.js'

const imagesRoute = express.Router()
const upload = multer({ dest: 'uploads/' })

imagesRoute
  .post('/course/:courseId', authMiddleware, roleMiddleware(userRole.TEACHER), upload.single('image'), async (req, res) => {
    const { filename } = req.file
    const { courseId } = req.params
    const { error, course } = await imagesService.updateCourseImage(imagesData)(courseId, filename)

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(200).json(course)
  })

  .get('/course/:courseId', async (req, res) => {
    const { courseId } = req.params
    const { error, image } = await imagesService.getCourseImage(imagesData)(courseId)

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }

    res.contentType('image/png')
    res.send(image)
  })

export default imagesRoute
