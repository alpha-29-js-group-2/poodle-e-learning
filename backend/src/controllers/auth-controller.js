import express from 'express'
import serviceErrors from '../common/service-errors.js'
import {
  createStudent,
  validateUser,
  userExistsByEmail
} from '../services/users-service.js'
import { blacklistToken } from '../services/auth-service.js'
import createToken from '../auth/create-token.js'
import authData from '../data/auth-data.js'
import * as usersData from '../data/users-data.js'
import validateBody from '../middleware/validate-body.js'
import loginValidation from '../validations/login-user-validation.js'
import createUserValidation from '../validations/create-user-validation.js'

const authRoute = express.Router()

// To Login
authRoute.post('/login', validateBody(loginValidation), async (req, res) => {
  const { email, password } = req.body
  const { error, user } = await validateUser(usersData)({ email, password })
  if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
    return res.status(400).send({
      error: 'Invalid password'
    })
  }

  if (error === serviceErrors.RECORD_NOT_FOUND) {
    return res.status(400).send({
      error: 'No such username'
    })
  }
  const payload = {
    id: user.id,
    email: user.email,
    role: user.role_id
  }
  const token = createToken(payload)

  res.status(200).send({ token: token })
})

// To Register
authRoute.post(
  '/register',
  validateBody(createUserValidation),
  async (req, res, next) => {
    const student = req.body
    if (await userExistsByEmail(usersData)(student.email)) {
      return res.status(400).json({
        error: `Student with this email ${student.email} already exists.`
      })
    }

    const { error, data } = await createStudent(usersData)(student)
    if (error) {
      return res.status(400).json({
        error: error.message
      })
    }

    res.json(data)
  }
)

// To Logout
authRoute.delete('/logout', async (req, res) => {
  const auth = req.headers.authorization
  if (!auth) {
    return res.status(400).json({
      message: 'No authorization present!'
    })
  }

  const token = auth.replace('Bearer ', '')

  if (token === 'null') {
    return res.status(400).json({
      message: 'No token present!'
    })
  }

  const { error } = await blacklistToken(authData)(token)

  if (error === serviceErrors.DUPLICATE_RECORD) {
    return res.status(400).json({
      error: 'Token already used to sign out.'
    })
  }

  if (error === serviceErrors.SERVER_ERROR) {
    return res.status(400).json({
      error: 'Something went wrong when logging out.'
    })
  }

  res.json({ message: 'You have been logged out!' })
})

export default authRoute
