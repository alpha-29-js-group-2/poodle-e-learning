import express from 'express'
import coursesService from '../services/courses-service.js'
import coursesData from '../data/courses-data.js'
import * as usersData from '../data/users-data.js'
import * as sectionsData from '../data/sections-data.js'
import serviceErrors from '../common/service-errors.js'
import validateBody from '../middleware/validate-body.js'
import createCourseValidation from '../validations/create-course-validation.js'
import updateCourseValidation from '../validations/update-course-validation.js'
import updateSectionsOrderValidation from '../validations/update-sections-order-validation.js'
import { IS_DELETED, IS_PUBLIC } from '../common/constants.js'
import { authMiddleware, roleMiddleware } from '../middleware/authentication.js'
import userRole from '../common/user-role.js'

const coursesRoute = express.Router()

coursesRoute
// ---Before authentication---

// Get all public courses - in client -> render only title & description.
  .get('/browse', async (req, res) => {
    const { search } = req.query
    const isPublic = IS_PUBLIC
    const { limit, offset } = req.query
    const pagination = { limit, offset }

    const { error, courses } = await coursesService.getAllCourses(coursesData)(search, isPublic, pagination)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No courses found.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(200).json(courses)
  })

// ---After authentication---

// Teachers Get all courses - by search / title / pagination / public status
// Students Get only all courses - that are public
  .get('/', authMiddleware, async (req, res) => {
    const { search, isPublic } = req.query
    const { limit, offset } = req.query
    const pagination = { limit, offset }
    const role = req.user.role

    const { error, courses } = await coursesService.getAllCourses(coursesData)(search, isPublic, pagination, role)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No courses found.' })
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(409).json({ error: 'Operation is not permitted. Students can only see public courses.' })
    }
    res.status(200).json(courses)
  })

// Get all my courses -> by user
  .get('/mycourses', authMiddleware, async (req, res) => {
    const userId = req.user.id
    const role = req.user.role
    const { limit, offset, search } = req.query
    const pagination = { limit, offset }

    const { error, courses } = await coursesService.getAllCoursesByUser(coursesData)(parseInt(userId, 10), pagination, role, search)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No courses found!' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(200).json(courses)
  })

// Create course

  .post('/', authMiddleware, roleMiddleware(userRole.TEACHER), validateBody(createCourseValidation), async (req, res) => {
    const userId = req.user.id
    const { title, description, isPublic } = req.body
    const { error, course } = await coursesService.createCourse(coursesData)(title, description, isPublic, userId)

    if (error === serviceErrors.DUPLICATE_RECORD) {
      return res.status(409).json({ error: 'Course title already exists.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(201).json(course)
  })

// Update sections order in course

  .put('/:cid/rearrange-sections',
    authMiddleware,
    roleMiddleware(userRole.TEACHER),
    validateBody(updateSectionsOrderValidation),
    async (req, res) => {
      const cid = req.params.cid
      const userId = req.user.id
      const order = req.body.order
      const { error, course } = await coursesService.updateSectionsOrder(coursesData, sectionsData)(cid, order, userId)
      switch (error) {
        case serviceErrors.RECORD_NOT_FOUND:
          return res.status(404).json({ error: 'No course found.' })
        case serviceErrors.OPERATION_NOT_PERMITTED:
          return res.status(409).json({ error: 'Operation is not permitted. Only the creator of this course can change it' })
        case serviceErrors.SERVER_ERROR:
          return res.status(500).json({ error: 'Unknown error.' })
        case null:
          return res.status(200).json(course)
        default:
          return res.status(400).json({ error: error.message })
      };
    })

// Update course - only the teacher who created the course can change it
  .put('/:id', authMiddleware, roleMiddleware(userRole.TEACHER), validateBody(updateCourseValidation), async (req, res) => {
    const { id } = req.params
    const { title, description, isPublic, isDeleted } = req.body
    const userId = req.user.id

    const { error, course } = await coursesService.updateCourse(coursesData)(title, description, isPublic, parseInt(id, 10), userId, isDeleted)
    switch (error) {
      case serviceErrors.RECORD_NOT_FOUND:
        return res.status(404).json({ error: 'No course found.' })
      case serviceErrors.DUPLICATE_RECORD:
        return res.status(409).json({ error: 'Course title already exists.' })
      case serviceErrors.OPERATION_NOT_PERMITTED:
        return res.status(409).json({ error: 'Operation is not permitted. Only the creator of this course can change it' })
      case serviceErrors.SERVER_ERROR:
        return res.status(500).json({ error: 'Unknown error.' })
      case null:
        return res.status(200).json(course)
      default:
        return res.status(400).json({ error: 'Bad request.' })
    };
  })

// Delete course -  - only the teacher who created the course can delete it
  .delete('/:id', authMiddleware, roleMiddleware(userRole.TEACHER), async (req, res) => {
    const { id } = req.params
    const userId = req.user.id

    const { error, course } = await coursesService.deleteCourse(coursesData)(parseInt(id, 10), userId)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No course found.' })
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(409).json({ error: 'Operation is not permitted. Only the creator of this course can delete it.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(200).json(course)
  })

// Like/unlike course - enrolled students
  .put('/:id/like', authMiddleware, roleMiddleware(userRole.STUDENT), async (req, res) => {
    const { id } = req.params
    const userId = req.user.id

    const role = req.user.role
    if (role !== userRole.STUDENT) {
      return res.status(409).json({ error: 'Operation is not permitted. Only students can like the courses they are enrolled in' })
    }

    const { error, isLiked } = await coursesService.updateCourseIsLiked(coursesData)(parseInt(id, 10), parseInt(userId, 10))

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'Course not found!' })
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(409).json({ error: 'Operation is not permitted. You can only like courses you are enrolled in.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    return res.status(200).json(isLiked)
  })

// Get all deleted courses
  .get('/deleted', authMiddleware, roleMiddleware(userRole.TEACHER), async (req, res) => {
    const { search } = req.query
    const { limit, offset } = req.query
    const pagination = { limit, offset }

    const { error, courses } = await coursesService.getAllDeletedCourses(coursesData)(search, pagination)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No deleted courses found.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(200).json(courses)
  })

// Get deleted course by id - any teacher can see it
  .get('/deleted/:id', authMiddleware, roleMiddleware(userRole.TEACHER), async (req, res) => {
    const { id } = req.params

    const { error, course } = await coursesService.getDeletedCourseById(coursesData)(parseInt(id, 10), IS_DELETED)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No such course found.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(200).json(course)
  })

// Undelete course by id - any teacher can undelete it
  .put('/deleted/:id', authMiddleware, roleMiddleware(userRole.TEACHER), async (req, res) => {
    const { id } = req.params
    const { title, description, isPublic, isDeleted } = req.body
    const { error, course } = await coursesService.updateDeletedCourseById(coursesData)(title, description, isPublic, parseInt(id, 10), isDeleted)
    switch (error) {
      case serviceErrors.RECORD_NOT_FOUND:
        return res.status(404).json({ error: 'No course found.' })
      case serviceErrors.DUPLICATE_RECORD:
        return res.status(409).json({ error: 'Course title already exists.' })
      case serviceErrors.SERVER_ERROR:
        return res.status(500).json({ error: 'Unknown error.' })
      case null:
        return res.status(200).json(course)
      default:
        return res.status(400).json({ error: 'Bad request.' })
    };
  })

// Get course by id - Teachers access both public and private courses
// Students - access public courses and only private courses they are enrolled in
  .get('/:id', authMiddleware, async (req, res) => {
    const { id } = req.params
    const userId = req.user.id
    const role = req.user.role

    const { error, course } = await coursesService.getCourseById(coursesData, usersData)(parseInt(id, 10), role, userId)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({ error: 'No course found.' })
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(409).json({ error: 'Operation is not permitted. Students cannot see private courses they are not enrolled in.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }
    res.status(200).json(course)
  })

export default coursesRoute
