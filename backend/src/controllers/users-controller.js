import express from 'express'
import serviceErrors from '../common/service-errors.js'
import userRole from '../common/user-role.js'
import {
  getAllEnrolledStudents,
  getNotEnrolledStudents,
  getAllTeachers,
  getAuthorByCourseId,
  getAllStudents,
  getAllDeletedStudents,
  getAllUsers,
  isStudentEnrolled,
  enrollStudent,
  unEnrollStudent,
  userExistsById,
  deleteStudent,
  isStudentDeleted,
  unDeleteStudent,
  isUserATeacher,
  isStudentGrantedAccess,
  grantAccessToSection,
  revokeAccessToSection,
  getAllEnrolledCourses,
  getUserByEmail,
  updateUserById,
  updatePassword
} from '../services/users-service.js'

import coursesService from '../services/courses-service.js'
import coursesData from '../data/courses-data.js'
import { roleMiddleware } from '../middleware/authentication.js'
import validateBody from '../middleware/validate-body.js'
import enrollStudentsValidation from '../validations/enroll-students-validation.js'
import grantAccessStudentValidation from '../validations/grant-access-student-validation.js'
import * as usersData from '../data/users-data.js'
import * as sectionsData from '../data/sections-data.js'
import { getSectionById } from '../services/sections-service.js'
import updatePasswordValidation from '../validations/update-password-validation.js'
import updateUserInfoValidation from '../validations/update-user-info-validation.js'

const usersRoute = express.Router()

/** Get all teachers */
usersRoute.get(
  '/teachers',
  roleMiddleware(userRole.STUDENT),
  async (req, res, next) => {
    const { error, data } = await getAllTeachers(usersData)()

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'Something went wrong when displaying all teachers'
      })
    }

    return res.json(data)
  }
)

// Get author of course
usersRoute.get('/teachers/author/:cid',
  async (req, res, next) => {
    const cid = req.params.cid
    const { error, data } = await getAuthorByCourseId(usersData)(cid)

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: `Something went wrong when displaying author of course ${cid}`
      })
    }

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({
        error: `No course found with id ${cid}`
      })
    }

    return res.json(data)
  })

// Get all deleted students
usersRoute.get(
  '/students/deleted',
  roleMiddleware(userRole.TEACHER),
  async (req, res, next) => {
    const { error, data } = await getAllDeletedStudents(usersData)()

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'Something went wrong when displaying all students'
      })
    }

    return res.json(data)
  }
)

/** Get all students */
usersRoute.get(
  '/students',
  roleMiddleware(userRole.STUDENT),
  async (req, res, next) => {
    const { error, data } = await getAllStudents(usersData)()

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'Something went wrong when displaying all students'
      })
    }

    return res.json(data)
  }
)

/**
 * cid is the course id
 * enrolled is whether we are looking for enrolled,
 * or not enrolled students to the course
 */
usersRoute.get(
  '/',
  roleMiddleware(userRole.STUDENT),
  async (req, res, next) => {
    const cid = req.query.cid
    if (cid) {
      let searchTerm
      if (req.query.enrolled !== 'false') {
        if (req.query.search) {
          searchTerm = req.query.search
        }
        const { error, data } = await getAllEnrolledStudents(usersData)(cid, searchTerm)
        if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
          return res.status(400).json({
            error: `Something went wrong when displaying \
all enrolled students to course with id ${cid}`
          })
        }

        return res.json(data)
      } else {
        if (req.query.search) {
          searchTerm = req.query.search
        }
        const { error, data } = await getNotEnrolledStudents(usersData)(cid, searchTerm)

        if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
          return res.status(400).json({
            error: `Something went wrong when displaying \
all not enrolled students to course with id ${cid}`
          })
        }
        return res.json(data)
      }
    }

    const { error, data } = await getAllUsers(usersData)()
    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'Something went wrong when displaying all users'
      })
    }

    return res.json(data)
  }
)

// To enroll one or multiple students to a course
usersRoute.post(
  '/course/:cid',
  roleMiddleware(userRole.STUDENT),
  validateBody(enrollStudentsValidation),
  async (req, res, next) => {
    const cid = req.params.cid
    const students = req.body.batch
    const messages = {}
    if (!students) {
      return res.status(400).json({
        error: 'Array of students to be enrolled is empty'
      })
    }

    if (students.length > 1 && req.user.role < userRole.TEACHER) {
      return res.status(400).json({
        error: 'To enroll more than one person, you need to be a teacher.'
      })
    }

    if (req.user.role < userRole.TEACHER && +req.user.id !== +students[0]) {
      return res.status(400).json({
        error: 'Students can only enroll themselves.'
      })
    }

    const { error: courseError, data: isPublic } = await coursesService.courseIsPublic(coursesData)(cid)

    if (courseError === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({
        error: `Course with id ${cid} does not exist.`
      })
    }

    if (courseError === serviceErrors.SERVER_ERROR) {
      return res.status(404).json({
        error: `Something went wrong when checking whether  \
the course with id ${cid} is public.`
      })
    }

    if (req.user.role === userRole.TEACHER && isPublic) {
      return res.status(400).json({
        error: 'Teachers can only enroll students to private courses.'
      })
    }

    if (req.user.role === userRole.STUDENT && !isPublic) {
      return res.status(400).json({
        error: 'Students can only enroll themselves to public courses.'
      })
    }

    for (let i = 0; i < students.length; i++) {
      const studentId = students[i]

      if (await isStudentEnrolled(usersData)(studentId, cid)) {
        messages[i] = {
          error: `User with id ${studentId} is already \
enrolled for course with id ${cid}`
        }
        continue
      }

      if (await isUserATeacher(usersData)(studentId)) {
        messages[i] = {
          error: `User with id ${studentId} is a teacher \
and unfortunately cannot be enrolled for the course.`
        }
        continue
      }

      const { data, error } = await enrollStudent(usersData)(
        studentId,
        req.params.cid
      )
      if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
        messages[i] = {
          error: `Something went wrong when \
enrolling student with id ${studentId} \
for course with id ${cid}.`
        }
        continue
      }

      messages[i] = {
        success: data
      }
    }
    res.json(messages)
  }
)

// To unenroll student user
usersRoute.delete(
  '/:studentId/course/:cid',
  roleMiddleware(userRole.STUDENT),
  async (req, res, next) => {
    const studentId = req.params.studentId
    const cid = req.params.cid

    if (+req.user?.id === +studentId || req.user?.role >= userRole.TEACHER) {
      const enrolled = await isStudentEnrolled(usersData)(studentId, cid)

      if (!enrolled) {
        return res.status(400).json({
          error: `Student with Id ${studentId} is \
    not currently enrolled in course with id ${cid}`
        })
      }

      const { error, data } = await unEnrollStudent(usersData)(studentId, cid)

      if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
        return res.status(400).json({
          error: `Student with id ${studentId} was not \
    unenrolled for course with id ${cid}.`
        })
      }

      res.json(data)
    } else {
      return res.status(400).json({
        error: 'Only a teacher or the student themselves can unenroll that student from a course'
      })
    }
  }
)

// To grant access to student(s) to a section.
usersRoute.post(
  '/section/:sectionId',
  roleMiddleware(userRole.TEACHER),
  validateBody(grantAccessStudentValidation),
  async (req, res, next) => {
    const sectionId = req.params.sectionId
    const students = req.body.batch
    const messages = {}
    if (!students?.length) {
      return res.status(400).json({
        error: `Array of students to be granted access to section with \
id ${sectionId} is empty`
      })
    }

    const {
      error: sectionError,
      data: section
    } = await getSectionById(sectionsData)(sectionId)

    if (sectionError === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({
        error: `Section with id ${sectionId} does not exist.`
      })
    }

    if (sectionError === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: `Section with id ${sectionId} could not be fetched.`
      })
    }

    const cid = section.course_id

    for (let i = 0; i < students.length; i++) {
      const studentId = students[i]

      if (await isStudentDeleted(usersData)(studentId)) {
        messages[i] = {
          error: `User with id ${studentId} is deleted.`
        }
        continue
      }

      if (await isUserATeacher(usersData)(studentId)) {
        messages[i] = {
          error: `User with id ${studentId} is a teacher \
and does not need to be granted access to the section.`
        }
        continue
      }

      if (!await isStudentEnrolled(usersData)(studentId, cid)) {
        messages[i] = {
          error: `User with id ${studentId} needs to be already \
enrolled for course with id ${cid} before getting access to section \
with id ${sectionId}`
        }
        continue
      }

      if (await isStudentGrantedAccess(usersData)(studentId, sectionId)) {
        messages[i] = {
          error: `User with id ${studentId} is already \
granted access to section with id ${sectionId}`
        }
        continue
      }

      const { data, error } = await grantAccessToSection(usersData)(
        studentId,
        sectionId
      )
      if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
        messages[i] = {
          error: `Something went wrong when \
granting access to student with id ${studentId} \
for section with id ${sectionId}.`
        }
        continue
      }

      messages[i] = {
        success: data
      }
    }
    res.json(messages)
  }
)

// To revoke previously granted access to student(s) to a section.
usersRoute.delete(
  '/section/:sectionId',
  roleMiddleware(userRole.TEACHER),
  validateBody(grantAccessStudentValidation),
  async (req, res, next) => {
    const students = req.body.batch
    const sectionId = req.params.sectionId
    const messages = {}

    const {
      error: sectionError,
      data: section
    } = await getSectionById(sectionsData)(sectionId)

    if (sectionError === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({
        error: `Section with id ${sectionId} does not exist.`
      })
    }

    if (sectionError === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: `Section with id ${sectionId} could not be fetched.`
      })
    }

    const cid = section.course_id

    for (let i = 0; i < students.length; i++) {
      const studentId = students[i]

      if (await isStudentDeleted(usersData)(studentId)) {
        messages[i] = {
          error: `User with id ${studentId} is deleted.`
        }
        continue
      }

      if (await isUserATeacher(usersData)(studentId)) {
        messages[i] = {
          error: `User with id ${studentId} is a teacher \
and cannot be revoked the access to the section.`
        }
        continue
      }

      if (!await isStudentEnrolled(usersData)(studentId, cid)) {
        messages[i] = {
          error: `User with id ${studentId} needs to be already \
enrolled for course with id ${cid}, before getting his or her \
access to section with id ${sectionId} revoked`
        }
        continue
      }

      if (!await isStudentGrantedAccess(usersData)(studentId, sectionId)) {
        messages[i] = {
          error: `User with id ${studentId} hasn't been \
granted access to section with id ${sectionId}`
        }
        continue
      }

      const { data, error } = await revokeAccessToSection(usersData)(
        studentId,
        sectionId
      )
      if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
        messages[i] = {
          error: `Something went wrong when \
revoking the access of student with id ${studentId} \
for section with id ${sectionId}.`
        }
        continue
      }

      messages[i] = {
        success: data
      }
    }
    res.json(messages)
  }
)

// To delete student user
usersRoute.delete(
  '/:studentId',
  roleMiddleware(userRole.TEACHER),
  async (req, res, next) => {
    const studentId = req.params.studentId

    const student = await userExistsById(usersData)(studentId)
    if (!student) {
      return res.status(404).json({
        error: `Student with Id ${studentId} doesn't exist`
      })
    }

    const { error, data } = await deleteStudent(usersData)(studentId)
    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: `Student with id ${studentId} was not
            successfully deleted.`
      })
    }

    res.json(data)
  }
)

// To undelete student user
usersRoute.put(
  '/:studentId/undelete',
  roleMiddleware(userRole.TEACHER),
  async (req, res, next) => {
    const studentId = req.params.studentId
    const student = await isStudentDeleted(usersData)(studentId)
    if (!student) {
      return res.status(404).json({
        error: `Student with id ${studentId} cannot be undeleted, \
because he is not deleted.`
      })
    }

    const { error, data } = await unDeleteStudent(usersData)(studentId)
    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: `Student with id ${studentId} was not
            successfully unDeleted.`
      })
    }

    res.json(data)
  }
)

/** Get all courses a student is enrolled in */
usersRoute.get(
  '/courses-enrolled',
  roleMiddleware(userRole.STUDENT),
  async (req, res, next) => {
    const userId = req.user.id
    const role = req.user.role

    if (role === userRole.TEACHER) {
      return res.json([])
    }

    const { error, data } = await getAllEnrolledCourses(usersData)(userId)

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'Something went wrong when getting all student\'s enrolled courses'
      })
    }

    return res.json(data)
  }
)

/** Get user by email */
usersRoute.get(
  '/my-info',
  async (req, res, next) => {
    const userEmail = req.user.email

    const { error, data } = await getUserByEmail(usersData)(userEmail)

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'Something went wrong when displaying user information'
      })
    }
    return res.json(data)
  }
)

/** Update user's names */
usersRoute.put(
  '/my-info', validateBody(updateUserInfoValidation),
  async (req, res, next) => {
    const userId = req.user.id
    const newUser = req.body

    const { error, data } = await updateUserById(usersData)(newUser, userId)

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'Something went wrong when displaying user information'
      })
    }
    return res.json(data)
  }
)
/** Update user's password */

usersRoute.put(
  '/my-pass', validateBody(updatePasswordValidation),
  async (req, res, next) => {
    const userId = req.user.id
    const { comparePassword, newPassword } = req.body

    const { error, user } = await updatePassword(usersData)(comparePassword, newPassword, userId)

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'There was a mismatch in your original password'
      })
    }
    return res.json(user)
  }
)
export default usersRoute
