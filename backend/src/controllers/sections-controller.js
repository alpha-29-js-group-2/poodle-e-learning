import express from 'express'
import userRole from '../common/user-role.js'
import {
  //   getAllSections,
  getAllSectionsFromACourse,
  getSectionById,
  createSection,
  updateSection,
  deleteSection,
  isSectionRestrictedForStudents,
  isStudentGrantedAccessToSection,
  getUsersWithAccessToSection,
  getUsersWithoutAccessToSection
} from '../services/sections-service.js'
import { isStudentEnrolled } from '../services/users-service.js'
import * as usersData from '../data/users-data.js'
import {
  authMiddleware,
  roleMiddleware
} from '../middleware/authentication.js'
import validateBody from '../middleware/validate-body.js'
import * as sectionsData from '../data/sections-data.js'
import { encode, decode } from 'html-entities'
import createSectionValidation from '../validations/create-section-validation.js'
import coursesData from '../data/courses-data.js'
import serviceErrors from '../common/service-errors.js'
import updateSectionValidation from '../validations/update-section-validation.js'
import sanitizeHtml from 'sanitize-html'
import { HTML_SANITIZER_ALLOWED_ATTRIBUTES, HTML_SANITIZER_ALLOWED_TAGS } from '../common/constants.js'

const sectionsRoute = express.Router()

// Get sections from a course
// Endpoint: /sections/?cid=
sectionsRoute.get(
  '/course/:cid',
  authMiddleware,
  roleMiddleware(userRole.STUDENT),
  async (req, res, next) => {
    const cid = req.params.cid

    const { error, data } = await getAllSectionsFromACourse(
      sectionsData,
      coursesData
    )(cid)

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res
        .status(404)
        .json({ error: `Course with id ${cid} is not found.` })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(400).json({
        error: `Something went wrong when retrieving sections from course ${cid}.`
      })
    }

    // To return valid HTML
    const decodedData = data.map((entry) => {
      entry.content = decode(entry.content)
      return entry
    })

    const PromiseSectionsWithRestrictions = decodedData.map(async (section) => {
      const sectionIsUserRestricted = await isSectionRestrictedForStudents(
        sectionsData
      )(section.id)

      if (sectionIsUserRestricted) {
        section.hasRestrictionToStudents = true
      }
      return section
    })
    const sectionsWithRestrictions = await Promise.all(PromiseSectionsWithRestrictions)

    // Students cannot access restricted sections or sections in courses they are not enrolled in
    if (req.user.role < userRole.TEACHER) {
      const studentId = req.user.id

      const studentIsEnrolled = await isStudentEnrolled(usersData)(
        studentId,
        cid
      )
      if (!studentIsEnrolled) {
        return res.status(400).json({
          error: `To view those sections, you need to be enrolled to course with id ${cid}`
        })
      }
      const studentData = sectionsWithRestrictions.map(async (section) => {
        if (section.restriction_date && section.restriction_date > new Date()) {
          section.content = 'This section is restricted.'
        }

        const sectionIsUserRestricted = await isSectionRestrictedForStudents(
          sectionsData
        )(section.id)

        if (sectionIsUserRestricted) {
          section.hasRestrictionToStudents = true
          const isUserGrantedAccess = await isStudentGrantedAccessToSection(
            sectionsData
          )(section.id, studentId)

          if (!isUserGrantedAccess) {
            section.content = 'This section is restricted.'
            section.restrictedToUser = true
          }
        }

        return section
      })

      return res.json(await Promise.all(studentData))
    }
    return res.json(sectionsWithRestrictions)
  }
)

/* Get ids of students who can access a section
Endpoint:
  /sections/:id/granted-access */
sectionsRoute.get('/:id/users/',
  authMiddleware,
  roleMiddleware(userRole.TEACHER),
  async (req, res, next) => {
    const sectionId = req.params.id
    const granted = req.query.granted
    if (granted !== 'false') {
      const { error, data } = await getUsersWithAccessToSection(sectionsData)(sectionId)
      if (error === serviceErrors.RECORD_NOT_FOUND) {
        return res.status(400).json({
          error: `Section with id ${sectionId} doesn't exist.`
        })
      }
      if (error === serviceErrors.SERVER_ERROR) {
        return res.status(400).json({
          error: `Something went wrong when retrieving students that can access \
section with id ${sectionId}.`
        })
      }

      return res.json(data)
    }
    const { error, data } = await getUsersWithoutAccessToSection(sectionsData)(sectionId)
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(400).json({
        error: `Section with id ${sectionId} doesn't exist.`
      })
    }
    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(400).json({
        error: `Something went wrong when retrieving students that can access \
section with id ${sectionId}.`
      })
    }

    return res.json(data)
  })

/* Get section by id
Endpoint:
    /sections/:id */
sectionsRoute.get(
  '/:id',
  authMiddleware,
  roleMiddleware(userRole.STUDENT),
  async (req, res, next) => {
    const sectionId = req.params.id
    const { error, data: section } = await getSectionById(sectionsData)(sectionId)
    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(400).json({
        error: `Section with id ${sectionId} doesn't exist.`
      })
    }
    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(400).json({
        error: `Something went wrong when retrieving section with id ${sectionId}.`
      })
    }

    // To return valid HTML
    section.content = decode(section.content)

    const sectionIsUserRestricted = await isSectionRestrictedForStudents(
      sectionsData
    )(sectionId)

    if (sectionIsUserRestricted) {
      section.hasRestrictionToStudents = true
    }

    // Students can only access unrestricted sections that are in courses
    // they are enrolled in
    if (req.user.role < userRole.TEACHER) {
      if (section.restriction_date && section.restriction_date > new Date()) {
        section.content = 'This section is restricted.'
      }

      const studentId = req.user.id

      const studentIsEnrolled = await isStudentEnrolled(usersData)(
        studentId,
        section.course_id
      )
      if (!studentIsEnrolled) {
        return res.status(400).json({
          error: `To view this section, you need to be enrolled to course with id ${section.course_id}`
        })
      }

      if (sectionIsUserRestricted) {
        const accessGranted = await isStudentGrantedAccessToSection(
          sectionsData
        )(sectionId, studentId)

        if (!accessGranted) {
          section.content = 'This section is restricted.'
        }
      }
    }

    return res.json(section)
  }
)

// Create section - only the teacher who created the course can create sections for it; section must have a title
sectionsRoute.post(
  '/',
  authMiddleware,
  roleMiddleware(userRole.TEACHER),
  validateBody(createSectionValidation),
  async (req, res) => {
    const userId = req.user.id
    const { courseId, title, content, restrictionDate, isEmbedded } = req.body
    const sanitizedContent = sanitizeHtml(content, {
      allowedTags: sanitizeHtml.defaults.allowedTags.concat(HTML_SANITIZER_ALLOWED_TAGS),
      allowedAttributes: HTML_SANITIZER_ALLOWED_ATTRIBUTES
    })

    const encodedContent = encode(sanitizedContent)
    const reqParams = { courseId, title, encodedContent, restrictionDate, isEmbedded }
    const { error, data } = await createSection(sectionsData, coursesData)(
      reqParams,
      userId
    )

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res
        .status(400)
        .json({ error: `Course with id ${courseId} is not found.` })
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(409).json({
        error: 'Not allowed. Only the author of the course can create sections for it.'
      })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }

    data.content = decode(data.content)
    res.status(201).json(data)
  }
)

// Update section
sectionsRoute.put(
  '/:id',
  authMiddleware,
  roleMiddleware(userRole.TEACHER),
  validateBody(updateSectionValidation),
  async (req, res) => {
    const userId = req.user.id
    const sectionId = req.params.id
    const { courseId, title, content, restrictionDate, orderNum, isEmbedded } = req.body
    const sanitizedContent = sanitizeHtml(content, {
      allowedTags: sanitizeHtml.defaults.allowedTags.concat(HTML_SANITIZER_ALLOWED_TAGS),
      allowedAttributes: HTML_SANITIZER_ALLOWED_ATTRIBUTES
    })

    const encodedContent = encode(sanitizedContent)

    const reqParams = {
      courseId,
      sectionId,
      title,
      encodedContent,
      restrictionDate,
      orderNum,
      isEmbedded
    }
    const { error, course, section } = await updateSection(
      sectionsData,
      coursesData
    )(reqParams, userId)

    if (error === serviceErrors.RECORD_NOT_FOUND && course === null) {
      return res
        .status(400)
        .json({ error: `Course with id ${courseId} is not found.` })
    }

    if (error === serviceErrors.RECORD_NOT_FOUND && section === null) {
      return res
        .status(400)
        .json({ error: `Section with id ${sectionId} is not found.` })
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(409).json({
        error: 'Not allowed. Only the author of the course can update its sections.'
      })
    }

    if (error === serviceErrors.DUPLICATE_RECORD) {
      return res.status(409).json({ error: 'Duplicate section order number.' })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(500).json({ error: 'Unknown error.' })
    }

    section.content = decode(section.content)
    res.status(200).json(section)
  }
)

// Delete section
sectionsRoute.delete(
  '/:id',
  authMiddleware,
  roleMiddleware(userRole.TEACHER),
  async (req, res, next) => {
    const sectionId = req.params.id
    const userId = req.user.id
    const { error, data } = await deleteSection(sectionsData, coursesData)(
      sectionId,
      userId
    )

    if (error === serviceErrors.RECORD_NOT_FOUND) {
      return res.status(404).json({
        error: `Section with id ${sectionId} not found.`
      })
    }

    if (error === serviceErrors.OPERATION_NOT_PERMITTED) {
      return res.status(400).json({
        error: 'To delete a section, you need to be its course\'s author.'
      })
    }

    if (error === serviceErrors.SERVER_ERROR) {
      return res.status(400).json({
        error: `Deleting section with id ${sectionId} was unsuccessful.`
      })
    }

    data.content = decode(data.content)

    res.json(data)
  }
)

export default sectionsRoute
