import express from 'express'
import cors from 'cors'
import helmet from 'helmet'
import passport from 'passport'
import jwtStrategy from './auth/strategy.js'
import authRoute from './controllers/auth-controller.js'
import usersRoute from './controllers/users-controller.js'
import sectionsRoute from './controllers/sections-controller.js'
import { PORT } from './config/config.js'
import coursesRoute from './controllers/courses-controller.js'
import { authMiddleware } from './middleware/authentication.js'
import imagesRoute from './controllers/images-controller.js'

const app = express()

app.use(helmet())
app.use(cors(), express.json())
app.use('/auth', authRoute)
app.use('/users', authMiddleware, usersRoute)
app.use('/courses', coursesRoute)
app.use('/images', imagesRoute)
app.use('/sections', sectionsRoute)

passport.use(jwtStrategy)
app.use(passport.initialize())

app.all('*', (req, res) =>
  res.status(404).send({ message: 'Resource was not found!' })
)

app.listen(PORT, () => console.log(`Listening on PORT ${PORT}`))
