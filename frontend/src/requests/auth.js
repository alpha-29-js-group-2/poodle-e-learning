import { BASE_URL } from "../common/constants";

export const loginRequest = async (email, password) => {
  const result = await fetch(`${BASE_URL}/auth/login`, {
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, password }),
    method: "POST",
  });

  return await result.json();
};

export const logoutRequest = async () => {
  await (
    await fetch(`${BASE_URL}/auth/logout`, {
      method: "DELETE",
      headers: {
        authorization: "Bearer " + localStorage.getItem("token"),
      },
    })
  ).json();
};

export const registerRequest = async (email, password, firstname, lastname) => {
  const result = await fetch(`${BASE_URL}/auth/register`, {
    headers: {
      "Content-Type": "application/json",
    },
    body: JSON.stringify({ email, password, firstname, lastname }),
    method: "POST",
  });

  return await result.json();
}