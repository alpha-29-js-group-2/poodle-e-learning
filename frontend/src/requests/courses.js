import { BASE_URL } from "../common/constants";
import { getToken } from "../providers/AuthContext";


export const getAllPublicCourses = (offset, limit) =>
    fetch(`${BASE_URL}/courses/browse?offset=${offset}&limit=${limit}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());

export const getAllMyCourses = (offset, limit, search) =>
    fetch(`${BASE_URL}/courses/mycourses?search=${encodeURIComponent(search)}&offset=${offset}&limit=${limit}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());

export const getCourseImage = (courseId) =>
    fetch(`${BASE_URL}/images/course/${courseId}`, {
        method: 'GET',
        headers: {
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.blob());

export const getCourseById = (courseId) =>
    fetch(`${BASE_URL}/courses/${courseId}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());

export const getAllCourses = (offset, limit, search) => {
    return fetch(`${BASE_URL}/courses?search=${encodeURIComponent(search)}&offset=${offset}&limit=${limit}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());
};


export const createCourse = (course) => {
    course.isPublic ? course.isPublic = 1 : course.isPublic = 0;
    return fetch(`${BASE_URL}/courses`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify(course),
    }).then(r => r.json())
};

export const addImage = (courseId, image) => {
    const formData = new FormData();
    formData.append("image", image.imageAsFile);

    return fetch(`${BASE_URL}/images/course/${courseId}`, {
        method: 'POST',
        headers: {
            Authorization: `Bearer ${getToken()}`,
        },
        body: formData,
    }).then(r => {
        return r.json()
    })
};

export const deleteCourse = (courseId) =>
    fetch(`${BASE_URL}/courses/${courseId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(r => r.json());

export const updateCourse = (course) => {
    const isPublic = (+course.isPublic).toString();
    if (course.isDeleted) {
        return fetch(`${BASE_URL}/courses/deleted/${course.id}`, {
            method: 'PUT',
            headers: {
                'Content-Type': 'application/json',
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                title: course.title,
                description: course.description,
                isPublic: isPublic,
                isDeleted: course.isDeleted
            }),
        })
            .then(r => r.json());
    }

    return fetch(`${BASE_URL}/courses/${course.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify({
            title: course.title,
            description: course.description,
            isPublic: isPublic
        }),
    })
        .then(r => r.json());
};

export const getAllDeletedCourses = (offset, limit, search) =>
    fetch(`${BASE_URL}/courses/deleted?search=${encodeURIComponent(search)}&offset=${offset}&limit=${limit}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());
