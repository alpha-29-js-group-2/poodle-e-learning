import { BASE_URL } from "../common/constants";
import { getToken } from "../providers/AuthContext";


export const getAllSectionsInCourses = (cid) =>
    fetch(`${BASE_URL}/sections/course/${cid}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());

export const getSection = (id) => {
    return fetch(`${BASE_URL}/sections/${id}`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());
};

export const getStudentsWithAccess = (sectionId) => {
    return fetch(`${BASE_URL}/sections/${sectionId}/users/?granted=true`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());
}

export const getStudentsWithoutAccess = (sectionId) => {
    return fetch(`${BASE_URL}/sections/${sectionId}/users/?granted=false`, {
        method: 'GET',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    }).then(r => r.json());
}

export const createSectionInCourse = (courseId, section) => {
    const embedded = section.isEmbedded ? section.isEmbedded = 1 : section.isEmbedded = 0;
    return fetch(`${BASE_URL}/sections`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify({
            courseId: courseId,
            title: section.title,
            content: section.content,
            restrictionDate: section.restrictionDate,
            isEmbedded: embedded,
        })
    })
        .then(r => r.json())
}

export const updateSectionInCourse = (courseId, section) => {
    const embedded = section.isEmbedded ? section.isEmbedded = 1 : section.isEmbedded = 0;
    let restrictionDate = section.restriction_date ? section.restriction_date : null;

    if (restrictionDate?.includes('T')) {
        restrictionDate = restrictionDate.split('T')[0] + " " + restrictionDate.split('T')[1].substr(0, 8);
    }

    return fetch(`${BASE_URL}/sections/${section.id}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify({
            courseId: courseId,
            title: section.title,
            content: section.content,
            restrictionDate: restrictionDate,
            isEmbedded: embedded,
        })
    })
        .then(r => r.json())
}

export const rearrangeSections = (courseId, order) => {
    return fetch(`${BASE_URL}/courses/${courseId}/rearrange-sections`,{
        method: 'PUT',
        body: JSON.stringify({
            order,
        }),
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(r => r.json());
}

export const deleteSection = (sectionId) => {

    return fetch(`${BASE_URL}/sections/${sectionId}`, {
        method: 'DELETE',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
    })
        .then(r => r.json());
}