import { BASE_URL } from "../common/constants";
import { getToken } from "../providers/AuthContext";

export const getEnrolledStudents = async (courseId, searchTerm) => {
    const search = typeof searchTerm === 'string' && searchTerm !== '';
    const response = await fetch(`${BASE_URL}/users/?cid=${courseId}${search ?
        `&search=${encodeURIComponent(searchTerm)}` : ''}`, {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${getToken()}`,
        },
        method: "GET",
    });
    return await response.json();
}

export const getNotEnrolledStudents = async (courseId, searchTerm) => {
    const search = typeof searchTerm === 'string' && searchTerm !== '';
    const response = await
        fetch(`${BASE_URL}/users/?cid=${courseId}&enrolled=false${search ?
            `&search=${encodeURIComponent(searchTerm)}` : ''}`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${getToken()}`,
            },
            method: "GET",
        });
    return await response.json();
}

export const enrollStudents = async (studentsIds, courseId) => {

    const response =
        await fetch(`${BASE_URL}/users/course/${courseId}`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                batch: (studentsIds),
            }),
            method: "POST",
        });

    return await response.json();
}

export const unenrollStudent = async (studentId, courseId) => {
    const response =
        await fetch(`${BASE_URL}/users/${studentId}/course/${courseId}`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${getToken()}`,
            },
            method: "DELETE",
        });

    return await response.json();
}

export const grantStudents = async (studentsIds, sectionId) => {

    const response =
        await fetch(`${BASE_URL}/users/section/${sectionId}`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                batch: (studentsIds),
            }),
            method: "POST",
        });

    return await response.json();
}

export const revokeStudents = async (studentsIds, sectionId) => {

    const response =
        await fetch(`${BASE_URL}/users/section/${sectionId}`, {
            headers: {
                "Content-Type": "application/json",
                Authorization: `Bearer ${getToken()}`,
            },
            body: JSON.stringify({
                batch: (studentsIds),
            }),
            method: "DELETE",
        });

    return await response.json();
}

export const getAuthor = async (courseId) => {
    const response = await fetch(`${BASE_URL}/users/teachers/author/${courseId}`, {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${getToken()}`,
        },
    });

    return await response.json();
}

export const getEnrolledCourses = async () => {
    const response = await fetch(`${BASE_URL}/users/courses-enrolled`, {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${getToken()}`,
        },
    });

    return await response.json();
}

export const getUserInfo = async () => {
    const response = await fetch(`${BASE_URL}/users/my-info`, {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${getToken()}`,
        },
    });

    return await response.json();
}

export const updateUserInfo = async (user) => {
    const response = await fetch(`${BASE_URL}/users/my-info`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify(user),
    })
    return await response.json();
}

export const updateUserPass = async (comparePassword, newPassword) => {
    const response = await fetch(`${BASE_URL}/users/my-pass`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json',
            Authorization: `Bearer ${getToken()}`,
        },
        body: JSON.stringify({comparePassword, newPassword}),
    })
    return await response.json();
}

export const getCourseAuthor = async (courseId) => {
    const response = await fetch(`${BASE_URL}/users/teachers/author/${courseId}`, {
        headers: {
            "Content-Type": "application/json",
            Authorization: `Bearer ${getToken()}`,
        },
    });

    return await response.json();
}
