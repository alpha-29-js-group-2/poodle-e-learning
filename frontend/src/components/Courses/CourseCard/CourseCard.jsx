import React, { useState, useEffect } from 'react';
import '@fontsource/roboto';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import { Typography, Button, Box } from '@material-ui/core';
import { getCourseImage } from '../../../requests/courses';
import { Link } from 'react-router-dom';
import AuthContext from '../../../providers/AuthContext';
import { useContext } from 'react';
import { userRole } from '../../../common/user-role';
import { getEnrolledCourses } from '../../../requests/users';


const useStyles = makeStyles({
    root: {
        width: `100%`,
    },
    media: {
        height: 140,
    },

});


const CourseCard = ({ course, handleUndeleteCourse }) => {
    const { user } = useContext(AuthContext);
    const classes = useStyles();
    const [imageData, setImageData] = useState('');
    const [isStudentEnrolledInCourse, setIsStudentEnrolledInCourse] = useState(false);

    useEffect(() => {
        if (user && user.role === userRole.STUDENT) {
            getEnrolledCourses().then(r => {
                setIsStudentEnrolledInCourse(r.find(e => e.enrolledCourses === course.id));
            });
        }

        getCourseImage(course.id)
            .then(imageBlob => {

                const localUrl = URL.createObjectURL(imageBlob);
                setImageData(localUrl);
            });

    }, [course.id, user]);


    if (!course.isDeleted) {
        return (
                <div className={classes.root}>
                    <Card >
                        <Link to={`/courses/${course.id}`} style={{ textDecoration: 'none' }}>
                            <CardActionArea className={classes.container}>
                                <CardMedia className={classes.media}
                                    component="img"
                                    alt="Course Image"
                                    height="140"
                                    src={imageData}
                                    title="Course Image"
                                />
                                <CardContent>
                                    <Typography variant="h5" color="textPrimary" component="h2">
                                        {course.title}
                                    </Typography>
                                    <Typography variant="body2" color="textSecondary" component="p">
                                        {course.description}
                                    </Typography>
                                </CardContent>
                            </CardActionArea>
                            <CardActions style={{ justifyContent: 'center' }}>
                                {user && user.id === course.creator_id ? <Button disabled={true}>My course</Button> : <Box pb={4.5}></Box>}
                                {!user || user.role === userRole.TEACHER ? <></> : isStudentEnrolledInCourse ? <Button disabled={true}>Enrolled</Button> : <Button disabled={false} style={{ backgroundColor: 'transparent' }}>Enroll</Button>}
                            </CardActions>
                        </Link>
                    </Card >
                </div>
        )
    }
    return (
        <>
            <div style={{ display: 'flex', justifyContent: 'center' }}>
                <Card >
                    <CardActionArea className={classes.root} style={{ cursor: 'default' }}>
                        <CardMedia className={classes.media}
                            component="img"
                            alt="Course Image"
                            height="140"
                            src={imageData}
                            title="Course Image"
                        />
                        <CardContent>
                            <Typography variant="h5" color="textPrimary" component="h2">
                                {course.title}
                            </Typography>
                            <Typography variant="body2" color="textSecondary" component="p">
                                {course.description}
                            </Typography>
                        </CardContent>
                    </CardActionArea>
                    {user && user.id === course.creator_id ? <Button disabled={true}>My course</Button> : <Box pb={4.5}></Box>}
                    <br />
                    {course.isDeleted
                        ? <Button size="small" color="primary" onClick={() => { handleUndeleteCourse(course) }}>Restore</Button>
                        : <></>
                    }

                    <CardActions>
                    </CardActions>
                </Card >
            </div>
            <br />
        </>
    )
};

export default CourseCard;
