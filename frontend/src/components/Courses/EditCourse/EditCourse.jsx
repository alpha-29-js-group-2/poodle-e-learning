import React, { useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Button, TextField, Dialog, DialogActions, DialogContent, DialogContentText, DialogTitle, FormControlLabel, Checkbox, Box } from '@material-ui/core';
import { addImage, updateCourse } from '../../../requests/courses';
import AlertDialog from '../../Dialog/Dialog';
import { MAX_DESCRIPTION_LENGTH, MAX_TITLE_LENGTH, MIN_DESCRIPTION_LENGTH, MIN_TITLE_LENGTH } from '../../../common/constants';

const EditCourse = ({ visible, handleEdit, course, refreshCourse }) => {
    const [updatedCourse, setUpdatedCourse] = useState(course);
    const [titleError, setTitleError] = useState("");
    const [descriptionError, setDescriptionError] = useState("");
    const [dialogError, setDialogError] = useState({
        visible: false
    });
    const [titleChecked, setTitleChecked] = useState(false);
    const [descriptionChecked, setDescriptionChecked] = useState(false);
    const [imageChanged, setImageChanged] = useState(false);
    const [isPublicChanged, setIsPublicChanged] = useState(false);
    const [imageData, setImageData] = useState({});
    const history = useHistory();

    const handleChangeCourse = (key, value) => {
        if (key === 'isPublic') {
            setIsPublicChanged(true);
        }
        course[key] = value;
        setUpdatedCourse({ ...updatedCourse });
    };

    const closeDialog = () => {
        if (course.isDeleted) {
            history.push(`/courses/${course.id}`);
        } else {
            refreshCourse();
        }
        setTitleError("");
        setDescriptionError("");
        handleEdit();
        setDescriptionChecked(false);
        setTitleChecked(false);
        setImageChanged(false);
        setIsPublicChanged(false);
    };

    const closeDialogOnCancel = () => {
        setTitleError("");
        setDescriptionError("");
        if (course.isDeleted) {
            handleEdit(course);
        } else {
            handleEdit();
        }
        setDescriptionChecked(false);
        setTitleChecked(false);
        setImageChanged(false);
        setIsPublicChanged(false);
    }

    const closeDialogError = () => {
        setDialogError({
            visible: false
        })
    };

    const validateTitle = (title) => {
        if (title !== '' && title.length >= MIN_TITLE_LENGTH &&
            title.length <= MAX_TITLE_LENGTH) {
            setTitleChecked(true);
            return setTitleError("");
        }
        setTitleChecked(false);
        return setTitleError('Title must be between 5 and 100 symbols')
    };

    const validateDescription = (description) => {
        if (description !== '' && description.length >= MIN_DESCRIPTION_LENGTH &&
            description.length <= MAX_DESCRIPTION_LENGTH) {
            setDescriptionChecked(true);
            return setDescriptionError("");
        }
        setDescriptionChecked(false);
        return setDescriptionError('Description must be between 10 and 800 symbols')

    };


    const validateEdit = (!titleError && !descriptionError && [isPublicChanged, imageChanged].some(e => e)) || (!titleError && !descriptionError && [titleChecked, descriptionChecked].some(e => e));

    const handleSave = () => {

        updateCourse(course)
            .then(result => {
                if (result.error) {
                    setTitleError(result.error);
                    throw new Error(result.error);
                }
                return result;
            })
            .then(r => {
                if (!imageData.imageAsFile) {
                    return Promise.resolve(r);
                }
                return addImage(r.id, imageData);
            })
            .then(r => {
                closeDialog();
            })
            .catch(e => e)
    };

    const uploadImage = (e) => {
        const regex = new RegExp("(.*?)\.(png|jpg)$");
        if (!regex.test(e.currentTarget.value)) {
            setDialogError({
                content: "Incorrect file. Select jpg or png instead.",
                visible: true
            })
            return e.currentTarget.value = "";
        }

        const imgData = {
            imagePreview: URL.createObjectURL(e.target.files[0]),
            imageAsFile: e.target.files[0],
        }
        setImageData(imgData);
    };

    const buttonLabel = course.isDeleted ? 'Restore' : 'Save';

    return (
        <div>
            <AlertDialog content={dialogError.content} visible={dialogError.visible} onClose={closeDialogError}></AlertDialog>
            <Dialog open={visible} onClose={handleEdit} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">{course.isDeleted ? 'Restore course' :'Edit course'}</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                    {course.isDeleted ? 'Restore your course. ' : null}
                    Change the title or the description of your course. Manage its status - make it public or private - and update its image.
                    </DialogContentText>
                    <TextField
                        autoFocus
                        margin="dense"
                        id="title"
                        label="Title"
                        defaultValue={course.title}
                        fullWidth
                        variant="outlined"
                        onChange={(e) => validateTitle(e.target.value)}
                        onBlur={(e) => handleChangeCourse('title', e.target.value)}
                        error={titleError !== ""}
                        helperText={titleError ? titleError : ""}
                    />
                    <TextField
                        autoFocus
                        multiline
                        rows={4}
                        margin="dense"
                        id="description"
                        label="Description"
                        defaultValue={course.description}
                        fullWidth
                        variant="outlined"
                        onBlur={(e) => handleChangeCourse('description', e.target.value)}
                        onChange={(e) => validateDescription(e.target.value)}
                        error={descriptionError !== ""}
                        helperText={descriptionError ? descriptionError : ""}
                    />
                    {/* {course.isDeleted ? <> <FormControlLabel
                        control={<Checkbox checked={false} color="primary" disabled={true} />}
                        label="Deleted"
                    />
                        <br /></> : <></>
                    } */}
                    <FormControlLabel
                        control={<Checkbox checked={!!course.isPublic} onChange={(e) => handleChangeCourse('isPublic', !(course.isPublic))} color="primary" />}
                        label="Public"
                    />
                    <Box pt={2}><Button
                        color="primary"
                        component="label"
                        variant='outlined'
                    >
                        Upload Image
                        <input
                            type="file"
                            hidden
                            onChange={(e) => uploadImage(e)}
                        />
                    </Button>
                    </Box>
                </DialogContent>
                <DialogActions>
                    <Button onClick={closeDialogOnCancel} color="primary">
                        Cancel
                    </Button>
                    <Button onClick={handleSave} variant="contained" color="primary" disabled={validateEdit ? false : true} > {buttonLabel}</Button>

                </DialogActions>
            </Dialog>
        </div>
    );
};
export default EditCourse;
