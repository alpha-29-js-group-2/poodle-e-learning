import { makeStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import { useState } from "react";
import { useContext } from "react";
import { AddedStudentContext } from "../../../providers/EnrollStudentsContext";

const useStyles = makeStyles({
  root: {
    display: "flex",
    flexFlow: "row",
    justifyContent: "space-between",
    padding: "5px",
    minWidth: "300px",
    cursor: "default",
  },
  studentNames: {
    marginRight: "10px",
  },
});

export const CurrentlyEnrolledStudent = ({
  student: pStudent,
  courseId,
  unEnroll,
  viewUnenroll,
}) => {
  const classes = useStyles();

  const [student, setStudent] = useState(pStudent);

  const handleUnenroll = async () => {
    unEnroll(student.id, courseId);
    setStudent("");
  };

  return (
    <div>
      {student ? (
        <div className={classes.root}>
          <p className={classes.studentNames}>
            <b>
            {student.name} </b>
             - {student.email}
          </p>
          {viewUnenroll && (
            <Button
              className={classes.buttonUnenroll}
              onClick={handleUnenroll}
              variant="outlined"
              color="primary"
            >
              Unenroll student
            </Button>
          )}
        </div>
      ) : null}
    </div>
  );
};

export const CurrentlyNotEnrolledStudent = ({ student }) => {
  const { studentsToEnroll, setStudentsToEnroll } =
    useContext(AddedStudentContext);
  const classes = useStyles();

  const handleAdd = () => {
    setStudentsToEnroll([...studentsToEnroll, student.id]);
  };

  const handleRemove = () => {
    setStudentsToEnroll((current) => current.filter((id) => id !== student.id));
  };

  const added = studentsToEnroll.includes(student.id);

  return (
    <div>
      {student ? (
        <div className={classes.root}>
          <p className={classes.studentNames}>
            <b>{student.name} </b>- {student.email}
          </p>
          <Button
            className={classes.buttonUnenroll}
            onClick={added ? handleRemove : handleAdd}
            color={added ? "secondary" : "primary"}
            variant={added ? "text" : "outlined"}
          >
            {added ? "Remove" : "Add"} student
          </Button>
        </div>
      ) : null}
    </div>
  );
};
