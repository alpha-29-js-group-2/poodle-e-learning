import React, { useEffect, useState } from 'react';
import { getCourseById, getCourseImage } from '../../../requests/courses';
import { CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { Box, Grid, Typography, IconButton, Container, Divider } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AuthContext from '../../../providers/AuthContext';
import { useContext } from 'react';
import { deleteCourse } from '../../../requests/courses';
import { useHistory } from 'react-router-dom';
import EditCourse from '../EditCourse/EditCourse';
import AllSectionsInCourse from '../../Sections/AllSectionsInCourse/AllSectionsInCourse';
import { userRole } from '../../../common/user-role';
import CurrentlyEnrolledDialog from './CurrentlyEnrolledDialog';
import EnrollStudentsDialog from './EnrollStudentsDialog';
import Button from '@material-ui/core/Button';
import AddIcon from '@material-ui/icons/Add';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';

import { EnrolledStudentsDialogRefresh } from '../../../providers/EnrollStudentsContext';
import { getAuthor, unenrollStudent } from '../../../requests/users';
import AlternateActionDialog from '../../Dialog/AlternativeActionDialog';

const useStyles = makeStyles({
    root: {
        paddingTop: '30px',
    },
    editDeleteGrid: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    iconDiv: {
        position: "relative",
        display: "inline-flex",
        justifyContent: "center",
        alignItems: "center"
    },
    icon: {
        fontSize: "2.5em"
    },
    iconText: {
        position: "absolute",
        lineHeight: 1,
        color: "#fff",
        top: "0.5em",
        fontSize: "1em"
    },
    nPR: {
        paddingRight: 0,
    },
    img: {
        width: '100%',
        maxWidth: '912px',
        height: '400px',
    }
});

const Course = ({ match }) => {
    const courseId = match.params.id;
    const [course, setCourse] = useState(null);
    const [imageData, setImageData] = useState({});
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const { user } = useContext(AuthContext);
    const classes = useStyles();
    const history = useHistory();
    const [visibleEdit, setVisibleEdit] = useState(false);
    const [refreshedCourse, setRefreshedCourse] = useState(false);
    const [authorName, setAuthorName] = useState('');
    const [rearrange, setRearrange] = useState(false);

    const refreshCourse = () => {
        setRefreshedCourse(!refreshedCourse);
    };

    useEffect(() => {
        setLoading(true);
        getCourseById(courseId)
            .then(result => {
                setLoading(false);
                if (result.error) {
                    throw new Error(result.error);
                }
                setLoading(false);
                setCourse(result);
            })
            .catch(e => {
                setError(e.message);
            });

        getCourseImage(courseId)
            .then(imageBlob => {
                const localUrl = URL.createObjectURL(imageBlob);
                setImageData({
                    imagePreview: localUrl,
                });
            });
        getAuthor(courseId).then(data => setAuthorName(data.name));
    }, [courseId, refreshedCourse]);

    // Create Section dialog and All Sections re-render
    const handleCreateSectionPage = () => {
        history.push({
            pathname: "/sections/create",
            state: {
                courseId
            }
        })
    };

    const handleDelete = () => {
        deleteCourse(courseId)
            .then(r => {
                if (r?.isDeleted === 1) {
                    history.push("/courses");
                }
                setDeleteDialogInfo({ visible: false });
            })
    };

    // Edit course dialog and Course re-render
    const handleEdit = () => {
        setVisibleEdit(!visibleEdit);
    };

    //Dialog for currently enrolled students and to unenroll students.
    const [CEDialogBoxOpened, setCEDialogBoxOpened] = useState(false);
    const [CEDialogInfo, setCEDialogInfo] = useState({
        visible: false,
    });

    const onCEDialogClose = () => {
        setCEDialogInfo({
            visible: false,
        });
    };

    const showEnrolled = () => {
        setCEDialogInfo({
            visible: true,
        });
        setCEDialogBoxOpened(!CEDialogBoxOpened);
    };

    //Dialog to search for more students to enroll.
    const [ESDialogBoxOpened, setESDialogBoxOpened] = useState(false);
    const [ESDialogInfo, setESDialogInfo] = useState({
        visible: false,
    });

    const onESDialogClose = () => {
        setESDialogInfo({
            visible: false,
        });
    };

    const showEnrollMore = () => {
        setESDialogInfo({
            visible: true,
        });
        setESDialogBoxOpened(!ESDialogBoxOpened);
    };

    const [deleteDialogInfo, setDeleteDialogInfo] = useState({
        visible: false,
    });


    const openDeleteDialog = () => {
        setDeleteDialogInfo({
            visible: true,
            content: 'Are you sure you want to delete this course?',
        });
    }

    const onDeleteDialogClose = () => {
        setDeleteDialogInfo({
            visible: false,
        });
    }

    const deleteCourseAction = {
        label: "Delete",
        action: () => handleDelete(),
    }
    const [unenrollSelfDialogInfo, setUnenrollSelfDialogInfo] = useState({
        visible: false,
    });


    const openUnenrollSelfDialog = () => {
        setUnenrollSelfDialogInfo({
            visible: true,
            content: 'Are you sure you want to unenroll yourself from this course?',
        });
    }

    const onUnenrollSelfDialogClose = () => {
        setUnenrollSelfDialogInfo({
            visible: false,
        });
    }

    const handleUnenrollSelf = () => {
        unenrollStudent(user.id, courseId)
            .then(r => {
                if (!r?.error) {
                    history.push("/");
                    setDeleteDialogInfo({ visible: false });
                } else {
                    throw new Error(r.error);
                }
            }).catch((e) => alert(e));
    };

    const unenrollSelf = {
        label: "Unenroll",
        action: () => handleUnenrollSelf(),
    }

    const toggleRearrange = () => {
        setRearrange(!rearrange);
    }

    // show loading
    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    // show error msg
    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }

    if (course) {
        return (

            <Container maxWidth='md' style={{ textAlign: "left" }}>
                <EnrolledStudentsDialogRefresh.Provider
                    value={{
                        ESDialogBoxOpened, setESDialogBoxOpened,
                        CEDialogBoxOpened, setCEDialogBoxOpened
                    }}>
                    <CurrentlyEnrolledDialog
                        visible={CEDialogInfo.visible}
                        onClose={onCEDialogClose}
                        courseId={courseId}
                        opened={CEDialogBoxOpened}
                        isAuthor={user?.id === course.creator_id}
                    />
                    <EnrollStudentsDialog
                        visible={ESDialogInfo.visible}
                        onClose={onESDialogClose}
                        courseId={courseId}
                        opened={ESDialogBoxOpened}
                    />
                </EnrolledStudentsDialogRefresh.Provider>
                <Box pt={3} pb={5}>
                <Grid container >
                    <Grid item xs={12}>
                        <img src={imageData.imagePreview} className={classes.img} width="912" height="400" alt="Course img" />

                        </Grid>
                        <Grid item xs={10} className={classes.root} >
                            <Typography variant='h4' style={{ display: "inline" }}>
                                {course.title} by {user?.id === course.creator_id ? 'you' : authorName}
                            </Typography >
                        </Grid>
                        {
                            user.id === course.creator_id && (
                            <>
                                <AlternateActionDialog
                                    content={deleteDialogInfo.content}
                                    visible={deleteDialogInfo.visible}
                                    onClose={onDeleteDialogClose}
                                    alternativeAction={deleteCourseAction}
                                />
                                <Grid item xs={2} className={classes.editDeleteGrid}>
                                    {' '}<IconButton aria-label="edit" color="primary" onClick={handleEdit}>< EditIcon /></IconButton>
                                    <IconButton className={classes.nPR} aria-label="delete" color="primary" onClick={openDeleteDialog}>< DeleteIcon /></IconButton>
                                </Grid>
                            </>

                            )}
                        {
                            user.role === userRole.STUDENT && (
                                <>
                                    <AlternateActionDialog
                                        content={unenrollSelfDialogInfo.content}
                                        visible={unenrollSelfDialogInfo.visible}
                                        onClose={onUnenrollSelfDialogClose}
                                        alternativeAction={unenrollSelf}
                                    />
                                    <Grid item xs={2} className={classes.editDeleteGrid}>
                                        <Button variant="outlined" color="secondary" style={{padding: '10px'}} onClick={openUnenrollSelfDialog}>Unenroll</Button>
                                    </Grid>
                                </>
                            )
                        }
                    </Grid>
                    <Grid item xs={12} className={classes.root}>
                        <Typography>{course.description}</Typography>
                        {user?.role === userRole.TEACHER ? <p>{course.isPublic ? 'Public course' : 'Private course'}</p> : null}
                    </Grid>
                    <EditCourse visible={visibleEdit} handleEdit={handleEdit} course={Object.assign({}, course)} refreshCourse={refreshCourse} />
                {/* <br /> */}
                {user?.id === course.creator_id && !course.isPublic && <Button color="primary" variant="contained" onClick={showEnrollMore}>Enrol Students</Button>}{'  '}
                {user?.role === userRole.TEACHER && <Button color="primary" onClick={showEnrolled}>View Enrolled Students</Button>}
                </Box>
                <Box pb={10}>
                    <Divider />
                </Box>
                {user.role === userRole.STUDENT && <Box pb={3}> 
                <Typography variant='h4' style={{ display: 'inline', paddingRight: '8px'}}>Sections</Typography>
                </Box>}
                {
                    user.id === course.creator_id
                    && <Box style={{ display: 'flex', flexWrap: 'wrap', alignItems: 'center' }} pb={3}> <Typography variant='h4' style={{ display: 'inline', paddingRight: '8px' }}>Sections</Typography>
                        <IconButton aria-label="edit" color="primary" onClick={handleCreateSectionPage}>< AddIcon /></IconButton>
                        {user?.id === course.creator_id && <Button color="primary" onClick={toggleRearrange}>< DragIndicatorIcon />{rearrange ? 'Save' : 'Rearrange'} section order</Button>}
                    </Box>
                }
                <AllSectionsInCourse
                    courseId={courseId}
                    courseCreator={course.creator_id}
                    courseTitle={course.title}
                    courseRefresh={{ refreshedCourse, setRefreshedCourse }}
                    rearrange={rearrange}
                />
            </Container >
        )
    }

    return <></>
};
export default Course;
