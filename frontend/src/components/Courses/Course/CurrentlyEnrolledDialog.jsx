import React, { useState, useEffect } from 'react';
import { getEnrolledStudents, unenrollStudent } from '../../../requests/users';
import { CurrentlyEnrolledStudent } from './StudentRow';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Box from '@material-ui/core/Box';
import CircularProgress from '@material-ui/core/CircularProgress';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';


const DialogContent = withStyles(() => ({
    root: {
        margin: '20px',
        padding: 0,
        border: '1px solid gray',
        paddingTop: 0,
        '&:first-child': {
            paddingTop: 0,
        }
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default function CurrentlyEnrolledDialog({ visible, onClose, courseId, opened, isAuthor }) {

    const [loading, setLoading] = useState(true);
    const [enrolledStudents, setEnrolledStudents] = useState([]);
    const [error, setError] = useState(null);



    useEffect(() => {
        setLoading(true);
        const fetchData = async () => {
            const data = await getEnrolledStudents(courseId);
            if (data && !data.error) {
                setLoading(false);
                setEnrolledStudents(data);
            } else {
                setError(data.error);
            }         
        }
        fetchData();
        setLoading(false);
    }, [courseId, opened]);



    const unEnroll = async (studentId, courseId) => {
        const result = await unenrollStudent(studentId, courseId);
        if (!result.error) {
            setEnrolledStudents((prev) => prev.filter((student) =>
                student.id !== studentId
            ));
        }
        else {
            alert(result.error);
        }

    }

    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    // show error msg
    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }
    return (
        <div>
            <Dialog onClose={onClose} open={visible}>
                <DialogContent dividers >
                    {enrolledStudents?.length ? enrolledStudents.map((student) => (
                        <CurrentlyEnrolledStudent
                            key={student.id}
                            student={student}
                            courseId={courseId}
                            unEnroll={unEnroll}
                            viewUnenroll={isAuthor} />
                    )) :
                        <p style={{ padding: '10px' }}>
                            There are currently no enrolled students for this course.
                        </p>}
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={onClose} color="primary">
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
