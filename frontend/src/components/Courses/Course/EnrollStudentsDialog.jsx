import React, { useState, useEffect, useContext } from 'react';
import { getNotEnrolledStudents, enrollStudents } from '../../../requests/users';
import { CurrentlyNotEnrolledStudent } from './StudentRow';
import { withStyles } from '@material-ui/core/styles';
import { AddedStudentContext, EnrolledStudentsDialogRefresh } from '../../../providers/EnrollStudentsContext';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';


const DialogContent = withStyles(() => ({
    root: {
        margin: '20px',
        padding: 0,
        border: '1px solid gray',
        paddingTop: 0,
        '&:first-child': {
            paddingTop: 0,
        }
    },
}))(MuiDialogContent);

const DialogSearch = withStyles(() => ({
    root: {
        marginTop: '10px',
        marginBottom: '20px',
        width: '100%'
    },
}))(TextField);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

export default function EnrollStudentsDialog({ visible, onClose, courseId,  }) {
    const [loading, setLoading] = useState(true);
    const [displayedStudents, setDisplayedStudents] = useState([]);
    const [studentsToEnroll, setStudentsToEnroll] = useState([]);
    const [error, setError] = useState(null);
    const [searchTerm, setSearchTerm] = useState('');

    const { ESDialogBoxOpened, setESDialogBoxOpened } = useContext(EnrolledStudentsDialogRefresh);

    useEffect(() => {
        setLoading(true);
        const fetchData = async () => {
            const data = await getNotEnrolledStudents(courseId, searchTerm);
            if (data && !data.error) {
                setLoading(false);
                setDisplayedStudents(data);
            } else {
                setError(data.error);
            }
        }
        fetchData();
        setLoading(false);
    }, [courseId, ESDialogBoxOpened, searchTerm]);

    const handleEnrollStudents = async () => {
        const result = await enrollStudents(studentsToEnroll, courseId);
        if (!result.error) {
            setStudentsToEnroll([]);
            setESDialogBoxOpened(!ESDialogBoxOpened);
        }
        else {
            alert(result.error);
        }
    }


    const handleClose = () => {
        onClose();

        //This is to make sure the window closes before the search
        //Term is set to '', making the window grow back up, making
        //up for the time needed for the close animation. 
        //Set the timer to 10 to see what I mean.
        setTimeout(() => setSearchTerm(''), 200);
    }

    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    // show error msg
    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }



    return (
        <div>
            <Dialog onClose={onClose} open={visible}>
                <DialogContent dividers >
                    <DialogSearch
                        autoFocus
                        label="Search Terms"
                        id="search-students"
                        placeholder="Search Students"
                        onChange={(e) => setSearchTerm(e.target.value)}
                    />
                    <AddedStudentContext.Provider value={{studentsToEnroll, setStudentsToEnroll}}>
                        {displayedStudents?.length ? displayedStudents.map((student) => (
                            <CurrentlyNotEnrolledStudent
                                key={student.id}
                                student={student}
                                courseId={courseId}
                                studentAdded={studentsToEnroll.includes(student.id)}
                                />
                        )) :
                            <p style={{ padding: '10px' }}>
                                No students found with these search terms.
                            </p>}
                    </AddedStudentContext.Provider>

                    <Button disabled={!studentsToEnroll.length} onClick={handleEnrollStudents}>
                        Enroll Student{studentsToEnroll.length > 1 && 's'}
                    </Button>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={handleClose} color="primary">
                        Close
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}
