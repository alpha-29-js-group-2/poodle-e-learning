import React, { useState, useEffect } from 'react';
import { getAllCourses } from '../../../requests/courses';
import CourseCard from '../CourseCard/CourseCard';
import { Box, CircularProgress, TextField, Grid, makeStyles } from '@material-ui/core';
import PaginationView from '../../Pagination/PaginationView';
import { PAGE_SIZE } from '../../../common/constants';

const useStyles = makeStyles(() => ({
    searchContainer: {
        width: '65.85%',
    },
    searchBar: {
        width: '100%',
        marginLeft: 0,
    },
    createCourseButton: {
        height: '40px',
        marginRight: 0,
    },
    cardsContainer: {
        marginTop: '10px',
        marginBottom: '10px',
    },
    nPL: {
        paddingLeft: 0,
    },
    nPR: {
        paddingRight: 0,
    },
}));

const MyCourses = () => {
    const classes = useStyles();
    const [courses, setCourses] = useState([]);
    const [totalResults, setTotalResults] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [pageSize, setPageSize] = useState(PAGE_SIZE.SIX);
    const [search, setSearch] = useState("");

    useEffect(() => {
        const offset = (currentPage - 1) * pageSize;

        getAllCourses(offset, pageSize, search)
            .then(result => {
                if (result.results) {
                    setCourses(result.results);
                    setTotalResults(result.total);
                    setLoading(false);

                } 
                else if (result.error === "No courses found.") {
                    setLoading(false);
                    setCourses([]);
                }
                else {
                    setLoading(false);
                    throw new Error(result.error);
                }
            })
            .catch(e => setError(e.message));
    }, [currentPage, pageSize, search]);

    const handleChange = (e, page) => {
        setCurrentPage(page);
    }

    const handleChangePageSize = (e) => {
        setPageSize(e.target.value);
        setCurrentPage(1);

    };

    // show loading
    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    // show error msg
    if (error) {
        return <Box p={5}><p>{error}</p></Box>;  
    }

    // show My courses
    return (
        <>
            <h1>All Courses</h1>
            <Box p={2} className={`${classes.nPL} ${classes.nPR} ${classes.searchContainer}`}>
                <TextField
                    size="small"
                    id="outlined-search"
                    label="Search field"
                    type="search"
                    variant="outlined"
                    className={classes.searchBar}
                    onChange={(e) => setSearch(e.target.value)}
                />
            </Box>
            <Grid
                container
                spacing={4}
                className={classes.cardsContainer}
            >
                {!loading &&
                    courses.length === 0
                    ? <Box p={2} m="auto"><p>This topic has no results. Try another one</p></Box>
                    : courses.map(c => <Grid item sm={4} key={c.id}><CourseCard course={c} /></Grid>)}
            </Grid>
            <PaginationView pageSize={pageSize} handleChangePageSize={handleChangePageSize} totalResults={totalResults} currentPage={currentPage} handleChange={handleChange} />
        </>)
}

export default MyCourses;
