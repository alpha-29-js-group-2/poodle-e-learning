import React, { useState, useEffect, useContext } from 'react';
import { getAllMyCourses } from '../../../requests/courses';
import CourseCard from '../CourseCard/CourseCard';
import { CircularProgress, Button, Grid, Box, TextField, Typography, makeStyles } from '@material-ui/core';
import AuthContext from '../../../providers/AuthContext';
import { userRole } from '../../../common/user-role';
import { useHistory } from 'react-router-dom';
import PaginationView from '../../Pagination/PaginationView';
import { PAGE_SIZE } from '../../../common/constants';

const useStyles = makeStyles(() => ({
    searchContainer: {
        width: '65.85% !important',
    },
    controlsContainer: {
        display: 'flex',
        flexFlow: 'row',
        alignItems: 'center',
        justifyContent: 'space-between',
    },
    searchBar: {
        width: '100%',
        marginLeft: 0,
    },
    createCourseButton: {
        height: '40px',
        marginRight: 0,
    },
    cardsContainer: {
        marginTop: '10px',
        marginBottom: '10px',
    },
    nPL: {
        paddingLeft: 0,
    },
    jCFE: {
        display: 'flex',
        justifyContent: 'flex-end',
        alignItems: 'flex-end',
    },
    nPR: {
        paddingRight: 0,
    },
}));

const MyCourses = () => {

    const [courses, setCourses] = useState([]);
    const [totalResults, setTotalResults] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [pageSize, setPageSize] = useState(PAGE_SIZE.SIX);
    const [search, setSearch] = useState("");
    const { user } = useContext(AuthContext);
    const history = useHistory();

    const classes = useStyles();

    useEffect(() => {
        const offset = (currentPage - 1) * pageSize;
        getAllMyCourses(offset, pageSize, search)
            .then(result => {
                if (result.results) {
                    setCourses(result.results);
                    setTotalResults(result.total);
                    setLoading(false);
                }
                else if (result.error === "No courses found!") {
                    setLoading(false);
                    setCourses([]);
                }
                else {
                    setLoading(false);
                    throw new Error(result.error);
                }
            })
            .catch(e => setError(e.message));
    }, [currentPage, pageSize, search]);

    const handleChange = (e, page) => {
        setCurrentPage(page);
    }

    const handleChangePageSize = (e) => {
        setPageSize(e.target.value);
        setCurrentPage(1);

    };

    const openCreate = () => {
        history.push('/courses/create');
    };


    // show loading
    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    // show error msg
    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }

    // show My courses
    return (
        <>
            <h1>My Courses</h1>
            <Grid container className={classes.controlsContainer}>

                <Grid item className={`${classes.searchContainer} ${classes.nPR}`}>
                    <Box p={2} className={`${classes.nPL} ${classes.nPR}`}>
                        <TextField
                            size="small"
                            id="outlined-search"
                            label="Search field"
                            type="search"
                            variant="outlined"
                            className={classes.searchBar}
                            onChange={(e) => setSearch(e.target.value)}
                        />
                    </Box>
                </Grid>
                {user && user.role === userRole.TEACHER && (
                    <Grid item sm={4}>
                        <Box p={2} className={`${classes.jCFE} ${classes.nPR}`}>
                            <Button className={classes.createCourseButton} variant="outlined" color="primary" onClick={openCreate}>
                                Create course
                            </Button>
                        </Box>
                    </Grid>
                )}

            </Grid>
            <Grid
                container
                spacing={4}
                className={classes.cardsContainer}
            >
                {!loading &&
                    courses.length === 0
                    ? <Box p={2} m="auto"><Typography variant="h6">No courses found.</Typography></Box>
                    : courses.map(c => <Grid item sm={4} key={c.id}><CourseCard course={c} /></Grid>)}
            </Grid>
            <PaginationView pageSize={pageSize} handleChangePageSize={handleChangePageSize} totalResults={totalResults} currentPage={currentPage} handleChange={handleChange} />
        </>)
}

export default MyCourses;
