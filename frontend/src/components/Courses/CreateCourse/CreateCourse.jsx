import React, { useState } from 'react';
import { TextField, Button, FormControl, FormGroup, FormControlLabel, Checkbox } from '@material-ui/core';
import { addImage, createCourse } from '../../../requests/courses';
import Container from '../../Container/Container';
import { useHistory } from 'react-router-dom';
import Box from '@material-ui/core/Box';
import Dialog from '../../Dialog/Dialog'
import { MAX_DESCRIPTION_LENGTH, MAX_TITLE_LENGTH, MIN_DESCRIPTION_LENGTH, MIN_TITLE_LENGTH } from '../../../common/constants';

const CreateCourse = () => {
    const [checked, setChecked] = useState(false);
    const [titleError, setTitleError] = useState("");
    const [descriptionError, setDescriptionError] = useState("");
    const [image, setImage] = useState({});
    const history = useHistory();
    const [uploadVisible, setUploadVisible] = useState(false);
    const [dialogInfo, setDialogInfo] = useState({
        visible: false
    });
    const [titleChecked, setTitleChecked] = useState(false);
    const [descriptionChecked, setDescriptionChecked] = useState(false);

    let courseId;

    const [course, setCourse] = useState({
        title: '',
        description: '',
        isPublic: '',
    });


    const validateTitle = (title) => {
        if (title !== '' && title.length >= MIN_TITLE_LENGTH &&
            title.length <= MAX_TITLE_LENGTH) {
            setTitleChecked(true);
            return setTitleError("");
        }
        setTitleChecked(false);
        return setTitleError(`Title must be between ${MIN_TITLE_LENGTH} and ${MAX_TITLE_LENGTH} symbols`)
    };

    const validateDescription = (description) => {
        if (description !== '' && description.length >= MIN_DESCRIPTION_LENGTH &&
            description.length <= MAX_DESCRIPTION_LENGTH) {
            setDescriptionChecked(true);
            return setDescriptionError("");
        }
        setDescriptionChecked(false);
        return setDescriptionError(`Description must be between ${MIN_DESCRIPTION_LENGTH} and ${MAX_DESCRIPTION_LENGTH} symbols`);

    };

    const validateCreate = [titleChecked, descriptionChecked].every(e => e);


    const showUpload = (value) => {
        setUploadVisible(!value);
    };

    const onDialogClose = () => {
        setDialogInfo({
            visible: false
        })
    };

    const handleChangeCourse = (key, value) => {
        if (key === "isPublic") {
            setChecked(value);
        }
        course[key] = value;
        setCourse({ ...course });
    };

    const createNewCourse = () => {
        setTitleError("");
        setDescriptionError("");
        createCourse(course)
            .then(result => {

                if (result.error) {
                    setTitleError(result.error);
                    throw new Error(result.error);
                }
                return result;
            })
            .then(r => {
                courseId = r.id;
                if (!image.imageAsFile) {
                    return r;
                }
                return addImage(r.id, image);
            })
            .then((r) => {
                history.push(`/courses/${courseId}`);
            })
            .catch(e => e)
    };

    const uploadImage = (e) => {
        const regex = new RegExp("(.*?)\.(png|jpg)$");
        if (!regex.test(e.currentTarget.value)) {
            setDialogInfo({
                content: "Incorrect file. Select jpg or png instead",
                visible: true

            })
            e.currentTarget.value = "";
            return
        }

        setImage({
            imagePreview: URL.createObjectURL(e.target.files[0]),
            imageAsFile: e.target.files[0],
        });
    };

    return (
        < Container>
            <Dialog content={dialogInfo.content} visible={dialogInfo.visible} onClose={onDialogClose}></Dialog>
            <FormControl>
                <h1>Create a Course</h1>
                <Box pt={1} pb={4}>
                    <TextField
                        autoFocus
                        margin="dense"
                        label="Title"
                        type="title"
                        required={true}
                        size="medium"
                        variant="outlined"
                        style={{ width: 345 }}
                        onChange={(e) => validateTitle(e.target.value)}
                        onBlur={(e) => handleChangeCourse('title', e.target.value)}
                        error={titleError !== ""}
                        helperText={titleError ? titleError : ""}
                    />
                </Box>
                <Box pb={2}>
                    <TextField
                        label="Content"
                        multiline
                        rows={4}
                        required={true}
                        size="medium"
                        variant="outlined"
                        style={{ width: 345 }}
                        onChange={(e) => validateDescription(e.target.value)}
                        onBlur={(e) => handleChangeCourse('description', e.target.value)}

                        error={descriptionError !== ""}
                        helperText={descriptionError ? descriptionError : ""}
                    />
                </Box>
                <FormGroup row>
                    <FormControlLabel
                        control={<Checkbox checked={checked} onChange={(e) => handleChangeCourse('isPublic', !checked)} color="primary" />}
                        label="Public"
                    />
                    <FormControlLabel
                        control={<Checkbox checked={uploadVisible} onChange={(e) => showUpload(uploadVisible)} color="primary" />}
                        label="Custom Image"
                    />
                </FormGroup>
                {uploadVisible
                    ? <Box p={2}><Button
                        color="primary"
                        component="label"
                        variant='outlined'
                    >
                        Upload Image
                        <input
                            type="file"
                            hidden
                            onChange={(e) => uploadImage(e)}
                        />
                    </Button>
                    </Box>

                    : <Box p={4.3}></Box>
                }
                <Button onClick={createNewCourse} variant="contained" color="secondary" disabled={validateCreate ? false : true} > Create</Button>

            </FormControl>
        </Container >
    )
};

export default CreateCourse;
