import React, { useContext } from "react";
import { makeStyles } from '@material-ui/core/styles';
import { NavLink, withRouter } from "react-router-dom";
import AuthContext from "../../providers/AuthContext";
import "@fontsource/roboto";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from '@material-ui/core/Toolbar';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import ClickAwayListener from '@material-ui/core/ClickAwayListener';
import Grow from '@material-ui/core/Grow';
import Paper from '@material-ui/core/Paper';
import Popper from '@material-ui/core/Popper';
import MenuItem from '@material-ui/core/MenuItem';
import MenuList from '@material-ui/core/MenuList';
import Container from "@material-ui/core/Container";
import { logoutRequest } from "../../requests/auth";
import { userRole } from "../../common/user-role";
import ArrowDropDownIcon from '@material-ui/icons/ArrowDropDown';
import { Grid } from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
  nPL: {
    paddingLeft: 0,
  },
  nPR: {
    paddingRight: 0,
  },
  navLink: {
    color: '#333',
    textDecoration: 'none',
    '& a': {
      color: '#333',
      textDecoration: 'none',
      '&:visited': {
        color: '#333',
        textDecoration: 'none',
      },
      '&:hover': {
        borderBottom: '1px solid #2989fc',
      },
    },
  },
  title: {
    flexGrow: 1,
  },
  mainNavigation: {
    display: 'flex',
    flexFlow: 'row',
    justifyContent: 'flex-start',
    '& $navLink': {
      marginRight: theme.spacing(3),
    },
  },
  authNavigation: {
    display: 'flex',
    flexFlow: 'row',
    justifyContent: 'flex-end',
    '& $menuButton': {
      marginRight: theme.spacing(1),
    },
  },
  menuButton: {
    textDecoration: 'none',
    color: 'black',

  },
}));

const navLinkActive = {
  borderBottom: '1px solid #2989fc',
}

const menuButtonActive = {
  borderBottom: '1px solid #2989fc',
};


const Header = ({ history, location }) => {
  const classes = useStyles();
  const { isLoggedIn, user, setLoginState } = useContext(AuthContext);

  const handleClose = (event) => {
    if (anchorRef.current && anchorRef.current.contains(event.target)) {
      return;
    }

    setOpen(false);
  };

  const logout = async (event) => {
    handleClose(event);
    await logoutRequest();
    history.push("/");
    localStorage.removeItem('token');
    setLoginState({
      isLoggedIn: false,
      user: null,
    });
  };

  const [open, setOpen] = React.useState(false);
  const anchorRef = React.useRef(null);

  const handleToggle = () => {
    setOpen((prevOpen) => !prevOpen);
  };

  function handleListKeyDown(event) {
    if (event.key === 'Tab') {
      event.preventDefault();
      setOpen(false);
    }
  }

  // return focus to the button when we transitioned from !open -> open
  const prevOpen = React.useRef(open);
  React.useEffect(() => {
    if (prevOpen.current === true && open === false) {
      anchorRef.current.focus();
    }

    prevOpen.current = open;
  }, [open]);
  return (
    /*     <div>
          {" | "}
          <NavLink to="/">Home</NavLink>
          {" | "}
          {isLoggedIn && <NavLink to="/courses">My Courses</NavLink> + " | "}
          {isLoggedIn && user?.role === userRole.TEACHER && <NavLink to="/courses">All Courses</NavLink>}
          <div className="auth-navigation">
            {!isLoggedIn && location.pathname !== '/register' && <NavLink to="/register">Register</NavLink>}
            {!isLoggedIn && location.pathname !== '/login' && <NavLink to="/login">Login</NavLink>}
            {isLoggedIn && <button onClick={logout}>Logout</button>}
          </div>
        </div> */
    <AppBar position="sticky" color="default">
      <Container maxWidth="lg" className={`${classes.nPL} ${classes.nPR}`}>
        <Toolbar>
          <Grid container>
            <Grid item className={`${classes.mainNavigation} ${classes.nPL}`} xs={10} >
              <Typography variant="h6" className={classes.navLink}><NavLink exact to="/" activeStyle={navLinkActive}>Home</NavLink></Typography>
              {isLoggedIn && (<Typography variant="h6" className={classes.navLink}>
                <NavLink exact to="/courses" activeStyle={navLinkActive}>
                  My Courses
                </NavLink>
              </Typography>)}
              {isLoggedIn && user?.role === userRole.TEACHER &&
                (<Typography variant="h6" className={classes.navLink}>
                  <NavLink to="/courses/all" activeStyle={navLinkActive}>All Courses</NavLink>
                </Typography>)}
              {isLoggedIn && user?.role === userRole.TEACHER &&
                (<Typography variant="h6" color="primary" className={classes.navLink}>
                  <NavLink to="/courses/create" activeStyle={navLinkActive}>Create Course</NavLink>
                </Typography>)}
              {isLoggedIn && user?.role === userRole.TEACHER &&
                (<Typography variant="h6" className={classes.navLink}>
                  <NavLink to="/courses/deleted" activeStyle={navLinkActive}>Archive</NavLink>
                </Typography>)}
            </Grid>
            <Grid item className={classes.authNavigation} xs={2}>
              <Button
                ref={anchorRef}
                aria-controls={open ? 'menu-list-grow' : undefined}
                aria-haspopup="true"
                onClick={handleToggle}
                className={`${classes.navLink} ${classes.nPR}`}
              >
                {!isLoggedIn ? 'Sign in' : user.email}<ArrowDropDownIcon />
              </Button>
              <Popper open={open} anchorEl={anchorRef.current} role={undefined} transition disablePortal>
                {({ TransitionProps, placement }) => (
                  <Grow
                    {...TransitionProps}
                    style={{ transformOrigin: placement === 'bottom' ? 'center top' : 'center bottom' }}
                  >
                    <Paper>
                      {!isLoggedIn && (<ClickAwayListener onClickAway={handleClose}>
                        <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>

                          <MenuItem onClick={handleClose}>
                            <NavLink
                              to="/login"
                              className={classes.menuButton}
                              activeStyle={menuButtonActive}
                            >
                              Login
                            </NavLink>
                          </MenuItem>
                          <MenuItem onClick={handleClose}><NavLink className={classes.menuButton} to="/register" 
                              activeStyle={menuButtonActive}>Register</NavLink></MenuItem>

                        </MenuList>
                      </ClickAwayListener>
                      )}

                      {isLoggedIn && (
                        <ClickAwayListener onClickAway={handleClose}>
                          <MenuList autoFocusItem={open} id="menu-list-grow" onKeyDown={handleListKeyDown}>
                            <MenuItem onClick={handleClose}><NavLink className={classes.menuButton} to="/user" 
                              activeStyle={menuButtonActive}>My account</NavLink></MenuItem>
                            <MenuItem onClick={logout}>Logout</MenuItem>
                          </MenuList>
                        </ClickAwayListener>
                      )}
                    </Paper>
                  </Grow>
                )}
              </Popper>

            </Grid>
          </Grid>

        </Toolbar>
      </Container>
    </AppBar>

  );
};

export default withRouter(Header);
