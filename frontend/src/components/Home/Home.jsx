import React, { useState, useEffect } from 'react';
import CourseCard from '../Courses/CourseCard/CourseCard';
import { CircularProgress, Grid, makeStyles } from '@material-ui/core';
import Container from '../Container/Container';
import Box from '@material-ui/core/Box';
import { getAllPublicCourses } from '../../requests/courses';
import PaginationView from '../Pagination/PaginationView';
import { PAGE_SIZE } from '../../common/constants';

const useStyles = makeStyles(() => ({
    cardsContainer: {
        marginTop: '10px',
        marginBottom: '10px',
    },
    nPL: {
        paddingLeft: 0,
    },
    nPR: {
        paddingRight: 0,
    },
}));

const Home = () => {
    const classes = useStyles();
    const [courses, setCourses] = useState([]);
    const [totalResults, setTotalResults] = useState(null);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const [currentPage, setCurrentPage] = useState(1);
    const [pageSize, setPageSize] = useState(PAGE_SIZE.SIX);

    useEffect(() => {
        const offset = (currentPage - 1) * pageSize;

        getAllPublicCourses(offset, pageSize)
            .then(result => {
                if (result.results) {
                    setCourses(result.results);
                    setTotalResults(result.total);
                    setLoading(false);

                } else {
                    throw new Error(result.error);
                }
            })
            .catch(e => setError(e.message));
    }, [currentPage, pageSize]);

    const handleChange = (e, page) => {
        setCurrentPage(page);
    }

    const handleChangePageSize = (e) => {
        setPageSize(e.target.value);
        setCurrentPage(1);

    };


    // show loading
    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    // show error msg
    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }

    // show All Public courses
    return (
        <Container maxWidth="lg">
            <h1>Home</h1>
            <Grid
                container
                spacing={4}
                className={classes.cardsContainer}
            >
                {!loading &&
                    courses.length === 0
                    ? <p>This topic has no results. Try another one</p>
                    : courses.map(c => <Grid item sm={4} key={c.id}><CourseCard course={c} /></Grid>)}
            </Grid>
            <PaginationView pageSize={pageSize} handleChangePageSize={handleChangePageSize} totalResults={totalResults} currentPage={currentPage} handleChange={handleChange} />
        </Container >)
};
export default Home;