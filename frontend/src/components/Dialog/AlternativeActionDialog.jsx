import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
// import MuiDialogTitle from '@material-ui/core/DialogTitle';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import Typography from '@material-ui/core/Typography';

const DialogContent = withStyles((theme) => ({
    root: {
        padding: theme.spacing(2),
    },
}))(MuiDialogContent);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

const AlternateActionDialog = ({ visible, content, onClose, alternativeAction }) => {
    return (
        <div>
            <Dialog onClose={onClose} open={visible}>
                <DialogContent dividers style={{ width: 345 }} >
                    <Typography gutterBottom>
                        {content}
                    </Typography>
                </DialogContent>
                <DialogActions>
                    <Button autoFocus onClick={alternativeAction.action} color="secondary">
                        {alternativeAction.label}
                    </Button>
                    <Button autoFocus onClick={onClose} color="primary">
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

AlternateActionDialog.defaultProps = {
    content: '',
  }

AlternateActionDialog.propTypes = {
    visible: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    content: PropTypes.string,
    alternativeAction: PropTypes.object.isRequired,
}

export default AlternateActionDialog;