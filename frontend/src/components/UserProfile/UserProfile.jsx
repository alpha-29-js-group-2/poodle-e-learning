import React, { useState, useEffect } from "react";
import TextField from '@material-ui/core/TextField';
import { EMAIL_REGEX, MAX_FIRSTNAME_LENGTH, MAX_LASTNAME_LENGTH, MAX_PASSWORD_LENGTH, MIN_FIRSTNAME_LENGTH, MIN_LASTNAME_LENGTH, MIN_PASSWORD_LENGTH } from "../../common/constants";
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Grid } from "@material-ui/core";
import Dialog from '../Dialog/Dialog';
import { getUserInfo, updateUserInfo, updateUserPass } from "../../requests/users";

const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexFlow: 'row',
    flexWrap: 'wrap',
    margin: '0 auto',
    justifyContent: 'center',
  },
  gridItem: {
    display: 'flex',
    '&:nth-child(2n)': {
      justifyContent: 'flex-start',
    },
    '&:nth-child(2n+1)': {
      justifyContent: 'flex-end',
    },
  },
  inputField: {
    minHeight: '70px',
    minWidth: '250px',
    margin: '20px 10px',
  },
  authButton: {
    margin: '0',
  }
}));

const UserProfile = () => {

  const classes = useStyles();

  const [user, setUser] = useState({
    firstName: "",
    lastName: "",
  });
  const [confirmPassword, setConfirmPassword] = useState("");
  const [newPassword, setNewPassword] = useState("");
  useEffect(() => {
    getUserInfo()
      .then(r => {
        setUser({
          ...user,
          firstName: r.firstname,
          lastName: r.lastname
        })
      })
  }, []);

  const [dialogInfo, setDialogInfo] = useState({
    visible: false
  });

  const onDialogClose = () => {
    setDialogInfo({
      visible: false
    });
  };

  const [passwordError, setPasswordError] = useState('');
  const [firstNameError, setFirstNameError] = useState('');
  const [lastNameError, setLastNameError] = useState('');
  const [oldPasswordError, setOldPasswordError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');

  const [oldPassword, setOldPassword] = useState('');
  const [passwordChecked, setPasswordChecked] = useState(false);
  const [oldPasswordChecked, setOldPasswordChecked] = useState(false);
  const [confirmPasswordChecked, setConfirmPasswordChecked] = useState(false);
  const [firstNameChecked, setFirstNameChecked] = useState(false);
  const [lastNameChecked, setLastNameChecked] = useState(false);


  const checkPassword = (password) => {
    if (password === '') {
      setPasswordChecked(false);
      return setPasswordError('Please, enter a password!');
    } else if (password.length < MIN_PASSWORD_LENGTH ||
      EMAIL_REGEX.length > MAX_PASSWORD_LENGTH) {
      setPasswordChecked(false);
      return setPasswordError(`Please, enter between \
  ${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} characters!`);
    } else {
      setPasswordChecked(true);
      return setPasswordError('');
    }
  }

  const validatePassword = (password) => {
    if (password && password.length >= MIN_PASSWORD_LENGTH &&
      password.length <= MAX_PASSWORD_LENGTH) {
      setPasswordError('');
      setPasswordChecked(true);
    }
  }

  const checkConfirmPassword = (confirmPassword) => {
    if (confirmPassword !== newPassword) {
      setConfirmPasswordChecked(false);
      return setConfirmPasswordError(`Passwords don't match.`);
    } else {
      setConfirmPasswordChecked(true);
      return setConfirmPasswordError('');
    }
  }

  const checkOldPassword = (password) => {
    if (password === '') {
      setOldPasswordChecked(false);
      return setOldPasswordError('Please, enter a password!');
    } else if (password.length < MIN_PASSWORD_LENGTH ||
      EMAIL_REGEX.length > MAX_PASSWORD_LENGTH) {
      setOldPasswordChecked(false);
      return setOldPasswordError(`Please, enter between \
    ${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} characters!`);
    } else {
      setOldPasswordChecked(true);
      return setOldPasswordError('');
    }
  }

  const checkFirstName = (firstName) => {
    if (firstName === '') {
      setFirstNameChecked(false);
      return setFirstNameError('Please, enter your first name!');
    } else if (firstName.length < MIN_FIRSTNAME_LENGTH ||
      EMAIL_REGEX.length > MAX_FIRSTNAME_LENGTH) {
      setFirstNameChecked(false);
      return setFirstNameError(`Please, enter between \
  ${MIN_FIRSTNAME_LENGTH} and ${MAX_FIRSTNAME_LENGTH} characters!`);
    } else {
      setFirstNameChecked(true);
      return setFirstNameError('');
    }
  }

  const validateFirstName = (firstName) => {
    if (firstName && firstName.length >= MIN_FIRSTNAME_LENGTH &&
      firstName.length <= MAX_FIRSTNAME_LENGTH) {
      setFirstNameError('');
      setFirstNameChecked(true);
    }
  }

  const checkLastName = (lastName) => {
    if (lastName === '') {
      setLastNameChecked(false);
      return setLastNameError('Please, enter your last name!');
    } else if (lastName.length < MIN_LASTNAME_LENGTH ||
      EMAIL_REGEX.length > MAX_LASTNAME_LENGTH) {
      setLastNameChecked(false);
      return setLastNameError(`Please, enter between \
  ${MIN_LASTNAME_LENGTH} and ${MAX_LASTNAME_LENGTH} characters!`);
    } else {
      setLastNameChecked(true);
      return setLastNameError('');
    }
  }

  const validateLastName = (lastName) => {
    if (lastName && lastName.length >= MIN_LASTNAME_LENGTH &&
      lastName.length <= MAX_LASTNAME_LENGTH) {
      setLastNameError('');
      setLastNameChecked(true);
    }
  }


  const updateFirstName = (firstName) => {
    validateFirstName(firstName);
    return setUser({ ...user, firstName });
  }
  const updateLastName = (lastName) => {
    validateLastName(lastName);
    return setUser({ ...user, lastName });
  }

  const updateOldPassword = (password) => {
    validatePassword(password);
    return setOldPassword(password);
  }
  const updatePassword = (password) => {
    validatePassword(password);
    return setNewPassword(password);
  }
  const updateConfirmPassword = (confirmPassword) => {
    checkConfirmPassword(confirmPassword);
    return setConfirmPassword(confirmPassword);
  }
  const validateSave = [firstNameChecked, lastNameChecked].some(e => e);
  const validatePasswordSave = [oldPasswordChecked, passwordChecked, confirmPasswordChecked].every(e => e);

  const saveUpdate = async () => {

    updateUserInfo(user)
      .then(r => {
        setDialogInfo({
          visible: true,
          content: 'Your profile info was updated'
        })
        setFirstNameChecked(false);
        setLastNameChecked(false);
      })
  }

  const savePasswordUpdate = () => {
    updateUserPass(oldPassword, newPassword)
      .then(r => {
        console.log(r)
        if (r.error) {
          setOldPasswordError(r.error);
          setOldPasswordChecked(false);
        } else {
          setDialogInfo({
            visible: true,
            content: 'Your password was updated'
          })
          setOldPasswordChecked(false);
          setPasswordChecked(false);
          setConfirmPasswordChecked(false);
          setNewPassword("");
          setOldPassword("");
          setConfirmPassword("");
        }

      })
  }

  return (
    <>
      <h1>Edit Profile</h1>
      <Dialog content={dialogInfo.content} visible={dialogInfo.visible} onClose={onDialogClose} />
      <form className={classes.root}>
        <Grid container>
          <Grid className={classes.gridItem} item xs={12} sm={6}>
            <TextField
              className={classes.inputField}
              label="First Name"
              id="first-name"
              required={true}
              value={user.firstName}
              onChange={(e) => updateFirstName(e.target.value)}
              onBlur={(e) => checkFirstName(e.target.value)}
              error={firstNameError !== ""}
              helperText={firstNameError ? firstNameError : ""} />
          </Grid>
          <Grid className={classes.gridItem} item xs={12} sm={6}>
            <TextField
              className={classes.inputField}
              label="Last Name"
              id="last-name"
              required={true}
              value={user.lastName}
              onChange={(e) => updateLastName(e.target.value)}
              onBlur={(e) => checkLastName(e.target.value)}
              error={lastNameError !== ""}
              helperText={lastNameError ? lastNameError : ""} />
          </Grid>
          <Grid item xs={12}>
            <Button
              disabled={validateSave ? false : true}
              variant="contained" className={classes.authButton}
              color="primary" onClick={saveUpdate}>
              Edit Name
            </Button>
          </Grid>
        </Grid>
        <Grid container>
          <Grid className={classes.gridItem} item xs={12} sm={6}>
            <TextField
              className={classes.inputField}
              label="Old Password"
              id="old-password"
              type="password"
              required={true}
              value={user.password}
              autoComplete="current-password"
              onChange={(e) => updateOldPassword(e.target.value)}
              onBlur={(e) => checkOldPassword(e.target.value)}
              error={oldPasswordError !== ""}
              helperText={oldPasswordError ? oldPasswordError : ""} />
          </Grid>
          <Grid className={classes.gridItem} item xs={12} sm={6}>
            <TextField
              className={classes.inputField}
              label="New Password"
              id="password"
              type="password"
              required={true}
              value={user.password}
              autoComplete="current-password"
              onChange={(e) => updatePassword(e.target.value)}
              onBlur={(e) => checkPassword(e.target.value)}
              error={passwordError !== ""}
              helperText={passwordError ? passwordError : ""} />
          </Grid>
          <Grid className={classes.gridItem} item xs={12} sm={6}>
            <TextField
              className={classes.inputField}
              label="Confirm Password"
              id="confirm-password"
              type="password"
              required={true}
              value={confirmPassword}
              onChange={(e) => updateConfirmPassword(e.target.value)}
              error={confirmPasswordError !== ""}
              helperText={confirmPasswordError ? confirmPasswordError : ""} />
          </Grid>
          <Grid item xs={12}>
            <Button
              disabled={validatePasswordSave ? false : true}
              variant="contained" className={classes.authButton}
              color="primary" onClick={savePasswordUpdate}>
              Update Password
            </Button>
          </Grid>

        </Grid>

      </form>

    </>
  );
};

export default UserProfile
