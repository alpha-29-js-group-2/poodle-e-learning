import React from 'react';
import { Box, FormControl, InputLabel, Select, MenuItem } from '@material-ui/core';
import { Pagination } from '@material-ui/lab';
import { PAGE_SIZE } from '../../common/constants';

const PaginationView = ({ pageSize, handleChangePageSize, totalResults, currentPage, handleChange }) => {
    return (
        <>
            <FormControl>
                <InputLabel>
                    Show
                </InputLabel>
                <Select
                    id="simple-select-label"
                    value={pageSize}
                    onChange={handleChangePageSize}
                    displayEmpty
                >
                    <MenuItem value={PAGE_SIZE.SIX}>
                        <em>{PAGE_SIZE.SIX}</em>
                    </MenuItem>
                    <MenuItem value={PAGE_SIZE.TWELVE}>{PAGE_SIZE.TWELVE}</MenuItem>
                    <MenuItem value={PAGE_SIZE.TWENTY_FOUR}>{PAGE_SIZE.TWENTY_FOUR}</MenuItem>
                </Select>
            </FormControl>
            <Box p={3}>
                <Pagination count={Math.ceil(totalResults / pageSize)} page={currentPage} onChange={handleChange} variant="outlined" style={{ justifyContent: "center", display: 'flex' }} />
            </Box>
        </>
    )
};
export default PaginationView;
