import { makeStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import { useContext } from "react";
import { StudentAccessContext } from "../../../providers/EnrollStudentsContext";


const useStyles = makeStyles({
    root: {
        display: "flex",
        flexFlow: "row",
        justifyContent: "space-between",
        padding: "5px",
        minWidth: "300px",
        cursor: "default",
    },
    studentNames: {
        marginRight: "10px",
    },
    granted: {
        border: '1px green',
    },
    revoked: {
        border: '1px gray'
    },
});

export const AccessRow = ({ student, hasAccess }) => {
    const {
        studentsWithAccessToBeRevoked,
        setStudentsWithAccessToBeRevoked,
        studentsWithAccessToBeGranted,
        setStudentsWithAccessToBeGranted
    } = useContext(StudentAccessContext);
    const classes = useStyles();

    const handleToggle = () => {
        if (hasAccess) {
            setStudentsWithAccessToBeRevoked([...studentsWithAccessToBeRevoked, student.id]);
        } else{
            setStudentsWithAccessToBeGranted([...studentsWithAccessToBeGranted, student.id]);
        }
    };

    const handleRevert = () => {
        if (hasAccess) {
            setStudentsWithAccessToBeRevoked((current) => current.filter((id) => id !== student.id));
        } else{
            setStudentsWithAccessToBeGranted((current) => current.filter((id) => id !== student.id));
        }
    };

    

    //const added = studentsWithAccess.includes(student.id);

    let toggled;
    if ((hasAccess && studentsWithAccessToBeRevoked.includes(student.id)) || 
    (!hasAccess && studentsWithAccessToBeGranted.includes(student.id)) ) {
        toggled = true;
    } else {
        toggled = false;
    }

    return (
        <div>
            {student ? (
                <div className={classes.root}>
                    <p className={classes.studentNames}>
                        <b>{student.name} </b>- {student.email}
                    </p>
                        <Button
                            onClick={toggled ? handleRevert : handleToggle}
                            variant={toggled ? "text" : "contained"}
                            color={hasAccess ? "secondary" : "primary"}
                        >
                            {hasAccess ? (toggled ? "Cancel Revoking" : "Revoke Access" ) : (toggled ? "Cancel Granting" : "Grant Access" ) }
                        </Button>
                </div>
            ) : null}
        </div>
    );
};
