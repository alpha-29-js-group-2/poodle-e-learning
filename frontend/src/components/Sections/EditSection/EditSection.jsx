import React, { useState, useEffect, createRef } from 'react';
import { FormControl, FormControlLabel, Checkbox, Box, Typography, Button, TextField, DialogActions, IconButton } from '@material-ui/core';
import { MAX_TITLE_LENGTH, MIN_TITLE_LENGTH } from '../../../common/constants';
import { Editor, EditorTools, EditorUtils, ProseMirror } from "@progress/kendo-react-editor";
import DateAndTimePicker from '../../DateAndTimePicker/DateAndTimePicker';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import AddIcon from '@material-ui/icons/Add';
import { convertFromUTCTime } from '../../../common/helpers';
import { getStudentsWithAccess, updateSectionInCourse } from '../../../requests/sections';
import { useHistory } from 'react-router-dom';
import { AccessStudentsDialogRefresh, StudentAccessContext } from '../../../providers/EnrollStudentsContext';
import AccessDialog from './AccessDialog';
import { grantStudents, revokeStudents } from '../../../requests/users';


const EditSection = ({ location }) => {
    const { section } = location.state;
    const { courseId } = location.state;

    const [updatedSection, setUpdatedSection] = useState(section);

    const editorRef = createRef();
    const history = useHistory();

    const [titleError, setTitleError] = useState("");
    const [titleChecked, setTitleChecked] = useState(false);
    const [contentError, setContentError] = useState("");
    const [contentChecked, setContentChecked] = useState(false);
    const [isEmbeddedChanged, setIsEmbeddedChanged] = useState(false);
    const [restrictionDateChanged, setRestrictionDateChanged] = useState(false);
    const [restrictionDateVisible, setRestrictionDateVisible] = useState(false);
    const [studentsWithAccess, setStudentsWithAccess] = useState([]);
    const [studentsWithoutAccess, setStudentsWithoutAccess] = useState([]);
    const [studentsWithAccessToBeRevoked, setStudentsWithAccessToBeRevoked] = useState([]);
    const [studentsWithAccessToBeGranted, setStudentsWithAccessToBeGranted] = useState([]);
    const [refreshSection, setRefreshSection] = useState(false);
    const [AccessDialogBoxOpened, setAccessDialogBoxOpened] = useState(false);
    // const [enrolledStudents, setEnrolledStudents] = useState([]);

    const [AccessDialogInfo, setAccessDialogInfo] = useState({
        visible: false,
    });

    const onAccessDialogClose = () => {
        setAccessDialogInfo({
            visible: false,
        });
    };

    const showAccessDialog = () => {
        setAccessDialogInfo({
            visible: true,
        })
    }

    const restrictionUsersChanged = !!(studentsWithAccessToBeRevoked.length + studentsWithAccessToBeGranted.length);

    useEffect(() => {
        getStudentsWithAccess(section.id).then((result) => {
            return result.map((entry) => entry.id)
        }).then(ids => setStudentsWithAccess(ids));
    }, [section.id, refreshSection])

    const handleEditSection = (key, value) => {
        if (key === 'restrictionDate') {
            key = 'restriction_date';
            setRestrictionDateChanged(true);
        } if (key === 'isEmbedded') {
            setIsEmbeddedChanged(true);
        }
        updatedSection[key] = value;
        setUpdatedSection({ ...updatedSection });
    };

    const attachEditorValueToSection = () => {
        const value = EditorUtils.getHtml(editorRef.current.view.state);
        handleEditSection('content', value);
        return value;
    }

    const validateTitle = (title) => {
        if (title !== '' && title.length >= MIN_TITLE_LENGTH &&
            title.length <= MAX_TITLE_LENGTH) {
            setTitleChecked(true);
            return setTitleError("");
        }
        setTitleChecked(false);
        return setTitleError(`Title must be between ${MIN_TITLE_LENGTH} and ${MAX_TITLE_LENGTH} symbols`)
    };

    const validateContent = (content) => {
        if (content !== '' && content !== '<p></p>') {
            setContentChecked(true);
            return setContentError("");
        }
        setContentChecked(false);
        return setContentError(`Content must not be empty`)

    };
    const onBlur = (e) => {
        const content = EditorUtils.getHtml(e.state);
        validateContent(content);
        setContentChecked(true);
    };

    const showRestrictionDate = () => {
        setRestrictionDateVisible(true);
    };

    const removeRestrictionDate = () => {
        handleEditSection('restrictionDate', null);
        setRestrictionDateVisible(false);
    };

    const handleSaveSection = async () => {
        const content = attachEditorValueToSection(section);

        const updateSectionResult = await updateSectionInCourse(courseId, { ...updatedSection, content });
        if (!updateSectionResult.error) {
            if (studentsWithAccessToBeGranted.length) {
                const grantAccessResult =
                    await grantStudents(studentsWithAccessToBeGranted,
                        section.id);
                if (!grantAccessResult.error) {
                    setStudentsWithAccessToBeGranted([]);
                } else {
                    return alert(grantAccessResult.error);
                }
            }

            if (studentsWithAccessToBeRevoked.length) {
                const revokeAccessResult =
                    await revokeStudents(studentsWithAccessToBeRevoked,
                        section.id);
                if (!revokeAccessResult.error) {
                    setStudentsWithAccessToBeRevoked([]);
                    setAccessDialogBoxOpened(!AccessDialogBoxOpened);
                } else {
                    return alert(revokeAccessResult.error);
                }
            }

            history.push(`/courses/${courseId}`)
        } else {
            return alert(updateSectionResult.error);
        }
    };

    const handleCancel = () => {
        history.push(`/courses/${courseId}`)
    }


    const validateEdit = (!titleError && !contentError && [isEmbeddedChanged, restrictionDateChanged, contentChecked, restrictionUsersChanged].some(e => e)) || (!titleError && !contentError && [titleChecked, contentChecked].some(e => e));

    // Kendo UI Editor - onBlur, onClick

    const handleOnMount = (event) => {
        return new ProseMirror.EditorView(
            { mount: event.dom },
            {
                ...event.viewProps,

                handleDOMEvents: {
                    ...(event.viewProps.handleDOMEvents || {}),
                    blur: onBlur,
                },
            }
        );
    };


    return (
        <FormControl>
            <AccessStudentsDialogRefresh.Provider
                value={{
                    AccessDialogBoxOpened,
                    setAccessDialogBoxOpened,
                }}>
                <StudentAccessContext.Provider value={{
                    studentsWithAccess,
                    setStudentsWithAccess,
                    studentsWithoutAccess,
                    setStudentsWithoutAccess,
                    studentsWithAccessToBeRevoked,
                    setStudentsWithAccessToBeRevoked,
                    studentsWithAccessToBeGranted,
                    setStudentsWithAccessToBeGranted
                }}>
                    <AccessDialog
                        visible={AccessDialogInfo.visible}
                        onClose={onAccessDialogClose}
                        courseId={parseInt(location.state.courseId)}
                    />
                </StudentAccessContext.Provider>
            </AccessStudentsDialogRefresh.Provider>
            <h1>Edit section</h1>
            <Typography>
                Change the title or the content of the section, choose whether it is embedded or restricted.
            </Typography>
            <TextField
                autoFocus
                margin="dense"
                id="title"
                label="Title"
                style={{backgroundColor: "white"}}
                defaultValue={updatedSection.title}
                fullWidth
                required={true}
                variant="outlined"
                onChange={(e) => validateTitle(e.target.value)}
                onBlur={(e) => handleEditSection('title', e.target.value)}
                error={titleError !== ""}
                helperText={titleError ? titleError : ""}
            />
            <Editor
                tools={[
                    [Bold, Italic, Underline],
                    [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                    [Indent, Outdent],
                    [OrderedList, UnorderedList],
                    FontSize,
                    FontName,
                    FormatBlock,
                    [Undo, Redo],
                    [Link, Unlink, InsertImage, ViewHtml],
                    [InsertTable],
                    [AddRowBefore, AddRowAfter, AddColumnBefore, AddColumnAfter],
                    [DeleteRow, DeleteColumn, DeleteTable],
                    [MergeCells, SplitCell],
                ]}
                contentStyle={{
                    height: 250,
                }}
                defaultContent={updatedSection.content}
                ref={editorRef}
                onMount={handleOnMount}

            />
            <FormControlLabel
                control={<Checkbox checked={!!updatedSection.isEmbedded} onChange={(e) => handleEditSection('isEmbedded', !updatedSection.isEmbedded)} color="primary" />
                }
                label="Embedded view"
            />
            <Box style={{ height: "80px" }}>
                {!restrictionDateVisible
                    ? <>Restriction date: {updatedSection.restriction_date ? convertFromUTCTime(updatedSection.restriction_date) : 'none'} <IconButton aria-label="edit" color="primary" onClick={showRestrictionDate}>< EditIcon /></IconButton>
                    </>
                    : <>Restriction date: {updatedSection.restriction_date ? convertFromUTCTime(updatedSection.restriction_date) : 'none'} <IconButton aria-label="edit" color="primary" onClick={removeRestrictionDate}>< DeleteIcon /></IconButton>
                    </>}
                {restrictionDateVisible ? <><br /><DateAndTimePicker handleCreateSection={handleEditSection} /> </> : <></>}
            </Box>
            <Box>
                {!studentsWithAccess.length
                    ? (<>
                        Grant access to specific students
                        <IconButton onClick={showAccessDialog} color="primary" aria-label="edit">
                            < AddIcon />
                        </IconButton>
                    </>)
                    : (<>
                        Edit students with access <IconButton color="primary" aria-label="edit" onClick={showAccessDialog} >
                            < EditIcon />
                        </IconButton>
                    </>)}
            </Box>
            <DialogActions>
                <Button onClick={handleCancel} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleSaveSection} variant="contained" color="primary" disabled={validateEdit ? false : true} > Save</Button>

            </DialogActions>
        </FormControl>
    );
}

const {
    Bold,
    Italic,
    Underline,
    AlignLeft,
    AlignCenter,
    AlignRight,
    AlignJustify,
    Indent,
    Outdent,
    OrderedList,
    UnorderedList,
    Undo,
    Redo,
    FontSize,
    FontName,
    FormatBlock,
    Link,
    Unlink,
    InsertImage,
    ViewHtml,
    InsertTable,
    AddRowBefore,
    AddRowAfter,
    AddColumnBefore,
    AddColumnAfter,
    DeleteRow,
    DeleteColumn,
    DeleteTable,
    MergeCells,
    SplitCell,
} = EditorTools;

export default EditSection;

