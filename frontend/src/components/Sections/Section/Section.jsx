import React, { useEffect, useState, useRef, useContext } from 'react';
import { deleteSection, getSection } from '../../../requests/sections.js';
import { Typography, CircularProgress, Box, Container, Breadcrumbs, Link, IconButton } from '@material-ui/core';
import AuthContext from '../../../providers/AuthContext';
import { userRole } from '../../../common/user-role';
import { convertFromUTCTime } from '../../../common/helpers';
import { makeStyles } from '@material-ui/core/styles';
import { useHistory } from 'react-router-dom';
import DeleteIcon from '@material-ui/icons/Delete';
import EditIcon from '@material-ui/icons/Edit';
import { getCourseAuthor } from '../../../requests/users.js';
import AlternateActionDialog from '../../Dialog/AlternativeActionDialog.jsx';


const useStyles = makeStyles({
    root: {
        paddingTop: '10px',
    },
    icon: {
        paddingBottom: "7px"
    },
    pointerCursor: {
        cursor: 'pointer',
    },
    pointerDefault: {
        cursor: 'default',
    },
});

const enableIframes = (content) => {
    content = content.replace("&lt;iframe", "<iframe");
    content = content.replace("&gt;&lt;/iframe&gt;", "></iframe>");
    return content;
}

const Section = ({ match }) => {
    const sectionId = match.params.id;
    const courseId = match.url.split("/")[2]
    const [section, setSection] = useState({});
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);
    const contentEl = useRef(null);
    const { user } = useContext(AuthContext);
    const history = useHistory();
    const [courseAuthorId, setCourseAuthorId] = useState(null);
    const classes = useStyles();

    useEffect(() => {
        getSection(sectionId)
            .then(result => {
                if (result.error) {
                    setLoading(false);
                    throw new Error(result.error);
                }
                setLoading(false);
                setSection(result);

                contentEl.current.innerHTML = enableIframes(result.content);
            }).catch(e => setError(e.message));

            getCourseAuthor(courseId)
            .then(result => {
                setCourseAuthorId(result.id);
            })

    }, [sectionId, courseId])

    const handleClick = () => {
        history.goBack();
    }

    const handleEdit = () => {
        history.push({
            pathname: `/sections/${section.id}/edit`,
            state: {
                section,
                courseId
            }
        })
    }

    const handleDelete = async () => {
        const result = await deleteSection(section.id);
        if (!result.error) {
            history.goBack();
        }
    }
    
    const [deleteDialogInfo, setDeleteDialogInfo] = useState({
        visible: false,
    });


    const openDeleteDialog = () => {
        setDeleteDialogInfo({
            visible: true,
            content: 'Are you sure you want to delete this section?',
        });
    }

    const onDeleteDialogClose = () => {
        setDeleteDialogInfo({
            visible: false,
        });
    }

    const deleteCourseAction = {
        label: "Delete",
        action: () => handleDelete(),
    }

    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }

    return (
        
        <Container maxWidth="lg" style={{ textAlign: 'left', paddingTop: '20px' }}>
            <AlternateActionDialog
                                    content={deleteDialogInfo.content}
                                    visible={deleteDialogInfo.visible}
                                    onClose={onDeleteDialogClose}
                                    alternativeAction={deleteCourseAction}
                                />
            <Breadcrumbs aria-label="breadcrumb">
                <Link className={classes.pointerCursor} color="inherit" onClick={handleClick}>{decodeURIComponent(match.params.title)}</Link>       
                <Typography className={classes.pointerDefault} color="textPrimary">{section.title}</Typography>
            </Breadcrumbs>
            <br />
            <Typography className={classes.root} style={{display: "inline"}} variant="h4" color="textPrimary">{section.title}</Typography>
            {'  '}
            {!courseAuthorId || user.id !== courseAuthorId ? null :
            <><IconButton aria-label="edit" color="primary" onClick={handleEdit}>< EditIcon className={classes.icon} /></IconButton>
            <IconButton aria-label="edit" color="primary" onClick={openDeleteDialog}>< DeleteIcon className={classes.icon}/></IconButton></>
        }
            <Typography className={classes.root} variant="subtitle1" ref={contentEl}></Typography>
            {user.role === userRole.TEACHER && !!section.restriction_date && (
                <Typography className={classes.root} variant="subtitle2" color="textPrimary">Restriction date: {section.restriction_date ? convertFromUTCTime(section.restriction_date) : 'none'}</Typography>
            )}
        </Container>
    )
}

export default Section;
