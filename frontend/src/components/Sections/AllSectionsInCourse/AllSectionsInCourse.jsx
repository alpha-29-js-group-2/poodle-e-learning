import React, { useState, useEffect, useCallback } from 'react';
import { getAllSectionsInCourses, rearrangeSections } from '../../../requests/sections';
import { Box, Button, CircularProgress } from '@material-ui/core';
import update from 'immutability-helper';
import SectionCard from '../SectionCard/SectionCard';
import { DndProvider } from 'react-dnd'
import { HTML5Backend } from 'react-dnd-html5-backend'


const AllSectionsInCourse = ({ courseId, courseTitle, refreshedSections, courseCreator, courseRefresh, rearrange }) => {
    const [sections, setSections] = useState([]);
    const [loading, setLoading] = useState(true);
    const [error, setError] = useState(null);

    useEffect(() => {

        getAllSectionsInCourses(courseId)
            .then(result => {
                if (result.error) {
                    setLoading(false);
                    throw new Error(result.error);
                }
                if (result.length < 0) {
                    setLoading(false);
                }
                setLoading(false);
                setSections(result);
            }).catch(e => setError(e.message))
    }, [courseId, refreshedSections]);

    const moveSection = useCallback((dragIndex, hoverIndex) => {
            const dragCard = sections[dragIndex];
            const newOrder = update(sections, {
                $splice: [
                    [dragIndex, 1],
                    [hoverIndex, 0, dragCard],
                ],
            })
            setSections(newOrder);
            (async () => {
                const order = newOrder.map(e => e.id);
                const result = await rearrangeSections(courseId, order);
                if (result.error) {
                    alert(result.error);
                }
            })();
    }, [sections, courseId]);
    const renderSection = (section, index) => {
        return (
            <SectionCard
                key={section.id}
                index={index}
                id={section.id}
                moveSection={moveSection}
                section={section}
                courseCreator={courseCreator}
                courseId={courseId}
                courseTitle={courseTitle}
                courseRefresh={courseRefresh}
                rearrange={rearrange}
            />);
    };

    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }

    if (!loading && sections.length === 0) {
        return <Box pt={3}><p>The course has no resources yet</p></Box>;
    }

    return (
        <DndProvider backend={HTML5Backend}>
            <Box pb={5}>
                {sections.map((section, i) => renderSection(section, i))}
            </Box >
        </DndProvider>

    )
};

export default AllSectionsInCourse;
