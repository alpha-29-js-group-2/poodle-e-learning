import React, { useState, createRef } from 'react';
import { Button, Box, FormControlLabel, Checkbox, IconButton, TextField, DialogActions, DialogContent, DialogContentText, DialogTitle, } from '@material-ui/core';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import { MAX_TITLE_LENGTH, MIN_TITLE_LENGTH } from '../../../common/constants';
import { createSectionInCourse } from '../../../requests/sections';
import DateAndTimePicker from '../../DateAndTimePicker/DateAndTimePicker';
import { useHistory } from 'react-router-dom';
import { Editor, EditorTools, EditorUtils } from "@progress/kendo-react-editor";
import { GrantedAccessStudentsDialogRefresh, GrantedStudentContext } from '../../../providers/EnrollStudentsContext';
import GrantAccessDialog from './GrantAccessDialog';
import { grantStudents } from '../../../requests/users';

const CreateSection = ({ location }) => {
    const [section, setSection] = useState({
        title: '',
        content: '',
        restrictionDate: null,
        isEmbedded: false,
    });

    const editorRef = createRef();
    const [titleChecked, setTitleChecked] = useState(false);
    const [titleError, setTitleError] = useState('');
    const [restrictionDateVisible, setRestrictionDateVisible] = useState(false);
    const [checked, setChecked] = useState(false);
    const history = useHistory();
    const [studentsWithAccess, setStudentsWithAccess] = useState([]);
    const [studentsWithoutAccess, setStudentsWithoutAccess] = useState([]);
    const [GADialogBoxOpened, setGADialogBoxOpened] = useState(false);
    const [GADialogInfo, setGADialogInfo] = useState({
        visible: false,
    });

    const handleCreateSection = (key, value) => {
        if (key === "isEmbedded") {
            setChecked(value);
        }
        section[key] = value;
        setSection({ ...section });
    }

    const onGADialogClose = () => {
        setGADialogInfo({
            visible: false,
        });
    };

    const showGADialog = () => {
        setGADialogInfo({
            visible: true,
        });
    }

    const revokeAll = () => {
        setStudentsWithAccess([]);
    }

    const attachEditorValueToSection = () => {
        const value = EditorUtils.getHtml(editorRef.current.view.state);
        section.content = value;
        return section;
    }

    const handleSaveSection = async () => {
        const updatedSection = attachEditorValueToSection(section)
        const sectionNew = await createSectionInCourse(location.state.courseId, updatedSection);
        if (!sectionNew.error) {
            if (studentsWithAccess.length) {
                const result = await grantStudents(studentsWithAccess, sectionNew.id);
                if (!result.error) {
                    setStudentsWithAccess([]);
                    setStudentsWithoutAccess([]);
                    setGADialogBoxOpened(!GADialogBoxOpened);
                }
                else {
                    alert(result.error);
                }
            }
            
            history.goBack()
        } else {
            alert(sectionNew.error);
        }
        
    }

    const showRestrictionDate = () => {
        setRestrictionDateVisible(true);
    };

    const removeRestrictionDate = () => {
        handleCreateSection('restrictionDate', null);
        setRestrictionDateVisible(false);
    }

    const validateTitle = (title) => {
        if (title !== '' && title.length >= MIN_TITLE_LENGTH &&
            title.length <= MAX_TITLE_LENGTH) {
            setTitleChecked(true);
            return setTitleError("");
        }
        setTitleChecked(false);
        return setTitleError(`Title must be between ${MIN_TITLE_LENGTH} and ${MAX_TITLE_LENGTH} symbols`)
    };

    return (
        <div>
            <GrantedAccessStudentsDialogRefresh.Provider
                value={{
                    GADialogBoxOpened,
                    setGADialogBoxOpened,
                }}>
                <GrantedStudentContext.Provider value={{
                    studentsWithAccess,
                    setStudentsWithAccess,
                    studentsWithoutAccess,
                    setStudentsWithoutAccess
                }}>
                    <GrantAccessDialog
                        visible={GADialogInfo.visible}
                        onClose={onGADialogClose}
                        courseId={location.state.courseId}
                    />
                </GrantedStudentContext.Provider>
            </GrantedAccessStudentsDialogRefresh.Provider>
            <h1>Add section</h1>
            <DialogContent>
                <DialogContentText>
                    Create a section for your course and add content as HTML.
                </DialogContentText>
                <TextField
                    autoFocus
                    margin="dense"
                    id="title"
                    label="Title"
                    fullWidth
                    variant="outlined"
                    style={{backgroundColor: "white"}}
                    onChange={(e) => validateTitle(e.target.value)}
                    onBlur={(e) => handleCreateSection('title', e.target.value)}
                    error={titleError !== ""}
                    helperText={titleError ? titleError : ""}
                />
                <Editor
                    tools={[
                        [Bold, Italic, Underline],
                        [AlignLeft, AlignCenter, AlignRight, AlignJustify],
                        [Indent, Outdent],
                        [OrderedList, UnorderedList],
                        FontSize,
                        FontName,
                        FormatBlock,
                        [Undo, Redo],
                        [Link, Unlink, InsertImage, ViewHtml],
                        [InsertTable],
                        [AddRowBefore, AddRowAfter, AddColumnBefore, AddColumnAfter],
                        [DeleteRow, DeleteColumn, DeleteTable],
                        [MergeCells, SplitCell],
                    ]}
                    contentStyle={{
                        height: 250,
                    }}
                    ref={editorRef}
                />
                <FormControlLabel
                    control={<Checkbox checked={checked} onChange={(e) => handleCreateSection('isEmbedded', !checked)} color="primary" />
                    }
                    label="Embedded view"
                />
                <Box style={{ height: "80px" }}>
                    {!restrictionDateVisible
                        ? <>Restriction date
                            <IconButton aria-label="edit" onClick={showRestrictionDate} color="primary"> 
                                < AddIcon />
                            </IconButton>
                        </>
                        :
                        <>Restriction date
                            <IconButton aria-label="edit" onClick={removeRestrictionDate} color="primary">
                                < DeleteIcon />
                            </IconButton>
                        </>}
                    {restrictionDateVisible &&
                        <><br />
                            <DateAndTimePicker handleCreateSection={handleCreateSection} />
                        </>}
                </Box>
                <Box>
                    {!studentsWithAccess.length ?
                        <Button onClick={showGADialog}>
                            Grant access to specific students
                            <IconButton aria-label="edit" color="primary">
                                < AddIcon />
                            </IconButton>
                        </Button>
                        :
                        <Button onClick={revokeAll}>
                            Revoke access <IconButton aria-label="edit" color="primary">
                                < DeleteIcon />
                            </IconButton>
                        </Button>}
                </Box>
            </DialogContent>
            <DialogActions>
                <Button onClick={history.goBack} color="primary">
                    Cancel
                </Button>
                <Button onClick={handleSaveSection} variant="contained" color="primary" disabled={titleChecked ? false : true}> Save</Button>
            </DialogActions>
        </div >
    )
}


const {
    Bold,
    Italic,
    Underline,
    AlignLeft,
    AlignCenter,
    AlignRight,
    AlignJustify,
    Indent,
    Outdent,
    OrderedList,
    UnorderedList,
    Undo,
    Redo,
    FontSize,
    FontName,
    FormatBlock,
    Link,
    Unlink,
    InsertImage,
    ViewHtml,
    InsertTable,
    AddRowBefore,
    AddRowAfter,
    AddColumnBefore,
    AddColumnAfter,
    DeleteRow,
    DeleteColumn,
    DeleteTable,
    MergeCells,
    SplitCell,
} = EditorTools;

export default CreateSection;
