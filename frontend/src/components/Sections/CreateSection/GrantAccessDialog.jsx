import React, { useState, useEffect, useContext } from 'react';
import PropTypes from 'prop-types';
import { getEnrolledStudents } from '../../../requests/users';
import { withStyles } from '@material-ui/core/styles';
import { GrantedAccessStudentsDialogRefresh, GrantedStudentContext } from '../../../providers/EnrollStudentsContext';
import Button from '@material-ui/core/Button';
import Dialog from '@material-ui/core/Dialog';
import Box from '@material-ui/core/Box';
import TextField from '@material-ui/core/TextField';
import CircularProgress from '@material-ui/core/CircularProgress';
import MuiDialogContent from '@material-ui/core/DialogContent';
import MuiDialogActions from '@material-ui/core/DialogActions';
import { StudentAccessRow } from './StudentAccessRow';


const DialogContent = withStyles(() => ({
    root: {
        margin: '20px',
        padding: 0,
        border: '1px solid gray',
        paddingTop: 0,
        '&:first-child': {
            paddingTop: 0,
        }
    },
}))(MuiDialogContent);

const DialogSearch = withStyles(() => ({
    root: {
        marginTop: '10px',
        marginBottom: '20px',
        width: '100%'
    },
}))(TextField);

const DialogActions = withStyles((theme) => ({
    root: {
        margin: 0,
        padding: theme.spacing(1),
    },
}))(MuiDialogActions);

const GrantAccessDialog = ({ visible, onClose, courseId, }) => {
    const [loading, setLoading] = useState(true);
    const [displayedStudents, setDisplayedStudents] = useState([]);
    const { studentsWithAccess,
        setStudentsWithAccess,
        setStudentsWithoutAccess
    } = useContext(GrantedStudentContext);
    const [error, setError] = useState(null);
    const [searchTerm, setSearchTerm] = useState('');

    const { GADialogBoxOpened, setGADialogBoxOpened } = useContext(GrantedAccessStudentsDialogRefresh);

    useEffect(() => {
        setLoading(true);
        (async () => {
            const data = await getEnrolledStudents(courseId, searchTerm);
            if (data && !data.error) {
                setLoading(false);
                setDisplayedStudents(data);
            } else {
                setError(data.error);
            }
        })()
        setLoading(false);
    }, [courseId, GADialogBoxOpened, searchTerm]);

    const handleSave = async () => {
        onClose();

        //This is to make sure the window closes before the search
        //Term is set to '', making the window grow back up, making
        //up for the time needed for the close animation. 
        //Set the timer to 10 to see what I mean.
        setTimeout(() => {
            setGADialogBoxOpened(!GADialogBoxOpened);
            setSearchTerm('');
        }, 200);
    }

    const handleCancel = () => {
        onClose();

        //This is to make sure the window closes before the search
        //Term is set to '', making the window grow back up, making
        //up for the time needed for the close animation. 
        //Set the timer to 10 to see what I mean.
        setTimeout(() => {
            setStudentsWithAccess([]);
            setStudentsWithoutAccess([]);
            setGADialogBoxOpened(!GADialogBoxOpened);
            setSearchTerm('');
        }, 200);
    }

    if (loading) {
        return <Box p={5}><CircularProgress /></Box>;
    }

    // show error msg
    if (error) {
        return <Box p={5}><p>{error}</p></Box>;
    }



    return (
        <div>
            <Dialog onClose={onClose} open={visible}>
                <DialogContent dividers >
                    <DialogSearch
                        autoFocus
                        label="Search Terms"
                        id="search-students"
                        placeholder="Search Students"
                        onChange={(e) => setSearchTerm(e.target.value)}
                    />
                    {displayedStudents?.length ? displayedStudents.map((student) => (
                        <StudentAccessRow
                            key={student.id}
                            student={student}
                            courseId={courseId}
                            studentGranted={studentsWithAccess.includes(student.id)}
                        />
                    )) :
                        <p style={{ padding: '10px' }}>
                            No students found with these search terms.
                        </p>}
                </DialogContent>
                <DialogActions>
                    <Button disabled={!studentsWithAccess.length} onClick={handleSave} color="primary">
                        Save
                    </Button>
                    <Button onClick={handleCancel}>
                        Cancel
                    </Button>
                </DialogActions>
            </Dialog>
        </div>
    );
}

GrantAccessDialog.propTypes = {
    visible: PropTypes.bool.isRequired,
    onClose: PropTypes.func.isRequired,
    courseId: PropTypes.string.isRequired,
}

export default GrantAccessDialog;