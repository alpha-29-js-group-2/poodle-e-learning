import { makeStyles } from "@material-ui/styles";
import Button from "@material-ui/core/Button";
import { useContext } from "react";
import { GrantedStudentContext } from "../../../providers/EnrollStudentsContext";


const useStyles = makeStyles({
    root: {
        display: "flex",
        flexFlow: "row",
        justifyContent: "space-between",
        padding: "5px",
        minWidth: "300px",
        cursor: "default",
    },
    studentNames: {
        marginRight: "10px",
    },
    granted: {
        border: '1px green',
    },
    revoked: {
        border: '1px gray'
    },
});

export const StudentAccessRow = ({ student }) => {
    const { studentsWithAccess,
        setStudentsWithAccess,
        studentsWithoutAccess,
        setStudentsWithoutAccess } =
        useContext(GrantedStudentContext);
    const classes = useStyles();

    const handleAdd = () => {
        setStudentsWithAccess([...studentsWithAccess, student.id]);
        setStudentsWithoutAccess((current) => current.filter((id) => id !== student.id));
    };

    const handleRemove = () => {
        setStudentsWithoutAccess([...studentsWithoutAccess, student.id]);
        setStudentsWithAccess((current) => current.filter((id) => id !== student.id));
    };

    const added = studentsWithAccess.includes(student.id);
    console.log('student',student);
    console.log('added', added) 

    return (
        <div>
            {student ? (
                <div className={classes.root}>
                    <p className={classes.studentNames}>
                        <b>{student.name} </b>- {student.email}
                    </p>
                        <Button
                            onClick={added ? handleRemove : handleAdd}
                            variant={added ? "text" : "outlined"}
                            color={added ? "secondary" : "primary"}
                        >
                            {added ? "Revoke" : "Grant"} access
                        </Button>
                </div>
            ) : null}
        </div>
    );
};
