import React, { useCallback, useContext, useState, useRef } from 'react';
import { useDrag, useDrop } from 'react-dnd';
import { ItemTypes } from './ItemTypes.js';
import { Typography, Box, IconButton, makeStyles, Grid, Divider } from '@material-ui/core';
import { Link } from 'react-router-dom';
import { convertFromUTCTime } from '../../../common/helpers';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import DragHandleIcon from '@material-ui/icons/DragHandle';
import AuthContext from '../../../providers/AuthContext';
import { useHistory } from 'react-router-dom';
import { userRole } from '../../../common/user-role';
import AlternateActionDialog from '../../Dialog/AlternativeActionDialog';
import { deleteSection } from '../../../requests/sections';
import { toPrettyUrlString } from '../../../common/helpers';

const useStyles = makeStyles(() => ({
    normal: {
        position: 'relative',
        display: 'flex',
        justifyContent: 'left',
        flexFlow: 'column',
    },
    embedded: {
        position: 'relative',
        justifyContent: 'left',
    },
    dragStyle: {
        border: '1px dashed gray',
        padding: '0.5rem 1rem',
        marginBottom: '.5rem',
        backgroundColor: 'white',
    },
    isDragging: {
        opacity: 0.4,
    },
    handleVisible: {
        cursor: 'move',
        position: 'absolute',
        right: 0,
    },
    handleInvisible: {
        display: 'none',
    },
    iconButton: {
        padding: '10px',
    },
    editDeleteGrid: {
        paddingTop: '8px',
        display: 'flex',
        justifyContent: 'flex-end'
    }
}));


const SectionCard = ({ section, courseCreator, courseId, courseTitle, courseRefresh, id, index, moveSection, rearrange }) => {
    const classes = useStyles();
    const { refreshedCourse, setRefreshedCourse } = courseRefresh;
    const history = useHistory();
    const handleSectionEdit = () => {
        history.push({
            pathname: `/sections/${section.id}/edit`,
            state: {
                section,
                courseId
            }
        })
    }

    // const handleOpenSection = () => {
    //                                 // to={`/courses/${courseId}/sections/${section.id}`}

    //     history.push({
    //         pathname: `/courses/${courseId}/sections/${section.id}`,
    //         state: {
    //         section,
    //         courseId
    //         }
    //     })
    // }

    const contentEl = useCallback((node) => {
        if (section.isEmbedded && node) {
            node.innerHTML = section.content;
        }
    }, [section]);

    const [deleteDialogInfo, setDeleteDialogInfo] = useState({
        visible: false,
    });


    const openDeleteDialog = () => {
        setDeleteDialogInfo({
            visible: true,
            content: 'Are you sure you want to delete this section?',
        });
    }

    const onDeleteDialogClose = () => {
        setDeleteDialogInfo({
            visible: false,
        });
    }

    const handleDeleteSection = async (sectionId) => {
        const result = await deleteSection(sectionId);
        if (!result.error) {
            setDeleteDialogInfo({ visible: false })
            setRefreshedCourse(!refreshedCourse);
        }
    }

    const alternativeAction = {
        label: "Delete",
        action: () => handleDeleteSection(section.id),
    }

    const { user } = useContext(AuthContext);

    /* if (!!section.restriction_date) {
        return (
            <>
                <Box style={{ justifyContent: 'left' }} pt={2} pb={4}>
                    <Typography variant="h6" color={user.role === userRole.TEACHER ? "textPrimary" : "textSecondary"} >{section.title}</Typography>
                    <EditAndDelete/>
                    <Typography color="textSecondary" style={{ display: 'block' }}>Section will become available on {convertFromUTCTime(section.restriction_date)}</Typography>
                </Box>
                <hr />
            </>
        )
    }
    console.log('section', section);
    if (!!section.restrictedToUser) {
        return (
            <>
                <Box style={{ justifyContent: 'left' }} pt={2} pb={4}>
                    <Typography variant="h6" color="textSecondary">{section.title}</Typography>
                    <Typography color="textSecondary" style={{ display: 'block' }}>Section is restricted.</Typography>
                </Box>
                <hr />
            </>
        )
    } */

    //Start dnd section

    const ref = useRef(null);
    const [{ handlerId }, drop] = useDrop({
        accept: ItemTypes.SECTION,
        collect(monitor) {
            return {
                handlerId: monitor.getHandlerId(),
            };
        },
        hover(item, monitor) {
            if (!ref.current) {
                return;
            }
            const dragIndex = item.index;
            const hoverIndex = index;
            // Don't replace items with themselves
            if (dragIndex === hoverIndex) {
                return;
            }
            // Determine rectangle on screen
            const hoverBoundingRect = ref.current?.getBoundingClientRect();
            // Get vertical middle
            const hoverMiddleY = (hoverBoundingRect.bottom - hoverBoundingRect.top) / 2;
            // Determine mouse position
            const clientOffset = monitor.getClientOffset();
            // Get pixels to the top
            const hoverClientY = clientOffset.y - hoverBoundingRect.top;
            // Only perform the move when the mouse has crossed half of the items height
            // When dragging downwards, only move when the cursor is below 50%
            // When dragging upwards, only move when the cursor is above 50%
            // Dragging downwards
            if (dragIndex < hoverIndex && hoverClientY < hoverMiddleY) {
                return;
            }
            // Dragging upwards
            if (dragIndex > hoverIndex && hoverClientY > hoverMiddleY) {
                return;
            }
            // Time to actually perform the action
            moveSection(dragIndex, hoverIndex);
            // Note: we're mutating the monitor item here!
            // Generally it's better to avoid mutations,
            // but it's good here for the sake of performance
            // to avoid expensive index searches.
            item.index = hoverIndex;
        },
    });
    const [{ isDragging }, drag, preview] = useDrag({
        type: ItemTypes.SECTION,
        item: () => {
            return { id, index };
        },
        collect: (monitor) => ({
            isDragging: monitor.isDragging(),
        }),
    });
    drag(drop(ref));


    //End dnd section

    /*     const opacity = isDragging ? 0.4 : 1;
        let style;
        if (section.isEmbedded) {
            style = {
                justifyContent: 'left',
            }
        }
        else {
            style = {
                display: 'flex',
                justifyContent: 'left',
                flexFlow: 'column',
            } 
        }
    
        const dragStyle = {
            border: '1px dashed gray',
            padding: '0.5rem 1rem',
            marginBottom: '.5rem',
            backgroundColor: 'white',
            cursor: 'move',
        } */

    return (
        <>
            <Box
                className={`${rearrange ? classes.dragStyle : ``}
                ${section.isEmbedded ? classes.embedded : classes.normal}
                ${isDragging ? classes.isDragging : ``}`}
                pt={3}
                pb={3}
                ref={preview}
                data-handler-id={handlerId}>
                {/* <Grid style={{ display: 'flex' }}> */}
                <Grid container style={{ paddingTop: '6px', paddingBottom: '6px', alignItems: 'center' }}>

                    {/*If the section is embedded or if the section is restricted to the student,
                    the title is not a link  */}
                    {section.isEmbedded || (user?.role < userRole.TEACHER && ((!!section.restriction_date && section.restriction_date > (new Date()).toISOString()) || !!section.restrictedToUser)) ?
                        (<Grid item xs={10} style={{ paddingTop: '8px', paddingBottom: '8px'}}><Typography variant="h6"  color={!!section.restrictedToUser ||
                            (!!section.restriction_date && user?.role < userRole.TEACHER)
                            ? "textSecondary"
                            : "textPrimary"}
                        >
                            {section.title}
                        </Typography></Grid>)
                        :
                        (<Grid item xs={10} style={{ paddingTop: '8px', paddingBottom: '8px'}}><Link
                            to={`/courses/${courseId}/sections/${section.id}/${encodeURIComponent(courseTitle)}`}
                            style={{ textDecoration: 'none' }}
                        >
                            <Typography variant="h6" color="textPrimary" style={{ display: 'inline' }}>
                                {section.title}
                            </Typography>
                        </Link></Grid>)}
                    {user.id === courseCreator && (
                        <>
                            <AlternateActionDialog
                                content={deleteDialogInfo.content}
                                visible={deleteDialogInfo.visible}
                                onClose={onDeleteDialogClose}
                                alternativeAction={alternativeAction}
                            />

                            {!rearrange
                                && <Grid item xs={2} className={classes.editDeleteGrid}>  <IconButton aria-label="edit" color="primary" className={classes.iconButton} onClick={handleSectionEdit} >
                                    < EditIcon />
                                </IconButton>
                                    <IconButton aria-label="delete" color="primary" className={classes.iconButton} onClick={openDeleteDialog} >
                                        < DeleteIcon />
                                    </IconButton>
                                </Grid>
                            }
                            <IconButton aria-label="edit" color="primary"
                                className={rearrange ? classes.handleVisible : classes.handleInvisible}
                                ref={ref} >
                                < DragHandleIcon />
                            </IconButton>
                        </>
                    )}
                </Grid>


                {/* If the section is embedded, this displays the content on the page */}
                {section.isEmbedded &&
                    (user?.role === userRole.TEACHER ||
                        (!(!!section.restrictedToUser) && !(!!section.restriction_date)))
                    ? <Typography variant="h6" color="textPrimary" ref={contentEl} /> : null}

                {/* Restriction messages for students:
                    The first one shows if the section is showed only to some students
                    and the second if the section has a date restriction. */}
                {!!section.restrictedToUser ? (
                    <Typography color={user.role === userRole.TEACHER ? "textPrimary" : "textSecondary"} style={{ display: 'block' }}>
                        Section is restricted.
                    </Typography>
                ) : (!!section.restriction_date && section.restriction_date > (new Date()).toISOString() ? <Typography color={user.role === userRole.TEACHER ? "textPrimary" : "textSecondary"} style={{ display: 'block', paddingBottom: '15px' }}>
                    Section will become available on {convertFromUTCTime(section.restriction_date)}
                </Typography> : null)}
                {user.role === userRole.TEACHER && !!section.hasRestrictionToStudents ? (<Typography color="textPrimary" style={{ display: 'block', paddingBottom: '15px' }}>
                    Section is restricted to specific users.
                </Typography>) : null}
            </Box>
            <Divider />
        </>
    )
};

export default SectionCard;
