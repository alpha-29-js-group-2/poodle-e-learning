import React from 'react';
import CssBaseline from '@material-ui/core/CssBaseline';


const Container = ({ children }) => {
    return (
        <div>
            <CssBaseline />
            {children}
        </div>
    )
}

export default Container
