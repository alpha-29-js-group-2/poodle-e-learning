import React, { useState, useContext, useEffect } from "react";
import PropTypes from "prop-types";
import AppError from "../../providers/AppError";
import AuthContext, { extractUser } from "../../providers/AuthContext";
import { loginRequest, registerRequest } from "../../requests/auth.js";
import TextField from '@material-ui/core/TextField';
import { EMAIL_REGEX, MAX_EMAIL_LENGTH, MAX_FIRSTNAME_LENGTH, MAX_LASTNAME_LENGTH, MAX_PASSWORD_LENGTH, MIN_EMAIL_LENGTH, MIN_FIRSTNAME_LENGTH, MIN_LASTNAME_LENGTH, MIN_PASSWORD_LENGTH } from "../../common/constants";
import Button from '@material-ui/core/Button';
import { makeStyles } from '@material-ui/core/styles';
import { Container, Grid } from "@material-ui/core";
import Typography from "@material-ui/core/Typography";
import Dialog from '../Dialog/Dialog';
const useStyles = makeStyles(() => ({
  root: {
    display: 'flex',
    flexFlow: 'row',
    flexWrap: 'wrap',
    margin: '0 auto',
    justifyContent: 'center',
  },
  gridItem: {
    display: 'flex',
    '&:nth-child(2n)': {
      justifyContent: 'flex-start',
    },
    '&:nth-child(2n+1)': {
      justifyContent: 'flex-end',
    },
  },
  buttonsContainer: {
    display: 'flex',
    flexFlow: 'column',
    alignItems: 'center',
  },
  inputField: {
    minHeight: '70px',
    minWidth: '250px',
    margin: '20px 10px',
  },
  authButton: {
    margin: '0',
    width: '75px',
    marginBottom: '10px',
  },
  redirectButton: {
    textTransform: 'none',
  },
  link: {
    marginLeft: "5px",
    fontWeight: "bold",
  },
}));

const SignIn = ({ history, location }) => {
  const classes = useStyles();
  const login = location.pathname === "/login";

  const [user, setUser] = useState({
    email: "",
    password: "",
    firstName: "",
    lastName: "",
  });
  const [confirmPassword, setConfirmPassword] = useState("");

  const { isLoggedIn, setLoginState } = useContext(AuthContext);

  useEffect(() => {
    if (isLoggedIn) {
      let { from } = location.state || { from: { pathname: "/" } };
 
      history.replace(from);
    }
  }, [history, isLoggedIn,location.state]);

  const [dialogInfo, setDialogInfo] = useState({
    visible: false
  });

  const onDialogClose = () => {
    setDialogInfo({
      visible: false
    });
  };

  const [emailError, setEmailError] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [firstNameError, setFirstNameError] = useState('');
  const [lastNameError, setLastNameError] = useState('');
  const [confirmPasswordError, setConfirmPasswordError] = useState('');

  const [emailChecked, setEmailChecked] = useState(false);
  const [passwordChecked, setPasswordChecked] = useState(false);
  const [confirmPasswordChecked, setConfirmPasswordChecked] = useState(false);
  const [firstNameChecked, setFirstNameChecked] = useState(false);
  const [lastNameChecked, setLastNameChecked] = useState(false);

  const checkEmail = (email) => {
    if (email === '') {
      setEmailChecked(false);
      return setEmailError('Please, enter a valid email!');
    } else if (email.length < MIN_EMAIL_LENGTH ||
      EMAIL_REGEX.length > MAX_EMAIL_LENGTH) {
      setEmailChecked(false);
      return setEmailError(`Please, enter between \
${MIN_EMAIL_LENGTH} and ${MAX_EMAIL_LENGTH} characters!`);
    } else if (!email.match(EMAIL_REGEX)) {
      setEmailChecked(false);
      return setEmailError(`The email should be valid.`);
    } else {
      setEmailChecked(true);
      return setEmailError('');
    }
  }

  const validateEmail = (email) => {
    if (email && email.length >= MIN_EMAIL_LENGTH &&
      email.length <= MAX_EMAIL_LENGTH && email.match(EMAIL_REGEX)) {
      setEmailError('');
      setEmailChecked(true);
    }
  }

  const checkPassword = (password) => {
    if (password === '') {
      setPasswordChecked(false);
      return setPasswordError('Please, enter a password!');
    } else if (password.length < MIN_PASSWORD_LENGTH ||
      EMAIL_REGEX.length > MAX_PASSWORD_LENGTH) {
      setPasswordChecked(false);
      return setPasswordError(`Please, enter between \
${MIN_PASSWORD_LENGTH} and ${MAX_PASSWORD_LENGTH} characters!`);
    } else {
      setPasswordChecked(true);
      return setPasswordError('');
    }
  }

  const validatePassword = (password) => {
    if (password && password.length >= MIN_PASSWORD_LENGTH &&
      password.length <= MAX_PASSWORD_LENGTH) {
      setPasswordError('');
      setPasswordChecked(true);
    }
  }

  const checkConfirmPassword = (confirmPassword) => {
    if (confirmPassword !== user.password) {
      setConfirmPasswordChecked(false);
      return setConfirmPasswordError(`Passwords don't match.`);
    } else {
      setConfirmPasswordChecked(true);
      return setConfirmPasswordError('');
    }
  }

  const checkFirstName = (firstName) => {
    if (firstName === '') {
      setFirstNameChecked(false);
      return setFirstNameError('Please, enter your first name!');
    } else if (firstName.length < MIN_FIRSTNAME_LENGTH ||
      EMAIL_REGEX.length > MAX_FIRSTNAME_LENGTH) {
      setFirstNameChecked(false);
      return setFirstNameError(`Please, enter between \
${MIN_FIRSTNAME_LENGTH} and ${MAX_FIRSTNAME_LENGTH} characters!`);
    } else {
      setFirstNameChecked(true);
      return setFirstNameError('');
    }
  }

  const validateFirstName = (firstName) => {
    if (firstName && firstName.length >= MIN_FIRSTNAME_LENGTH &&
      firstName.length <= MAX_FIRSTNAME_LENGTH) {
      setFirstNameError('');
      setFirstNameChecked(true);
    }
  }

  const checkLastName = (lastName) => {
    if (lastName === '') {
      setLastNameChecked(false);
      return setLastNameError('Please, enter your last name!');
    } else if (lastName.length < MIN_LASTNAME_LENGTH ||
      EMAIL_REGEX.length > MAX_LASTNAME_LENGTH) {
      setLastNameChecked(false);
      return setLastNameError(`Please, enter between \
${MIN_LASTNAME_LENGTH} and ${MAX_LASTNAME_LENGTH} characters!`);
    } else {
      setLastNameChecked(true);
      return setLastNameError('');
    }
  }

  const validateLastName = (lastName) => {
    if (lastName && lastName.length >= MIN_LASTNAME_LENGTH &&
      lastName.length <= MAX_LASTNAME_LENGTH) {
      setLastNameError('');
      setLastNameChecked(true);
    }
  }

  const updateEmail = (email) => {
    validateEmail(email);
    return setUser({ ...user, email });
  }
  const updatePassword = (password) => {
    validatePassword(password);
    return setUser({ ...user, password });
  }
  const updateFirstName = (firstName) => {
    validateFirstName(firstName);
    return setUser({ ...user, firstName });
  }
  const updateLastName = (lastName) => {
    validateLastName(lastName);
    return setUser({ ...user, lastName });
  }
  const updateConfirmPassword = (confirmPassword) => {
    checkConfirmPassword(confirmPassword);
    return setConfirmPassword(confirmPassword);
  }

  const validateLogin = [emailChecked, passwordChecked].every(e => e);

  const validateRegister = [emailChecked, passwordChecked,
    confirmPasswordChecked, firstNameChecked, lastNameChecked].every(e => e);

  const signIn = async () => {
    if (!user.email) {
      setDialogInfo({
        content: "Please, enter email!",
        visible: true,

      });
      //alert(`Please enter email!`);
      return;
    }
    if (!user.password) {
      setDialogInfo({
        content: "Please enter password!",
        visible: true,

      });
      // alert(`Please enter password!`);
      return;
    }

    if (login) {
      const { token, error } = await loginRequest(user.email, user.password);
      if (token) {
        localStorage.setItem("token", token);
        setLoginState({
          isLoggedIn: !!extractUser(token),
          user: extractUser(token),
        });
      } else {
        setDialogInfo({
          content: error,
          visible: true,

        });
      }
    } else {
      if (user.password !== confirmPassword) {
        setDialogInfo({
          content: "Your passwords don't match!",
          visible: true,

        });
        // alert(`Your passwords don't match!`);
        return;
      }
      const result = await registerRequest(
        user.email,
        user.password,
        user.firstName,
        user.lastName
      );
      if (!result.error) {
        history.push("/login");
      } else {
        setDialogInfo({
          content: result.error,
          visible: true,

        });
      }

    }
  };

  const redirect = () => {
    if (login) {
      history.push('/register')
    } else {
      history.push('/login')
    }
  }

  return (
    <Container maxWidth="lg">
      <Dialog content={dialogInfo.content} visible={dialogInfo.visible} onClose={onDialogClose} />
      {login ? <h1>Login</h1> : <h1>Register</h1>}
      <form className={classes.root}>
        <Grid container>
          <Grid className={classes.gridItem} item xs={12} sm={6}>
            <TextField
              className={classes.inputField}
              autoFocus
              label="Email"
              id="email"
              required={true}
              value={user.email}
              placeholder="Email"
              onChange={(e) => updateEmail(e.target.value)}
              onBlur={(e) => checkEmail(e.target.value)}
              error={emailError !== ""}
              helperText={emailError ? emailError : ""} />
          </Grid>
          {login === false ? (
            <>
              <Grid className={classes.gridItem} item xs={12} sm={6}>
                <TextField
                  className={classes.inputField}
                  label="First Name"
                  id="first-name"
                  required={true}
                  value={user.firstName}
                  placeholder="John"
                  onChange={(e) => updateFirstName(e.target.value)}
                  onBlur={(e) => checkFirstName(e.target.value)}
                  error={firstNameError !== ""}
                  helperText={firstNameError ? firstNameError : ""} />
              </Grid>
              <Grid className={classes.gridItem} item xs={12} sm={6}>
                <TextField
                  className={classes.inputField}
                  label="Last Name"
                  id="last-name"
                  required={true}
                  value={user.lastName}
                  placeholder="Doe"
                  onChange={(e) => updateLastName(e.target.value)}
                  onBlur={(e) => checkLastName(e.target.value)}
                  error={lastNameError !== ""}
                  helperText={lastNameError ? lastNameError : ""} />
              </Grid>
            </>
          ) : null}
          <Grid className={classes.gridItem} item xs={12} sm={6}>
            <TextField
              className={classes.inputField}
              label="Password"
              id="password"
              type="password"
              required={true}
              value={user.password}
              autoComplete="current-password"
              onChange={(e) => updatePassword(e.target.value)}
              onBlur={(e) => checkPassword(e.target.value)}
              error={passwordError !== ""}
              helperText={passwordError ? passwordError : ""} />
          </Grid>

          {login === false ? (
            <Grid className={classes.gridItem} item xs={12} sm={6}>
              <TextField
                className={classes.inputField}
                label="Confirm Password"
                id="confirm-password"
                type="password"
                required={true}
                value={confirmPassword}
                onChange={(e) => updateConfirmPassword(e.target.value)}
                error={confirmPasswordError !== ""}
                helperText={confirmPasswordError ? confirmPasswordError : ""} />
            </Grid>
          ) : null}
          <Grid className={classes.buttonsContainer} item xs={12}>
            <Button
              disabled={login ? (validateLogin ? false : true) :
                (validateRegister ? false : true)}
              variant="contained" className={classes.authButton}
              color="primary" onClick={signIn}>
              {login ? "Login" : "Register"}
            </Button>
            <Button
              className={classes.redirectButton}
              variant="text"
              onClick={redirect}>
              {login ? (
                <>
                  <Typography>
                    You do not have an account yet?
                  </Typography>
                  <Typography color="primary" className={classes.link}>
                    Register
                  </Typography >
                </>
              ) : (
                <>
                  <Typography>
                    You already have an account?
                  </Typography>
                  <Typography color="primary" className={classes.link}>
                    Log in
                  </Typography >
                </>
              )}
            </Button>
          </Grid>

        </Grid>

      </form>

    </Container >
  );
};

SignIn.propTypes = {
  history: PropTypes.object.isRequired,
  location: PropTypes.object.isRequired,
};

export default SignIn;
