import React, { useState } from 'react';
import DateFnsUtils from '@date-io/date-fns';
import {
    DateTimePicker,
    MuiPickersUtilsProvider,
} from '@material-ui/pickers';

function DateAndTimePicker({ handleCreateSection }) {
    const [selectedDate, handleDateChange] = useState(new Date());
    const today = new Date();

    const getDateAndTime = (selectedDate) => {
        handleDateChange(selectedDate)

        const updatedDateAndTime = `${selectedDate.getFullYear()}-${('0' + (selectedDate.getMonth() + 1)).slice(-2)}-${('0' + selectedDate.getDate()).slice(-2)}${" "}${('0' + selectedDate.getHours()).slice(-2)}:${('0' + selectedDate.getMinutes()).slice(-2)}:${('0' + selectedDate.getSeconds()).slice(-2)}`;
        handleCreateSection('restrictionDate', updatedDateAndTime);
    }

    return (
        <MuiPickersUtilsProvider utils={DateFnsUtils} >
            <DateTimePicker ampm={false} value={selectedDate} onChange={getDateAndTime} minDate={today} />
        </MuiPickersUtilsProvider >
    );
}

export default DateAndTimePicker;
