// import logo from './logo.svg';
import "./App.css";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import React, { useState } from "react";
import NotFound from "./components/NotFound/NotFound";
import Home from "./components/Home/Home";
import Header from "./components/Header/Header";
import Footer from "./components/Footer/Footer";
import AuthContext, { extractUser, getToken } from "./providers/AuthContext";
import MyCourses from "./components/Courses/MyCourses/MyCourses";
import Course from "./components/Courses/Course/Course";
import CreateSection from './components/Sections/CreateSection/CreateSection';
import SignIn from "./components/Auth/SignIn";
import AllCourses from "./components/Courses/AllCourses/AllCourses";
import CreateCourse from "./components/Courses/CreateCourse/CreateCourse";
import Container from "@material-ui/core/Container";
import GuardedRoute from "./providers/GuardedRoute";
import Section from "./components/Sections/Section/Section";
import { makeStyles } from "@material-ui/styles";
import AllDeletedCourses from "./components/Courses/AllDeletedCourses/AllDeletedCourses";
import EditSection from "./components/Sections/EditSection/EditSection";
import UserProfile from "./components/UserProfile/UserProfile";
import { userRole } from "./common/user-role";
import ThemeProvider from "@material-ui/styles/ThemeProvider";
import { newTheme } from "./providers/theme";

const useStyles = makeStyles(() => ({
  root: {
    minHeight: '100vh',
    display: 'flex',
    flexDirection: 'column',
    padding: 0
  },
}));



function App() {
  const classes = useStyles();
  const [authValue, setAuthValue] = useState({
    isLoggedIn: !!extractUser(getToken()),
    user: extractUser(getToken()),
  });


  return (
    <ThemeProvider theme={newTheme}>
    <Container maxWidth={false} className={`App ${classes.root}`}>
      <BrowserRouter>
        <AuthContext.Provider
          value={{ ...authValue, setLoginState: setAuthValue }}
        > 
          <Header />
          <Container maxWidth="lg">
            <Switch>
              <Route exact path="/" component={Home} />
              <Route exact path="/login" component={SignIn} />
              <Route exact path="/register" component={SignIn} />
              <GuardedRoute exact path="/courses" requiredRole={userRole.STUDENT} component={MyCourses} />
              <GuardedRoute exact path="/courses/all" requiredRole={userRole.TEACHER} component={AllCourses} />
              <GuardedRoute exact path="/courses/create" requiredRole={userRole.TEACHER} component={CreateCourse} />
              <GuardedRoute exact path="/courses/deleted" requiredRole={userRole.TEACHER} component={AllDeletedCourses} />
              <GuardedRoute exact path="/courses/:id" requiredRole={userRole.STUDENT} component={Course} />
              <GuardedRoute exact path="/sections/create" requiredRole={userRole.TEACHER} component={CreateSection} />
              <GuardedRoute exact path="/sections/:id/edit" requiredRole={userRole.TEACHER} component={EditSection} />
              <GuardedRoute exact path="/courses/:id/sections/:id/:title" requiredRole={userRole.STUDENT}  component={Section} />
              <GuardedRoute exact path="/user" requiredRole={userRole.STUDENT} component={UserProfile} />
              <Route path="*" component={NotFound} />
            </Switch>
          </Container>
          <Footer />
        </AuthContext.Provider>
      </BrowserRouter>
    </Container>
    </ThemeProvider>
  );
}

export default App;
