
import { createContext } from 'react';

const AddedStudentContext = createContext({
  studentsToEnroll: [],
  setStudentsToEnroll: () => { },
});

const GrantedStudentContext = createContext({
  studentsWithAccess: [],
  setStudentsWithAccess: () => { },
  studentsWithoutAccess: [],
  setStudentsWithoutAccess: () => { }
});

const EnrolledStudentsDialogRefresh = createContext({
  CEDialogBoxOpened: false,
  setCEDialogBoxOpened: () => { },
  ESDialogBoxOpened: false,
  setESDialogBoxOpened: () => { },
});

const GrantedAccessStudentsDialogRefresh = createContext({
  GADialogBoxOpened: false,
  setGADialogBoxOpened: () => { },
});

const AccessStudentsDialogRefresh = createContext({
  AccessDialogBoxOpened: false,
  setAccessDialogBoxOpened: () => { },
});

const StudentAccessContext = createContext({
  CEDialogBoxOpened: false,
  setCEDialogBoxOpened: () => { },
  ESDialogBoxOpened: false,
  setESDialogBoxOpened: () => { },
});



export {
  AddedStudentContext,
  EnrolledStudentsDialogRefresh,
  GrantedAccessStudentsDialogRefresh,
  GrantedStudentContext,
  AccessStudentsDialogRefresh,
  StudentAccessContext
};