import React, { useContext } from 'react';
import { Route, Redirect } from 'react-router-dom';
import PropTypes from 'prop-types';
import AuthContext from './AuthContext';

const GuardedRoute = ({ component: Component, requiredRole, ...rest }) => {
  const { isLoggedIn, user } = useContext(AuthContext);
  return (
    <Route
      {...rest} render={(props) => (
        isLoggedIn ? (user.role >= requiredRole
          ? <Component {...props} /> : <Redirect to='/' />)
          : (<Redirect to={{
            pathname: "/login",
            state: { from: props.location }
          }}/>)
    )}
    />
  );
}

GuardedRoute.defaultProps = {
  requiredRole: 1,
}

GuardedRoute.propTypes = {
  component: PropTypes.func.isRequired,
  requiredRole: PropTypes.number,
};

export default GuardedRoute;
