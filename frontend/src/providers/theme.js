import { createTheme } from "@material-ui/core";

export const newTheme = createTheme({
    palette: {
        primary: {  
            main: '#2989fc',
          },
          secondary: {
            main: '#b90138',
          },
          default: {
              main: '#F4F7FD',
          },
    },
  });