# Poodle E-Learning

<h1>E-Learning System</h1>

End-to-end application helping teachers manage their courses and students learn online. All users can manage their profiles. Teachers can manage courses and sections and enroll students. Courses are either public - accessible after registration - or private - require enrollment by a teacher. The SPA is responsive for Desktop.

The app was created as a final hands-on assignment for the Telerik Alpha JavaScript program.

<b>Authors:</b>
Hristo Staykov,
Ina Dobrilova

<b>Frontend Technologies used:</b>
React, React Material UI

<b>Backend Technologies used:</b>
Express, MariaDB, JWT

## Overview

1. Public part:

- Login
- Register
- Home - view the titles and descriptions to all public courses. Redirects to login

2. Private part:

<b>STUDENTS</b>

- My Courses - view all courses the student is enrolled in. Search courses by title
- Enroll to Public courses

<b>TEACHERS</b>

- My courses - view all courses created by the teacher. Search courses by title
- All courses - view all courses (created by all teachers), which are not archived. Search courses by title
- Individual course - view the course information and all its sections (could be embedded, restricted by date or user, or could open in a new view)
- Archive - view all archived (deleted) courses and restore course capability
- Create course - create a new course
- Enroll students to private courses created by the teacher
- Edit / Delete course - only if you are the teacher who created the course
- Create section - create a new section within a specific course
- Edit / Delete section - only if you are the teacher who created the course
- Restrict the access to a section by date or make it accessible only to certain users

<b>BOTH</b>
- Home - view all public courses 
- Logout

## Project setup

<b>Server</b>

1. You need to have Node.js installed and create an .env file in the backend root directory

The .env file must contain:

```txt
PORT=5000
DB_HOST="localhost"
DB_PORT=3306
DB_USER="root"
DB_PASS="root"
DATABASE="e-learning"
SECRET_KEY="s3cr3tm3ss7g3"
TOKEN_LIFETIME="24h"
```

2. npm i - to install all packages needed for this project.

3. E-Learning Schema - run this script in MariaDB workbench to setup your DB. You can get both the schema and initial data provided or leverage the seed file.

4. npm run seed - to populate your DB with some initial data, or import E-Learning-DB.

6. Start the backend server with npm run start:dev.

### Endpoints
<br>
Without Authentication endpoints:

| endpoint | method | address | body
| ------ | -------- |------ | ------ |
| Browse all public courses | GET | /courses/browse |  |

<br>
Authentication endpoints:

| endpoint | method | address | body
| ------ | -------- |------ | ------ |
| Register | POST | /auth/register | {email, firstname, lastname, password} |
| Login | POST | /auth/login | {username, password} |
| Logout | DELETE | /auth/logout  | n/a |

<br>
Students endpoints:

| endpoint | method | address | body
| ------ | -------- |------ | ------ |
| Get all sections in a course| GET | /sections/course/:cid | n/a |
| Get section by id| GET | /sections/:sectionId | n/a |
| Get a public course or a private one the student is enrolled for by id| GET | /courses/:courseId | n/a |
| Enroll oneself for a course | POST | /users/course/:courseId | batch (array, containing only the student id) |
| Unenroll oneself for a course | DELETE | /users/:userId/course/:courseId
| Get all courses a student is enrolled for | GET | /courses/mycourses | n/a |
| Get the ids of all courses a student is enrolled for | GET | /users/courses-enrolled | n/a |
| Get the image of a course | GET | /images/course/:courseId | n/a
| Get the author of a course | GET | /users/teachers/author/:courseId | n/a |
| Get user's info | GET | /users/my-info | n/a |
| Edit user's names | PUT | /users/my-info | newUser |
| Update password | PUT | /users/my-pass | comparePassword, newPassword |

<br>
Teachers endpoints:

| endpoint | method | address | body
| ------ | -------- |------ | ------ |
| Get ids of students who can access a section | GET | /sections/:id/users/ | n/a |
| Create section | POST | /sections/:id | { courseId, title, content, restrictionDate, isEmbedded } |
| Update section | PUT | /sections/:id | { courseId, title, content, restrictionDate, orderNum, isEmbedded } |
| Delete section | DELETE | /sections/:id | n/a |
| Create course | POST | /courses/:id | {title, description, isPublic, isDeleted} |
| Update section order in course | PUT | /courses/:id/rearrange-sections | order (an array with the ids of the course's section in the order they should be) |
| Update course | PUT | /courses/:id | {title, description, isPublic, isDeleted} |
| Delete course | DELETE | /courses/:id | n/a |
| Get a deleted course by id | GET | /courses/deleted/:id | n/a |
| Undelete and update deleted course by id | PUT | /courses/deleted/:id | {title, description, isPublic, isDeleted} |
| Search all deleted courses | GET | /courses/deleted/  | optional query parameters: search, limit, offset
| Get course by id, no matter public or private | GET | /courses/:courseId | n/a |
| Search all courses, no matter public or private | GET | /courses/ | optional query parameters: search, isPublic [0,1], limit, offset |
| Get all deleted students | GET | /users/students/deleted | n/a |
| Search from all ((not) enrolled) students to a course | GET | /users/ | optional query parameters: cid, search, enrolled
| Grant student(s) access to section | POST | /users/section/:sectionId |  batch (an array of student ids to be granted access) |
| Revoke student(s) access to section | DELETE | /users/section/:sectionId |  batch (an array of student ids to have their access revoked) |
| Delete student | DELETE | /users/:studentId/ | n/a |
| Undelete student | PUT | /users/:studentId/undelete | n/a |
| Enroll a batch of students to a private course | POST | /users/course/:courseId | batch (array, containing the ids of the students to be enrolled) |
| Unenroll a student to a private course | DELETE | /users/:userId/course/:courseId | n/a |
| Update/Upload course image | POST | /images/course/:courseId | file |


<br>
<b>Client</b>

1. npm i - to install all packages needed for this project.

2. Start the dev frontend server with npm run start.

3. Navigate to http://localhost:3000 in your browser.

<b>Default provided users</b>

Login as a:
- teacher with email: teacher@gmail.com and pass: 123456
- student with email: student@gmail.com and pass: 123456
- create your own profile

Enjoy and have fun.

## Available Scripts

1. npm run seed - starts a script that populates your DB with some initial data, which can be viewed in backend - src - config - seed.js

2. npm start - runs the frontend part of the app in development mode. Open http://localhost:3000 to view it in the browser.

3. npm run start:dev - runs the backend part of the app.

## Views

The app's home page shows all public courses, but they are accessible only after registration.

![](README-images/Public-Home.png)

You can navigate to the login/register view from the navigation.

![](README-images/Public-Register.png)


The My Courses view shows all the courses a student is enrolled in, or all the courses created by the teacher depending on the user's role. It allows users to search for a course by title, choose the number of results per page and navigate the results via pagination.

![](README-images/Private-MyCoursesView.png)

The All Courses view is accessible only to teachers and shows all courses (public and private).

![](README-images/Private-AllCoursesView.png)

Teachers can create new courses, update and delete their own courses, and enrol students to their private courses.
Teachers can also add new sections (create their own HTML, incl. embed video, ppt, image, and doc files) to the courses they've created, reorder sections, and add section restrictions.

![](README-images/Private-CourseView.png)

Teachers can also restore archived/deleted courses.

![](README-images/Private-RestoreArchivedCourses.png)

## Room for improvement
- Further improve UX and UI
- Add hamburger menu for mobile
- Add notifications about course (user is enrolled) updates
- Add like course / liked courses view 
- View other users' profiles
- Add ban students functionality
- Search courses by topic or teacher
- Streamline / enhance code



